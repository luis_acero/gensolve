// PRETTYPHOTO WP HACK
jQuery("a[data-rel^='prettyPhoto']").prettyPhoto();

(function($) {
	"use strict";

	$(window).load(function() {

		// WOW ANYMATIONS
		new WOW().init();
		
				// PRELOADING SCREEN
		var $preloader = $('.preloader');
		$($preloader).fadeOut().remove();

		// PRELOADER
		if ( typeof monte_preloader !== 'undefined') {
			if (monte_preloader == '1') {
			if (monte_preloader_inner == '1') {
				$('body').jpreLoader();
			} else {
				if (monte_frontpage == '1') {
					$('body').jpreLoader();
				} else {
					$('body').css('visibility', 'visible');
				}
			}
		} else {
			$('body').css('visibility', 'visible');
		}
		} else {
			$('body').css('visibility', 'visible');
		}

		var $menuitems = $('.navbar-nav > li');
		var activeitems = 0;
		$($menuitems).each(function() {
			$(this).children().addClass('first-lvl');
			if ($(this).hasClass('active')) {
				activeitems = activeitems + 1;
			}
		});
		if (activeitems == 0) {
			$('.navbar-nav > li:first-child').addClass('active');
		}
		if ( typeof disable_footer1 !== 'undefined') {
			if (disable_footer1 == 1) {
				var footer1 = $('#footer');
				$(footer1).css('display', 'none');
			} else {
				var footer1 = $('.footer-overlap');
				$(footer1).css('display', 'none');
			}
		}

		if ( typeof disable_footer2 !== 'undefined') {
			var footer2 = ('#upper-footer');
			$(footer2).css('display', 'none');
		}

		if ( typeof disable_footer3 !== 'undefined') {
			var footer3 = ('#lower-footer');
			$(footer3).css('display', 'none');
		}

		/*----------------------------------------------------*/
		// menu
		/*----------------------------------------------------*/


		
		if ($(".navbar-nav").length > 0) {
			monte_navbar();
		}
		function monte_navbar() {
			"use strict";
					var $navbar_nav = $(".navbar-nav");
		var $navbar_nav_active = $('.navbar-nav .active');
		$($navbar_nav).prepend("<li class='active-item-menu'></li>");

		var itempos = $($navbar_nav_active).position().left + 10;
		var itemsize = $($navbar_nav_active).width() - 35;
		var active_menu_item = $(".active-item-menu");

		$(active_menu_item).stop().animate({
			left : itempos,
			width : itemsize
		}, {
			duration : 300,
			easing : 'linear'
		});

		var $firstlev = $(".navbar-nav li a.first-lvl");
		$($firstlev).on({
			mouseenter : function() {
				var itemposition = $(this).parent().position().left + 10;
				var itemwidth = $(this).parent().width() - 35;
				$(active_menu_item).stop().animate({
					left : itemposition,
					width : itemwidth
				}, {
					duration : 300,
					easing : 'linear'
				});
			},
			mouseleave : function() {
				$(active_menu_item).stop().animate({
					left : itempos,
					width : itemsize
				}, {
					duration : 300,
					easing : 'linear'
				});
			}
		});
			
		}
		
		


		/*----------------------------------------------------*/
		// PORTFOLIO BUTTONS
		/*----------------------------------------------------*/

		if ($(".portfolio-buttons").length > 0) {
			monte_portfolio_buttons();
		}
		function monte_portfolio_buttons() {
			"use strict";

			var $pbuttons = $(".portfolio-buttons");
			$($pbuttons).prepend("<li class='active-item'></li>");

			var itempos1 = $('.portfolio-buttons .active').position().left + 10;
			var itemsize1 = $('.portfolio-buttons .active').width() - 20;
			var active_item = $(".active-item");
			var portfolio_link = $(".portfolio-buttons li a");

			$(active_item).stop().animate({
				left : itempos1,
				width : itemsize1
			}, {
				duration : 300,
				easing : 'linear'
			});

			$(portfolio_link).on({
				mouseenter : function() {
					var itemposition1 = $(this).position().left + 10;
					var itemwidth1 = $(this).parent().width() - 20;
					$(active_item).stop().animate({
						left : itemposition1,
						width : itemwidth1
					}, {
						duration : 300,
						easing : 'linear'
					});
				},
				mouseleave : function() {
					$(active_item).stop().animate({
						left : itempos1,
						width : itemsize1
					}, {
						duration : 300,
						easing : 'linear'
					});
				}
			});

		}

		// WOOCOMMERCE BUTTON CUSTOMIZATION
		var $wooproduct = $('.woocommerce-product-search');
		$('<button type="submit" class="search-button"><i class="fa fa-search"></i></button>').appendTo($wooproduct);

		$('.woocommerce ul.products li.product .add_to_cart_button, .woocommerce-page ul.products li.product .add_to_cart_button').each(function() {
			$(this).html('<i class="fa fa-plus"></i>');

			$(this).wrapAll('<div class="portfolio-hover">');

		});

		$('.woocommerce ul.products li.product, .woocommerce-page ul.products li.product').each(function() {
			var src2 = $(this).children('a').attr('href');

			$('<div class="product-overlay"></div>').appendTo(jQuery(this));
			$(this).children().children('.price').insertBefore(jQuery(this).children().children('h3'));

			$('<a class="button view-buttons" href="' + src2 + '"><i class="fa fa-link"></i></a>').appendTo(jQuery(this).children('.portfolio-hover'));

		});

		// SEARCH BOX VISIBILITY
		var $searchicon = $('#searchicon');
		$($searchicon).on('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('active');
		});


	

		// COUNTERS
		function monte_counter(numbers) {
			"use strict";

			var currentNumber = 0;
			var number = numbers.attr('data-number');
			jQuery({
				numberValue : currentNumber
			}).animate({
				numberValue : number
			}, {
				duration : 5000,
				easing : 'linear',
				step : function() {
					numbers.html('<h1>' + Math.round(this.numberValue) + '</h1>');
				},
				complete : function() {
					numbers.html('<h1>' + number + '</h1>');
				}
			});
		}

		var $counternumber = $(".counter-number");
		$($counternumber).each(function() {
			$(this).waypoint(function(event, direction) {
				var numbers = $(this);
				monte_counter(numbers);

			}, {
				triggerOnce : true,
				offset : '90%'
			});

		});

		// PROGRESS BARS

		// SKILL CHARTS

		var $chart = $('.chart');
		$($chart).each(function() {
			var color = $(this).attr('data-color');
			$(this).waypoint(function(event, direction) {

				$(this).easyPieChart({
					animate : 1000,
					barColor : color,
					size : '180',
					lineWidth : '7',
					trackColor : false,
					scaleColor : false
				});

			}, {
				triggerOnce : false,
				offset : '90%'
			});

		});

		var $chartwhite = $('.chart-white');
		$($chartwhite).each(function() {
			var color = $(this).attr('data-color');
			$(this).waypoint(function(event, direction) {

				$(this).easyPieChart({
					animate : 1000,
					barColor : color,
					size : '180',
					lineWidth : '7',
					trackColor : false,
					scaleColor : false
				});

			}, {
				triggerOnce : true,
				offset : '90%'
			});

		});

		// FITVIDS
		var $fidvids = $(".fitvid");
		$($fidvids).fitVids();

		// MASONRY
		var $container = $('.mason');
		$container.masonry({
			itemSelector : '.news-item',
			"gutter" : 0
		});

	});

	$(document).ready(function() {

		var $overlappedtop = $('.overlapped-top');
		var $overlappedbottom = $('.overlapped-bottom');

		$($overlappedtop).each(function() {
			if ($(this).next().attr('data-vc-full-width') == "true") {
				var $overlap_top_bg = $(this).attr('data-overlap-top-bg');
				$(this).next().prepend("<div class='top-overlap' style='background: " + $overlap_top_bg + "'></div>");
			}
		});

		$($overlappedbottom).each(function() {
			if ($(this).prev().prev().attr('data-vc-full-width') == "true") {
				var $overlap_bottom_bg = $(this).attr('data-overlap-bottom-bg');
				$(this).prev().prev().append("<div class='bottom-overlap' style='background: " + $overlap_bottom_bg + "'></div>");
			}
		});

		// DROPDOWN MENU ADJUSTMENT
		var $submeu = $('.sub-menu');
		$($submeu).each(function() {
			$(this).addClass('dropdown-menu span10');
		});
		var $lastchild = $('.menu-item-has-children');
		$($lastchild).each(function() {
			$(this).addClass('dropdown');
		});

		// FLEXSLIDER
		var $flexslider = $('.flexslider');
		$($flexslider).flexslider({

			animation : "fade",
			slideDirection : "horizontal",
			slideshow : true,
			slideshowSpeed : 3500,
			animationDuration : 500,
			directionNav : true,
			controlNav : false
		});

		// CAROUSELS
		var $carousel = $(".carousel");
		$($carousel).owlCarousel({
			autoPlay : 3000,
			transitionStyle : "fade",
			items : 6,
			itemsDesktopSmall : [979, 4],
			itemsTablet : [768, 2],
			itemsTabletSmall : false,
			itemsMobile : false,
			pagination : false
		});

		var $carouselsingle = $(".carousel-single");
		$($carouselsingle).owlCarousel({
			autoPlay : 2500,
			transitionStyle : "goDown",
			singleItem : true,
			navigation : false,
			pagination : false
		});

		var $teamcarousel = $(".team-carousel");
		$($teamcarousel).each(function() {
			var arrows = $(this).attr('data-carousel-arrows');
			if (arrows == 'true') {
				var arrows1 = true;
			} else {
				var arrows1 = false;
			}
			var items = $(this).attr('data-carousel-items');
			var pagination = $(this).attr('data-carousel-pagination');
			if (pagination == 'true') {
				var pagination1 = true;
			} else {
				var pagination1 = false;
			}
			$(this).owlCarousel({
				items : items,
				itemsDesktop : [1199, 3],
				itemsDesktopSmall : false,
				itemsTablet : [750, 2],
				itemsTabletSmall : false,
				itemsMobile : [479, 1],
				navigation : arrows1,
				pagination : pagination1,
				navigationText : ["<span class'nav-left'><i class='fa fa-angle-left'></i></span>", "<span class'nav-right'><i class='fa fa-angle-right'></i></span>"],

			});

		});

		var $testimonials = $(".testimonial-carousel");
		$($testimonials).each(function() {

			var arrows = $(this).attr('data-carousel-arrows');
			if (arrows == 'true') {
				var arrows1 = true;
			} else {
				var arrows1 = false;
			}
			var pagination = $(this).attr('data-carousel-pagination');
			if (pagination == 'true') {
				var pagination1 = true;
			} else {
				var pagination1 = false;
			}

			$(this).owlCarousel({
				autoPlay : 5000,
				slideSpeed : 3000,
				pagination : pagination1,
				transitionStyle : "fade",
				singleItem : true,
				navigation : arrows1,
				navigationText : ["<span class'nav-left'><i class='fa fa-angle-left'></i></span>", "<span class'nav-right'><i class='fa fa-angle-right'></i></span>"],
				lazyLoad : true,
				lazyFollow : true,
				lazyEffect : "fade",
			});

		});

		var $inneritem = $('.inner-item');
		$($inneritem).each(function() {
			var data_id = $(this).attr('data-id');
			var data_type = $(this).attr('data-type');
			$(this).parent().attr("data-id", data_id);
			$(this).parent().attr("data-type", data_type);
			$(this).removeAttr("data-id");
			$(this).removeAttr("data-type");

		});

		var $flexnext = $('.flex-next');
		$($flexnext).each(function() {
			$(this).html('<i class="fa fa-chevron-right"> </i>');
		});

		var flexprev = $('.flex-prev');
		$(flexprev).each(function() {
			$(this).html('<i class="fa fa-chevron-left"> </i>');
		});

		var $filterType = $('.portfolio-buttons li.active a').attr('class');
		var $holder = $('#wrapper-folio');
		var $data = $holder.clone();

		var $portfoliobuttons = $('.portfolio-buttons');
		var $portfoliobuttonsli = $('.portfolio-buttons li')
		var $pstandalone = $(".portfolioitems");
		var $loadmorelink_four = $("#loadmore a.four");
		var $loadmorelink_eight = $("#loadmore a.right");
		var $loadmore = $('#loadmore');

		$(".portfolioitems.eight.col-md-3.hidden").slice(0, 8).removeClass('hidden');
		$($loadmorelink_eight).on('click', function(e) {
			e.preventDefault();
			$(".portfolioitems.eight.col-md-3.hidden").slice(0, 4).removeClass('hidden');

			if (!$(".portfolioitems.eight.col-md-3.hidden").length) {
				$($loadmore).css('display', 'none');
			}
		});

		$(".portfolioitems.four.col-md-3.hidden").slice(0, 4).removeClass('hidden');
		$($loadmorelink_four).on('click', function(e) {
			e.preventDefault();
			$(".portfolioitems.four.col-md-3.hidden").slice(0, 4).removeClass('hidden');

			if (!$(".portfolioitems.col-md-3.hidden").length) {
				$($loadmore).css('display', 'none');
			}
		});

		if ($(".portfolioitems.hidden").length == 0) {
			$($loadmore).css('display', 'none');
		}

		$($portfoliobuttons).on('click', 'li a', function(e) {
			e.preventDefault();
			$(".portfolioitems.hidden").removeClass('hidden');
			$($pstandalone).css('display', 'block');
			$($loadmore).css('display', 'none');

			if (($(this).attr('class')) !== 'all') {
				$($portfoliobuttonsli).removeClass('active');
				var $filterType = $(this).attr('class');
				$(this).parent().addClass('active');
				if ($filterType == 'all') {
					var $filteredData = $data.find('div');
				} else {
					var $filteredData = $data.find('div[data-type~=' + $filterType + ']');
				}

				// call quicksand and assign transition parameters
				$holder.quicksand($filteredData, {
					duration : 0
				}, function() {// quickstand callback
					$holder.css('height', 'auto');
					$("a[data-rel^='prettyPhoto']").prettyPhoto();
					$('a[data-rel]').each(function() {
						$(this).attr('rel', $(this).data('rel'));
					});

				});
				$(".portfolioitems.hidden").removeClass('hidden');

			} else {
				$($portfoliobuttonsli).removeClass('active');
				var $filterType = $(this).attr('class');
				$(this).parent().addClass('active');
				if ($filterType == 'all') {
					var $filteredData = $data.find('div');
				} else {
					var $filteredData = $data.find('div[data-type~=' + $filterType + ']');
				}

				// call quicksand and assign transition parameters
				$holder.quicksand($filteredData, {
					duration : 0
				}, function() {// quickstand callback
					$holder.css('height', 'auto');
					$("a[data-rel^='prettyPhoto']").prettyPhoto();
					$('a[data-rel]').each(function() {
						$(this).attr('rel', $(this).data('rel'));
					});

				});
				$(".portfolioitems.hidden").removeClass('hidden');
			}

			return false;
		});

		// NAVIGATION
		var $nav1 = $(".navbar-nav > li > a");
		var $nav2 = $(".menu-item-has-children a");
		$($nav1).addClass('first-lvl');
		$($nav2).css('max-width', '100%');

		// VIEWPORT ADJUSTMENT
		function monte_viewport() {
			var e = window,
			    a = 'inner';
			if (!('innerWidth' in window )) {
				a = 'client';
				e = document.documentElement || document.body;
			}
			return {
				width : e[a + 'Width'],
				height : e[a + 'Height']
			};
		}

		var ww = monte_viewport().width;
		var $drop = $('.dropdown-menu');
		if (ww < 960) {
			$($drop).each(function() {

				$(this).addClass('displaynone');
			})
		}

		// DROPDOWN NAVIGATION
		var $firstlevel = $(".first-lvl");
		$($firstlevel).hover(function() {
			if (ww >= 960) {
				$($drop).each(function() {
					$(this).removeClass('displaynone');
				})
				$($drop).each(function() {
					$(this).addClass('displayblock');
				})
			}
		});

		// MENU & TOPBAR POSITIONING
		var $document = $(document);
		var $navbar = $('.navbar');
		var $navbarcontainer = $('.navbar .container .row');
		var $pageheader = $('.page-header')

		if (menu_position == 'above') {

			$($navbar).css({
				'position' : 'relative',
				'top' : 'auto'
			});

			$($navbarcontainer).css({

				'padding-top' : '15px',
				'padding-bottom' : '15px'
			});
			$document.scroll(function() {
				if ($document.scrollTop() >= 55) {
					$($navbar).css({
						'position' : 'fixed',
						'top' : '0px'
					});

					$($navbarcontainer).css({

						'padding-top' : '0',
						'padding-bottom' : '0'
					});

				} else {

					$($navbar).css({
						'position' : 'relative',
						'top' : 'auto'
					});

					$($navbarcontainer).css({

						'padding-top' : '15px',
						'padding-bottom' : '15px'
					});
				}

			});
		}
		if (menu_position == 'ontop') {

			$($pageheader).css('padding-top', '170px');
			$($navbar).css({
				'position' : 'absolute',
				'top' : '0px'
			});

			$($navbarcontainer).css({

				'padding-top' : '15px',
				'padding-bottom' : '15px'
			});

			if (menu_transparent == 1) {
				$($navbar).css('background', 'transparent');
			}

			$document.scroll(function() {

				if ($document.scrollTop() >= 55) {
					$($navbar).css({
						'position' : 'fixed',
						'top' : '0px'
					});

					$($navbarcontainer).css({

						'padding-top' : '0',
						'padding-bottom' : '0'
					});

					if (menu_transparent == 1) {
						$($navbar).css('background', menu_bg);
					}

				} else {

					$($navbar).css({
						'position' : 'absolute',
						'top' : '0px'
					});

					$($navbarcontainer).css({

						'padding-top' : '15px',
						'padding-bottom' : '15px'
					});

					if (menu_transparent == 1) {
						$($navbar).css('background', 'transparent');
					}
				}

			});
		}

		// DROPDOWN MENU WIDTH
		var $dm = $('.sub-menu');

		$($dm).each(function() {

			var maxlength = 0;
			var currlenght = 0;
			var csswidth = 0;
			$(this).children().each(function() {

				currlenght = $(this).children().html().length;

				if (currlenght > maxlength) {
					maxlength = currlenght;
				}

			});
			csswidth = maxlength * 7 + 70;
			$(this).css('width', csswidth + 'px')
		});
		var $dd = $('.dropdown');
		$($dd).each(function() {
			$(this).children().first().attr('class', 'dropdown-toggle');
			$(this).children().first().attr('data-toggle', 'dropdown');
		});

		$("[data-toggle='tooltip']").tooltip();

		var width_input = $(".wpcf7-submit").css('width');
		$(".wpcf7-submit").parent("p").css('width', width_input);
		$(".wpcf7-submit").parent("p").css('position', 'relative');

		// WIDGET STYLING
		var $option = $('option');
		$($option).each(function() {

			var myStr = $(this).text();
			var newStr;

			if (myStr.length > 35) {

				newStr = myStr.substring(0, 35);
				$(this).attr('title', myStr);
				$(this).html(newStr + ' ...');

			}
		});

		$('h3 .rsswidget img').each(function() {
			$(this).replaceWith('<i class="fa fa-rss"></i>');
		});

		$(".widget select").wrap("<div class='pseudo-dropdown'></div>");
		$(".widget_categories select").wrap("<div class='pseudo-dropdown'></div>");
		$(".widget_archive select").wrap("<div class='pseudo-dropdown'></div>");
		$(".widget_nav_menu select").wrap("<div class='pseudo-dropdown'></div>");
		$(".widget_meta select").wrap("<div class='pseudo-dropdown'></div>");
		$(".widget_pages select").wrap("<div class='pseudo-dropdown'></div>");
		$(".widget_recent_comments select").wrap("<div class='pseudo-dropdown'></div>");
		$(".widget_product_categories select").wrap("<div class='pseudo-dropdown'></div>");
		$(".woocommerce-ordering select").wrap("<div class='pseudo-dropdown'></div>");
		$("<span class='pseudo-dropdown-bg'><i class='fa fa-sort-down'> </i></span>").appendTo(".pseudo-dropdown");
		$(".widget_product_search #s").after("<button class='search-button'><i class='fa fa-search'></i></button>");
		$('#blogmasonry').insertBefore('#blogcontent');
		$('a[data-rel]').each(function() {
			$(this).attr('rel', $(this).data('rel'));
		});

		$.ajax({
			cache : false
		});

		// PORTFOLIO

		var $ptrick = $('.trick');
		var $pnext = $('.next_post a');
		var $pprev = $('.prev_post a');
		var $pwrapper = $("#wrapper-folio");
		var $pload = $("#loadmore");

		$($ptrick).off().on('click', monte_loadAjax);
		$($pnext).off().on('click', monte_loadAjax);
		$($pprev).off().on('click', monte_loadAjax);

		$(window).resize(function() {
			var cw = $($pwrapper).parents().width();
			$($pwrapper).width(cw);
		});
		
		
		if ( $( "#wpadminbar" ).length ) {
 
  			  $( ".navbar" ).css('margin-top', '32px');
 
		}



	});

})(jQuery);

(function($) {
	"use strict";
	// DEBOUNCE FUNCTION
	(function($, sr) {

		// debouncing function from John Hann
		// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
		var debounce = function(func, threshold, execAsap) {
			var timeout;

			return function debounced() {
				var obj = this,
				    args =
				    arguments;
				function delayed() {
					if (!execAsap)
						func.apply(obj, args);
					timeout = null;
				};

				if (timeout)
					clearTimeout(timeout);
				else if (execAsap)
					func.apply(obj, args);

				timeout = setTimeout(delayed, threshold || 100);
			};
		}
		// smartresize
		jQuery.fn[sr] = function(fn) {
			return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
		};

	})(jQuery, 'smartresize');

	// MENU ADJUSTMENTS
	monte_adjust_menu();
	$(window).smartresize(function() {
		var ww = monte_viewport().width;
		monte_adjust_menu(ww);
	});
	var ww = monte_viewport().width;

	function monte_viewport() {
		var e = window,
		    a = 'inner';
		if (!('innerWidth' in window )) {
			a = 'client';
			e = document.documentElement || document.body;
		}
		return {
			width : e[a + 'Width'],
			height : e[a + 'Height']
		};
	}

	function monte_adjust_menu(ww) {
		var $mheader = $('.page-header');
		var $mnavbar = $('.navbar');
		var $document2 = $(document);
		if (ww >= 992) {

			if (menu_transparent == 1) {

				//$($mnavbar).css('background', 'transparent');
				if ($document2.scrollTop() >= 55) {
					$($mnavbar).css('background', menu_bg);
				} else {

					$($mnavbar).css('background', 'transparent');
				}

			}
		} else {

			$($mnavbar).css('background', menu_bg);

		}

	}

})(jQuery);

// AJAX LOADING
function monte_loadAjax() {
	"use strict";
	var post_link = jQuery(this).attr("href") + " #ajaxContainer";

	var $shc = jQuery("#single-home-container");
	jQuery($shc).addClass("active");

	jQuery($shc).html('');

	jQuery($shc).load(post_link, function(responseTxt, statusTxt, xhr) {
		if (statusTxt == "success") {//  callback
			jQuery('.flexslider').flexslider({

				animation : "fade",
				slideDirection : "horizontal",
				slideshow : true,
				slideshowSpeed : 3500,
				animationDuration : 500,
				directionNav : true,
				controlNav : false
			});

			var $flexnext = jQuery('.flex-next');
			jQuery($flexnext).each(function() {
				jQuery(this).html('<i class="fa fa-chevron-right"> </i>');
			});

			var flexprev = jQuery('.flex-prev');
			jQuery(flexprev).each(function() {
				jQuery(this).html('<i class="fa fa-chevron-left"> </i>');
			});

			var $monte_trick = jQuery('.trick');
			var $monte_nextpost = jQuery('.next_post a');
			var $monte_prevpost = jQuery('.prev_post a');
			var $monte_close = jQuery('.close');
			var $monte_fit = jQuery(".fitvid");

			jQuery($monte_trick).off().on('click', monte_loadAjax);
			jQuery($monte_nextpost).off().on('click', monte_loadAjax);
			jQuery($monte_prevpost).off().on('click', monte_loadAjax);

			jQuery($monte_fit).fitVids();
			jQuery("a[data-rel^='prettyPhoto']").prettyPhoto();
			jQuery('a[data-rel]').each(function() {
				jQuery(this).attr('rel', jQuery(this).data('rel'));
			});

			jQuery($monte_close).on('click', function() {
				"use strict";
				jQuery($shc).html("");
				jQuery($shc).removeClass("active");
				return false;
			});

		}
		if (statusTxt == "error")

			alert("Error");
	});

	return false;
}

var vc_rowBehaviour;
function isFunction(func) {
	if ( typeof func == 'function')
		return true
}


jQuery(window).bind("load", function() {

	if (isFunction(vc_rowBehaviour) == true) {
		vc_rowBehaviour();
	}

	jQuery('.header > div > .module').removeAttr('style');
	jQuery(window).resize();
	jQuery('.header > div > .module').attr('style', 'min-height:1px;');

});

