(function ($) {
    var Shortcodes = vc.shortcodes;
    window.VcMembersView = vc.shortcode_view.extend({
        change_columns_layout:false,
        events:{
            'click > .controls .section_delete':'deleteShortcode',
            'click > .controls .set_columns':'setColumns',
            'click > .controls .section_add':'addElement',
            'click > .controls .section_edit':'editElement',
  
            'click > .controls .section_move':'moveElement',
            'click > .controls .section_toggle': 'toggleElement',
            'click > .wpb_element_wrapper .vc_controls': 'openClosedRow'
        },
    });
    

  
    
})(window.jQuery);