<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/docs/define-meta-boxes
 */

/********************* META BOX DEFINITIONS ***********************/

/**
 * Prefix of meta keys (optional)
 * Use underscore (_) at the beginning to make keys hidden
 * Alt.: You also can make prefix empty to disable it
 */

$prefix = 'monte_';

global $hazel_meta_boxes;

$hazel_meta_boxes = array();

global $smof_data;




/*  BLOG SETTINGS */

$hazel_meta_boxes[] = array(
	'id' => $prefix . 'page-blog',
	'title' => 'Blog Options',
	'pages' => array( 'page'),
	'context' => 'normal',

	'fields'	=> array(
		
	array(
			'name'		=> 'Display Type',
			'id'		=> $prefix . 'blog_type',
			'type'		=> 'select',
			'options'	=> array(
				'spinner_style'	=> 'Spinner Style',
				'sidebar_list'	=> 'Posts list with sidebar',
			),
			'multiple'	=> false,
			'std'		=> array( 'sidebar_list' )
		),
		
		
		array(
			'name'		=> 'Disable page header (Spinner View)',
			'id'		=> $prefix . "disable_header",
			'clone'		=> false,
			'type' => 'checkbox',
			'std'  => 1,
		),
		
		
		array(
			'name'		=> 'Disable widgetized footer (Spinner View)',
			'id'		=> $prefix . "disable_widgetized",
			'clone'		=> false,
			'type' => 'checkbox',
			'std'  => 1,
		),
		
		array(
			'name'		=> 'Disable social section footer (Spinner View)',
			'id'		=> $prefix . "disable_social",
			'clone'		=> false,
			'type' => 'checkbox',
			'std'  => 1,
		),
		
		array(
			'name'		=> 'Disable logo section footer (Spinner View)',
			'id'		=> $prefix . "disable_logo",
			'clone'		=> false,
			'type' => 'checkbox',
			'std'  => 0,
		),
		
		
		array(
			'name'	=> 'Post 1 BG Color (Spinner View)',
			'id'	=> $prefix . 'bg1',
			'type'	=> 'color',
			'std'		=> '#dc143c'
		),
		
		array(
			'name'	=> 'Post 1 Font Color (Spinner View)',
			'id'	=> $prefix . 'font1',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
		
			array(
			'name'	=> 'Post 2 BG Color (Spinner View)',
			'id'	=> $prefix . 'bg2',
			'type'	=> 'color',
			'std'		=> '#000000'
		),
		
		array(
			'name'	=> 'Post 2 Font Color (Spinner View)',
			'id'	=> $prefix . 'font2',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
		
			array(
			'name'	=> 'Post 3 BG Color (Spinner View)',
			'id'	=> $prefix . 'bg3',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
		
		array(
			'name'	=> 'Post 3 Font Color (Spinner View)',
			'id'	=> $prefix . 'font3',
			'type'	=> 'color',
			'std'		=> '#000000'
		),
		
			array(
			'name'	=> 'Post 4 BG Color (Spinner View)',
			'id'	=> $prefix . 'bg4',
			'type'	=> 'color',
			'std'		=> '#000000'
		),
		
		array(
			'name'	=> 'Post 4 Font Color (Spinner View)',
			'id'	=> $prefix . 'font4',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
		
			array(
			'name'	=> 'Post 5 BG Color (Spinner View)',
			'id'	=> $prefix . 'bg5',
			'type'	=> 'color',
			'std'		=> '#dc143c'
		),
		
		array(
			'name'	=> 'Post 5 Font Color (Spinner View)',
			'id'	=> $prefix . 'font5',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
		
			array(
			'name'	=> 'Post 6 BG Color (Spinner View)',
			'id'	=> $prefix . 'bg6',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
		
		array(
			'name'	=> 'Post 6 Font Color (Spinner View)',
			'id'	=> $prefix . 'font6',
			'type'	=> 'color',
			'std'		=> '#000000'
		),
		
		
	)
);





/* ----------------------------------------------------- */
// PAGE SETTINGS
/* ----------------------------------------------------- */

$hazel_meta_boxes[] = array(
	'id' => 'pagesettings',
	'title' => 'Page Settings',
	'pages' => array( 'page' ),
	'context' => 'normal',
	'priority' => 'high',


	'fields' => array(

			array(
			'name'		=> 'Active Menu Item',
			'id'		=> $prefix . 'active_menu_item',
			'type'		=> 'post',
			'field_type' => 'select',
			'post_type'  => 'nav_menu_item',
			'options' => array(
				'args' => array(
					'name'  =>  'id',
				)
				),
		),
		
		
		
				array(
			'name'		=> 'Alternative Page Title (optional)',
			'id'		=> $prefix . 'alt_page_title',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> 'the sp<span>i</span>nner'
		),
		
		
				array(
			'name'		=> 'Page title font size',
			'id'		=> $prefix . "title_font_size",
			'clone'		=> false,
			'type'		=> 'select',
			'options'	=> array(
				'70px'		=> '70px',
				'60px'		=> '60px',
				'50px'		=> '50px',
				'40px'		=> '40px',
				'30px'		=> '30px',
				'20px'		=> '20px',
			),
			'multiple'	=> false,
		),
		
		
			array(
			'name'		=> 'Page Subtitle (optional)',
			'id'		=> $prefix . 'page_subtitle',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> 'welcome! we are a creative digital agency'
		),
		

		array(
			'name'		=> 'Include Full Width Slider',
			'id'		=> $prefix . "fw_slider",
			'clone'		=> false,
			'type' => 'checkbox',
			'std'  => 0,
		),
		
		array(
			'name'		=> 'Fullwidth Slider Alias (optional)',
			'id'		=> $prefix . 'slider_shortcode',
			'desc'		=> 'Alias is a part of rev_slider shortcode. Eg. alias of [rev_slider slider5] is slider5.',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> 'slider5'
		),
		

		
			array(
			'name'		=> 'Page Header Type',
			'id'		=> $prefix . "header_type",
			'clone'		=> false,
			'type'		=> 'select',
			'options'	=> array(
				'color'		=> 'Color',
				'image'		=> 'Image'
			),
			'multiple'	=> false,
		),
		
		
						array(
			'name'		=> 'Use page header overlap',
			'id'		=> $prefix . "page_header_overlap",
			'clone'		=> false,
			'type'		=> 'select',
			'options'	=> array(
				'yes'	=> 'Yes',
				'no'		=> 'No',
			),
			'multiple'	=> false,
		),
		
			array(
			'name'	=> 'Page Header Overlap Background Color',
			'id'	=> $prefix . 'page_header_overlap_bg',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
		
				array(
			'name'	=> 'Header Background Image (Optional)',
			'id'	=> $prefix . 'header_image',
			'type'	=> 'plupload_image',
			'max_file_uploads' => 1,
			'std'		=> ''
		),
		
				array(
			'name'	=> 'Header Background Color',
			'id'	=> $prefix . 'header_bg_color',
			'type'	=> 'color',
			'std'		=> '#000000'
		),
					array(
			'name'	=> 'Heading Font Color',
			'id'	=> $prefix . 'heading_font_color',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
		
				array(
			'name'	=> 'Breadcrums Color #1',
			'id'	=> $prefix . 'breadcrums1_color',
			'type'	=> 'color',
			'std'		=> '#dc143c'
		),
		
					array(
			'name'	=> 'Breadcrums Color #2',
			'id'	=> $prefix . 'breadcrums2_color',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),

	
	
	)
);



/* ----------------------------------------------------- */
// BLOG SETTINGS
/* ----------------------------------------------------- */

$hazel_meta_boxes[] = array(
	'id'		=> $prefix . 'blog',
	'title'		=> 'Post Settings',
	'pages'		=> array( 'post' ),
	'context' => 'normal',

	
	'fields'	=> array(
	
		array(
			'name'		=> 'Active Menu Item',
			'id'		=> $prefix . 'active_menu_item',
			'type'		=> 'post',
			'field_type' => 'select',
			'post_type'  => 'nav_menu_item',
			'options' => array(
				'args' => array(
					'name'  =>  'id',
				)
				),
		),

		
	
				array(
			'name'		=> 'Alternative Page Title (optional)',
			'id'		=> $prefix . 'alt_post_title',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> 'the sp<span>i</span>nner'
		),
		
		
				array(
			'name'		=> 'Page title font size',
			'id'		=> $prefix . "post_title_font_size",
			'clone'		=> false,
			'type'		=> 'select',
			'options'	=> array(
				'70px'		=> '70px',
				'60px'		=> '60px',
				'50px'		=> '50px',
				'40px'		=> '40px',
				'30px'		=> '30px',
				'20px'		=> '20px',
			),
			'multiple'	=> false,
		),
		
		
			array(
			'name'		=> 'Page Subtitle (optional)',
			'id'		=> $prefix . 'post_subtitle',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> 'welcome! we are a creative digital agency'
		),
		



		
		
				array(
			'name'		=> 'Include Full Width Slider',
			'id'		=> $prefix . "post_fw_slider",
			'clone'		=> false,
			'type' => 'checkbox',
			'std'  => 0,
		),
		
		array(
			'name'		=> 'Fullwidth Slider Alias (optional)',
			'id'		=> $prefix . 'post_slider_shortcode',
			'desc'		=> 'Alias is a part of rev_slider shortcode. Eg. alias of [rev_slider slider5] is slider5.',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> 'slider5'
		),
		

		
			array(
			'name'		=> 'Page Header Type',
			'id'		=> $prefix . "post_header_type",
			'clone'		=> false,
			'type'		=> 'select',
			'options'	=> array(
				'color'		=> 'Color',
				'image'		=> 'Image'
			),
			'multiple'	=> false,
		),
		
		
					array(
			'name'		=> 'Use page header overlap',
			'id'		=> $prefix . "post_header_overlap",
			'clone'		=> false,
			'type'		=> 'select',
			'options'	=> array(
				'yes'	=> 'Yes',
				'no'		=> 'No',
			),
			'multiple'	=> false,
		),
		
			array(
			'name'	=> 'Page Header Overlap Background Color',
			'id'	=> $prefix . 'post_header_overlap_bg',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
		
		
		
				array(
			'name'	=> 'Header Background Image (Optional)',
			'id'	=> $prefix . 'post_header_image',
			'type'	=> 'plupload_image',
			'max_file_uploads' => 1,
			'std'		=> ''
		), 
					array(
			'name'	=> 'Header Background Color',
			'id'	=> $prefix . 'post_header_bg_color',
			'type'	=> 'color',
			'std'		=> '#000000'
		),
					array(
			'name'	=> 'Heading Font Color',
			'id'	=> $prefix . 'post_heading_font_color',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
		
				array(
			'name'	=> 'Breadcrums Color #1',
			'id'	=> $prefix . 'post_breadcrums1_color',
			'type'	=> 'color',
			'std'		=> '#dc143c'
		),
		
					array(
			'name'	=> 'Breadcrums Color #2',
			'id'	=> $prefix . 'post_breadcrums2_color',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
	)
);



/*  FEATURED SLIDER */
$hazel_meta_boxes[] = array(
	'id'		=> $prefix . 'blog-gallery',
	'title'		=> 'Featured Slider Settings',
	'pages'		=> array( 'post' ),
	'context' => 'normal',

	
	'fields'	=> array(

		array(
			'name'	=> 'Slider Imaes',
			'id'	=> $prefix . 'featured_slides',
			'type'	=> 'plupload_image',
			'max_file_uploads' => 10,
		)
		
	)
);

/*  FEATURED QUOTE */

$hazel_meta_boxes[] = array(
	'id' => $prefix . 'blog-quote',
	'title' => 'Featured Quote Settings',
	'pages' => array( 'post'),
	'context' => 'normal',

	'fields' => array(
	

			
		array(
			'name'		=> 'Quote',
			'id'		=> $prefix . 'featured_quote',
			'clone'		=> false,
			'type'		=> 'textarea',
			'std'		=> ''
		),
		array(
			'name'		=> 'Source',
			'id'		=> $prefix . 'featured_source',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> ''
		),
	)
);
/*  FEATURED AUDIO */

$hazel_meta_boxes[] = array(
	'id' => $prefix . 'blog-audio',
	'title' => 'Featured Audio Settings',
	'pages' => array( 'post'),
	'context' => 'normal',

	'fields' => array(	
	
	
		array(
			'name'		=> 'Audio Code',
			'id'		=> $prefix . 'featured_audio',
			'desc'		=> 'Enter Audio Embed Code.',
			'clone'		=> false,
			'type' 	=> 'textarea',
			'std' 	=> ""
		),
	)
);

/*  FEATURED VIDEO */

$hazel_meta_boxes[] = array(
	'id'		=> $prefix . 'blog-video',
	'title'		=> 'Featured Video Settings',
	'pages'		=> array( 'post' ),
	'context' => 'normal',

	'fields'	=> array(
		
		array(
			'name'		=> 'Type',
			'id'		=> $prefix . 'featured_video',
			'type'		=> 'select',
			'options'	=> array(
				'vimeo'		=> 'Vimeo',
				'youtube'	=> 'Youtube',
				'other'		=> 'Own Embed Code'
			),
			'multiple'	=> false,
			'std'		=> array( 'no' )
		),
		array(
			'name'	=> 'Video ID / Embed Code',
			'id'	=> $prefix . 'video_embed',
			'desc'	=> 'Paste video ID (http://www.youtube.com/watch?v=<b>nzY2Qcu5i2A</b>) or your own Embed Code.',
			'type' 	=> 'textarea',
			'std' 	=> ""
		)
	)
);



/* ----------------------------------------------------- */
/* PORTFOLIO SETTINGS
/* ----------------------------------------------------- */

// Portfolio Main settings 

$hazel_meta_boxes[] = array(
	'id' => 'portfolio_page',
	'title' => 'Portfolio Page Options',
	'pages' => array( 'portfolio' ),
	'context' => 'normal',	

	'fields' => array(
	
		array(
			'name'		=> 'Active Menu Item',
			'id'		=> $prefix . 'active_menu_item',
			'type'		=> 'post',
			'field_type' => 'select',
			'post_type'  => 'nav_menu_item',
			'options' => array(
				'args' => array(
					'name'  =>  'id',
				)
				),
		),

		
	
				array(
			'name'		=> 'Alternative Page Title (optional)',
			'id'		=> $prefix . 'alt_portfolio_title',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> 'the sp<span>i</span>nner'
		),
		
		
				array(
			'name'		=> 'Page title font size',
			'id'		=> $prefix . "portfolio_title_font_size",
			'clone'		=> false,
			'type'		=> 'select',
			'options'	=> array(
				'70px'		=> '70px',
				'60px'		=> '60px',
				'50px'		=> '50px',
				'40px'		=> '40px',
				'30px'		=> '30px',
				'20px'		=> '20px',
			),
			'multiple'	=> false,
		),
		
		
			array(
			'name'		=> 'Page Subtitle (optional)',
			'id'		=> $prefix . 'portfolio_subtitle',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> 'welcome! we are a creative digital agency'
		),
		



		
		
				array(
			'name'		=> 'Include Full Width Slider',
			'id'		=> $prefix . "portfolio_fw_slider",
			'clone'		=> false,
			'type' => 'checkbox',
			'std'  => 0,
		),
		
		array(
			'name'		=> 'Fullwidth Slider Alias (optional)',
			'id'		=> $prefix . 'portfolio_slider_shortcode',
			'desc'		=> 'Alias is a part of rev_slider shortcode. Eg. alias of [rev_slider slider5] is slider5.',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> 'slider5'
		),
		

		
			array(
			'name'		=> 'Page Header Type',
			'id'		=> $prefix . "portfolio_header_type",
			'clone'		=> false,
			'type'		=> 'select',
			'options'	=> array(
				'color'		=> 'Color',
				'image'		=> 'Image'
			),
			'multiple'	=> false,
		),
		
		
					array(
			'name'		=> 'Use page header overlap',
			'id'		=> $prefix . "portfolio_header_overlap",
			'clone'		=> false,
			'type'		=> 'select',
			'options'	=> array(
				'yes'	=> 'Yes',
				'no'		=> 'No',
			),
			'multiple'	=> false,
		),
		
			array(
			'name'	=> 'Page Header Overlap Background Color',
			'id'	=> $prefix . 'portfolio_header_overlap_bg',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
		
		
		
				array(
			'name'	=> 'Header Background Image (Optional)',
			'id'	=> $prefix . 'portfolio_header_image',
			'type'	=> 'plupload_image',
			'max_file_uploads' => 1,
			'std'		=> ''
		), 
					array(
			'name'	=> 'Header Background Color',
			'id'	=> $prefix . 'portfolio_header_bg_color',
			'type'	=> 'color',
			'std'		=> '#000000'
		),
					array(
			'name'	=> 'Heading Font Color',
			'id'	=> $prefix . 'portfolio_heading_font_color',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
		
				array(
			'name'	=> 'Breadcrums Color #1',
			'id'	=> $prefix . 'portfolio_breadcrums1_color',
			'type'	=> 'color',
			'std'		=> '#dc143c'
		),
		
					array(
			'name'	=> 'Breadcrums Color #2',
			'id'	=> $prefix . 'portfolio_breadcrums2_color',
			'type'	=> 'color',
			'std'		=> '#ffffff'
		),
	
	)
);

$hazel_meta_boxes[] = array(
	'id' => 'portfolio_item',
	'title' => 'Portfolio Item Details',
	'pages' => array( 'portfolio' ),
	'context' => 'normal',	

	'fields' => array(

		
		array(
			'name'		=> 'Item Type',
			'id'		=> $prefix . 'portfolio_item_type',
			'type'		=> 'select',
			'options'	=> array(
				'image'		=> 'Image',
				'slider'	=> 'Slider',
				'video'		=> 'Video'
			),
			'multiple'	=> false,
			'std'		=> array( 'no' )
		),

	)
);


// Portfolio Slider Settings

$hazel_meta_boxes[] = array(
	'id'		=> $prefix . 'portfolio-gallery',
	'title'		=> 'Project Slider Settings',
	'pages'		=> array( 'portfolio' ),
	'context' => 'normal',

	'fields'	=> array(

		
		array(
			'name'	=> 'Slider Imaes',
			'id'	=> $prefix . 'portfolio_slides',
			'type'	=> 'plupload_image',
			'max_file_uploads' => 10,
		)
		
	)
);


// Portfolio Video settings

$hazel_meta_boxes[] = array(
	'id'		=> $prefix . 'fortfolio-video',
	'title'		=> 'Portfolio Video Settings',
	'pages'		=> array( 'portfolio' ),
	'context' => 'normal',

	'fields'	=> array(

		
		array(
			'name'		=> 'Type',
			'id'		=> $prefix . 'portfolio_video_type',
			'type'		=> 'select',
			'options'	=> array(
				'vimeo'		=> 'Vimeo',
				'youtube'	=> 'Youtube',
			),
			'multiple'	=> false,
			'std'		=> array( 'no' )
		),
		array(
			'name'	=> 'Video ID / Embed Code',
			'id'	=> $prefix . 'portfolio_video_embed',
			'desc'	=> 'Paste video ID only (http://www.youtube.com/watch?v=<b>nzY2Qcu5i2A</b>)',
			'type' 	=> 'textarea',
			'std' 	=> ""
		)
	)
);




function hazel_metaboxes()
{
	global $hazel_meta_boxes;
	if ( class_exists( 'RW_Meta_Box' ) )
	{
		foreach ( $hazel_meta_boxes as $meta_box )
		{
			new RW_Meta_Box( $meta_box );
		}
	}
}
add_action( 'admin_init', 'hazel_metaboxes' );