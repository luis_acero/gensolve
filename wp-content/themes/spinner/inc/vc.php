<?php


// Recent projects

add_action( 'vc_before_init', 'monte_recent_projects_integrateWithVC' );

function monte_recent_projects_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Recent Projects', 'monte' ),
	'base' => 'monte_recent_projects',
	'show_settings_on_create' => true,
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),

	'params' => array(

		array(
			'type' => 'dropdown',
			'heading' => __( 'Number of projects to display', 'monte' ),
			'param_name' => 'amount',
			'value'      => array(
				__( '4', 'monte' )       => '4',
				__( '4 + load more', 'monte' )       => '4lm',
				__( '8', 'monte' )       => '8',
				__( '8 + loadmore', 'monte' )       => '8lm',
			),
			),
		 



	),
));
}



if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_Recent_Projects extends WPBakeryShortCode {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
        	'amount' => '8lm',

        ), $atts));

		$output = '';
		
		
		$output .= '<div class="row">';
		$output .= '<div class="col-md-12 padding-bottom-20 wow align-center fadeIn">';
			$portfolio_categories = get_terms('portfolio_categories');
			if ($portfolio_categories):
		$output .= '<ul class="portfolio-buttons wow fadeIn" id="clearajax">';
		$output .= '<li class="active">';
		$output .= '<a href="#" class="all">' . __( 'aLL', 'monte' ) . '</a>';
		$output .= '</li>';
			foreach ($portfolio_categories as $portfolio_category):
		$output .= '<li><a href="#" class="filter-'.esc_attr($portfolio_category -> slug).'">'.esc_attr($portfolio_category -> name).'</a></li>';
			endforeach; 
		$output .= '</ul>';
			endif; 
		$output .= '</div>';
		$output .= '</div>';
				
				
		
		$output .= '<div class="row">';
		$output .= '<div class="col-md-12">';
		$output .= '<div id="single-home-container"></div>';
		$output .= '</div></div>';
	
		$output .= '<div class="row padding-top-40 portfolioHolder" id="portfolioholder">';
		$output .= '<div id="wrapper-folio">';

		
		if (($amount == '4lm') || ($amount == '8lm')) { $amount1 = '-1' ;} else {
			$amount1 = $amount;
		}
		if ($amount == '4lm') {
			$additional = 'four';
		} else {
			$additional = 'eight';
		}
		
					$query_recentprojects = new WP_Query('post_type=portfolio &posts_per_page='.$amount1.'');
					if($query_recentprojects->have_posts()): while($query_recentprojects->have_posts()) : $query_recentprojects->the_post(); 
					$portfolio_title = get_the_title();
			
					
					$postid = get_the_ID();
			
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($postid), 'portfolio-thumb' );
					$url = $thumb['0'];
					
					$categories = get_the_terms( get_the_ID(), 'portfolio_categories' );
					if($categories) : 
						$slug = '';
						foreach ($categories as $category) {
						$slug .= 'filter-';
						$slug .= $category->slug;
						$slug .= ' ';
					} endif;	
					
		
		
			$output .= '<div class="col-md-3 '.$additional.' portfolio single portfolioitems hidden portfolio-standalone" data-id="id-'.$postid.'" data-type="'.esc_attr($slug).'">';

			$output .= '<div class="portfolio-picture" style="background-image:url('.esc_url($url).')">';
			$output .= '<div class="portfolio-hover fol">';
			$output .= '<h6 class="categories">'.strip_tags ( get_the_term_list( $postid, 'portfolio_categories', " ",", " ) ).'</h6>';
			$output .= '<h2 class="project-name">'.get_the_title().'</h2>';
									
									
			$output .= '<a href="'.wp_get_attachment_url(get_post_thumbnail_id($postid)).'" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>';
			
			
			$output .= '<a href="'.get_the_permalink().'" class="trick"><i class="fa fa-unlink"></i></a>';
									
									
			$output .= '</div>';
			$output .= '</div>';
			$output .= '</div>';

	
		endwhile;
		endif;

		
		$output .= '</div>';
		$output .= '</div>';

		if (($amount == '4lm') || ($amount == '8lm')) {
			$output .= '<div class="row">';
			$output .= '<div class="col-md-12 align-center padding-top-50" id="loadmore">';
			$output .= '<a href="#" class="button dark '.$additional.' col3">' . __( 'MORE PROJECTS', 'monte' ) . '</a>';
			$output .= '</div>';
			$output .= '</div>';
		}
		
   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}


// Recent posts

add_action( 'vc_before_init', 'monte_recent_posts_integrateWithVC' );

function monte_recent_posts_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Recent Posts', 'monte' ),
	'base' => 'monte_recent_posts',
	'show_settings_on_create' => true,
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),

	'params' => array(
	
	array(
			'type' => 'dropdown',
			'heading' => __( 'Number of posts to display', 'monte' ),
			'param_name' => 'amount',
			'value'      => array(
				__( '3', 'monte' )       => '3',
				__( '4', 'monte' )        => '4',
			),
			),



			array(
	'type' => 'dropdown',
	'heading' => __( 'Posts CSS Animation', 'monte' ),
	'param_name' => 'css_animation',
	'admin_label' => true,
	'value' => array(
		__( 'No', 'monte' ) => '',
		__( 'Top to bottom', 'monte' ) => 'top-to-bottom',
		__( 'Bottom to top', 'monte' ) => 'bottom-to-top',
		__( 'Left to right', 'monte' ) => 'left-to-right',
		__( 'Right to left', 'monte' ) => 'right-to-left',
		__( 'Appear from center', 'monte' ) => "appear",
		__( 'Fade In', 'monte' ) => "fadeIn"
	),
	'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'monte' )
),

	),
));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_Recent_Posts extends WPBakeryShortCode {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'amount' => '3',
            'css_animation' => '',
        ), $atts));

		if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
			$css_class = $this->getCSSAnimation($css_animation);
		}
		$output = '';
		
		
		$sticky_posts = get_option( 'sticky_posts' );
		
		if (is_array($sticky_posts)){
			$sticky_count = count($sticky_posts);
			if ($sticky_count >= $amount) {
				$amountx = $amount;
			} else {
				$amountx = $amount - $sticky_count;
			}
		} else {
			$amount2x = $amount;
		}
		
		$output .= '<div class="row">';
		$query_recentposts = new WP_Query("post_type=post &posts_per_page=$amountx");
		if($query_recentposts->have_posts()): while($query_recentposts->have_posts()) : $query_recentposts->the_post(); 		
		
		if (is_sticky()) {
			$extraclass = 'mainstickypost';
		} else $extraclass = '';

		$postid = get_the_ID();
		ob_start();
		
		get_template_part( 'post-formats/single-recent');	
		
		$template = ob_get_contents ();
		
		ob_end_clean();


	
		if ($amount == '4') {
			$output .= '<div class="col-md-3 '.esc_attr($extraclass).'">';
			
		} else if ($amount == '3') {
			
			$output .= '<div class="col-md-4 '.esc_attr($extraclass).'">';
	
		}		

		$output .= $template;
		

		
		
		$output .= '<div class="news-content">';
		$output .= '<a href="'.get_the_permalink().'"><h1>'. get_the_title() .'</h1></a>';
		$output .= '<span class="newsinfo">' . __('by', 'monte') . ' ' . get_the_author_link() . ' ' . __('in ', 'monte') . ' ' . get_the_category_list(', ', 'single', $postid) . '</span>';
		$output .= '<p class="news-excerpt">';
		$output .= monte_get_frontpage_excerpt();
		$output .= '</p>';
		$output .= '</div>';
		
		
		
		$output .= '</div>';
		
		
		

		
		endwhile;
		endif;

		$output .= '</div>';
		


		
   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}




//  Dropcap

add_action( 'vc_before_init', 'monte_dropcap_integrateWithVC' );

function monte_dropcap_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Dropcap', 'monte' ),
	'base' => 'monte_dropcap',
	'show_settings_on_create' => true,
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),

	'params' => array(
	
	array(
			'type' => 'dropdown',
			'heading' => __( 'Style', 'monte' ),
			'param_name' => 'style',
			'value'      => array(
				__( 'Primary Accent Color', 'monte' )       => 'dropcap',
				__( 'Secondary Accent Color', 'monte' )        => 'dropcap-dark',
				__( 'Primary Accent Color - Circle', 'monte' )       => 'dropcap-background',
				__( 'Secondary Accent Color - Circle', 'monte' )        => 'dropcap-dark-background',
				__( 'Primary Accent Color - Square', 'monte' )       => 'dropcap-square',
				__( 'Secondary Accent Color - Square', 'monte' )        => 'dropcap-dark-square',
			),
			),

		array(
			'type' => 'textfield',
			'heading' => __( 'Dropcap Letter', 'monte' ),
			'param_name' => 'letter',
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Content', 'monte' ),
			'param_name' => 'text',
		),

			array(
	'type' => 'dropdown',
	'heading' => __( 'CSS Animation', 'monte' ),
	'param_name' => 'css_animation',
	'admin_label' => true,
	'value' => array(
		__( 'No', 'monte' ) => '',
		__( 'Top to bottom', 'monte' ) => 'top-to-bottom',
		__( 'Bottom to top', 'monte' ) => 'bottom-to-top',
		__( 'Left to right', 'monte' ) => 'left-to-right',
		__( 'Right to left', 'monte' ) => 'right-to-left',
		__( 'Appear from center', 'monte' ) => "appear",
		__( 'Fade In', 'monte' ) => "fadeIn"
	),
	'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'monte' )
),

	),
));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_Dropcap extends WPBakeryShortCode {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'style' => 'dropcap',
            'letter' => '',
            'text' => '',
            'css_animation' => '',
        ), $atts));
		
		

		
		if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
			$css_class = $this->getCSSAnimation($css_animation);
		}
		$output = '';

		$output .= '<p class="'.esc_attr($css_class).'">';
		$output .= '<span class="'.esc_attr($style).'">'.esc_attr($letter).'</span>';
		$output .= ' '.esc_attr($text).' ';
		$output .= '</p>';
		
   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}




//  Counters

add_action( 'vc_before_init', 'monte_counter_integrateWithVC' );

function monte_counter_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Counters', 'monte' ),
	'base' => 'monte_counter',
	'show_settings_on_create' => true,
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),

	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => __( 'Color scheme', 'monte' ),
			'param_name' => 'color',
			'value'      => array(
				__( 'Dark + Dark Icon - for light background', 'monte' )       => 'dark_dark',
				__( 'Dark + Accent Icon - for light background', 'monte' )       => 'dark_accent',
				__( 'Light + Light Icon - for dark background', 'monte' )        => 'light_light',
				__( 'Light + Accent Icon - for dark background', 'monte' )        => 'light_accent',
			),
			),

		array(
			'type' => 'textfield',
			'heading' => __( 'Caption', 'monte' ),
			'param_name' => 'caption',
		),
			array(
			'type' => 'textfield',
			'heading' => __( 'Number to animate', 'monte' ),
			'param_name' => 'number',
		),
		
		array(
			'type' => 'textfield',
			'heading' => __( 'Fontawesome Icon', 'monte' ),
			'param_name' => 'icon',
			'description' => __( 'Enter a fornawesome icon code, e.g. "fa-music". See the cheatsheet <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet/" target=blank>here.</a>', 'monte' )
		),


	),
));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_counter extends WPBakeryShortCode {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'color' => 'dark_dark',
            'caption' => '',
            'number' => '',
            'icon' => '',
        ), $atts));
		
		
		$output = '';
		
		if ($color == 'dark_dark') { $colorclass = 'counter_dark_dark' ; }
		else if ($color == 'dark_accent') { $colorclass = 'counter_dark_accent' ; }
		else if ($color == 'light_light') { $colorclass = 'counter_light_light' ; }
		else if ($color == 'light_accent') { $colorclass = 'counter_light_accent' ; }
	

		$output .= '<div class="counter '.esc_attr($colorclass).'">';
		$output .= '<div class="counter-number" data-number="'.esc_attr($number).'">';
		$output .= '<h1>0</h1></div>';
		
		$output .= '<span><i class="fa '.esc_attr($icon).'"></i>';
		$output .= ''.esc_attr($caption).'</span>';
		$output .= '</div>';
		
					

        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}






//  Inner Heading

add_action( 'vc_before_init', 'monte_heading_integrateWithVC' );

function monte_heading_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Monte Heading', 'monte' ),
	'base' => 'monte_heading',
	'show_settings_on_create' => true,
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),

	'params' => array(
	


		array(
			'type' => 'textfield',
			'heading' => __( 'Headline Text', 'monte' ),
			'param_name' => 'text',
		),
				
		array(
			'type' => 'colorpicker',
			'heading' => __( 'Headline color', 'monte' ),
			'param_name' => 'color',
			"value" => '#000000',
		),
		
		array(
			'type' => 'colorpicker',
			'heading' => __( 'Dash color', 'monte' ),
			'param_name' => 'dash',
			"value" => '#dc143c',
		),
		
			array(
			'type' => 'textfield',
			'heading' => __( 'Subeadline Text', 'monte' ),
			'param_name' => 'subtext',
		),
				
		array(
			'type' => 'colorpicker',
			'heading' => __( 'Subheadlie color', 'monte' ),
			'param_name' => 'subcolor',
			"value" => '#757575',
		),
		
		
			
		
			array(
	'type' => 'dropdown',
	'heading' => __( 'CSS Animation', 'monte' ),
	'param_name' => 'css_animation',
	'admin_label' => true,
	'value' => array(
		__( 'No', 'monte' ) => '',
		__( 'Top to bottom', 'monte' ) => 'top-to-bottom',
		__( 'Bottom to top', 'monte' ) => 'bottom-to-top',
		__( 'Left to right', 'monte' ) => 'left-to-right',
		__( 'Right to left', 'monte' ) => 'right-to-left',
		__( 'Appear from center', 'monte' ) => "appear",
		__( 'Fade In', 'monte' ) => "fadeIn"
	),
	'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'monte' )
),

	),
));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_heading extends WPBakeryShortCode {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'text' => '',
            'color' => '#000000',
            'dash' => '#dc143c',
            'subtext' => '',
            'subcolor' => '#757575',
            'css_animation' => '',
        ), $atts));
		
		

		
		if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
			$css_class = $this->getCSSAnimation($css_animation);
		}
		$output = '';
		
		$output .= '<div class="page-heading-wrapper">';
		
		$output .= '<div class="page-heading '.esc_attr($css_class).'">';
		$output .= '<h2 style="color: '.esc_attr($color).'">'.esc_attr($text).'</h2>';
		$output .= '<div class="dash" style="background: '.esc_attr($dash).'"></div>';
		$output .= '</div>';
		$output .= '<p style="color: '.esc_attr($subcolor).'">'.esc_attr($subtext).'</p>';
		$output .= '</div>';
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}

//  Progress bar

add_action( 'vc_before_init', 'monte_progress_round_integrateWithVC' );

function monte_progress_round_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Progress Bar - Round', 'monte' ),
	'base' => 'monte_progress_round',
	'show_settings_on_create' => true,
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),

	'params' => array(
	
	array(
			'type' => 'dropdown',
			'heading' => __( 'Color Scheme', 'monte' ),
			'param_name' => 'color',
			'value'      => array(
				__( 'Light - for dark background', 'monte' )       => 'light',
				__( 'Dark - for light background', 'monte' )        => 'dark',
			),
			),
			
	array(
			'type' => 'dropdown',
			'heading' => __( 'Percentage', 'monte' ),
			'param_name' => 'percentage',
			'value'      => array(
				__( '100', 'monte' )       => '100',
				__( '99', 'monte' )       => '99',
				__( '98', 'monte' )       => '98',
				__( '97', 'monte' )       => '97',
				__( '96', 'monte' )       => '96',
				__( '95', 'monte' )       => '95',
				__( '94', 'monte' )       => '94',
				__( '93', 'monte' )       => '93',
				__( '92', 'monte' )       => '92',
				__( '91', 'monte' )       => '91',
				__( '90', 'monte' )       => '90',
				__( '89', 'monte' )       => '89',
				__( '88', 'monte' )       => '88',
				__( '87', 'monte' )       => '87',
				__( '86', 'monte' )       => '86',
				__( '85', 'monte' )       => '85',
				__( '84', 'monte' )       => '84',
				__( '83', 'monte' )       => '83',
				__( '82', 'monte' )       => '82',
				__( '81', 'monte' )       => '81',
				__( '80', 'monte' )       => '80',
				__( '79', 'monte' )       => '79',
				__( '78', 'monte' )       => '78',
				__( '77', 'monte' )       => '77',
				__( '76', 'monte' )       => '76',
				__( '75', 'monte' )       => '75',
				__( '74', 'monte' )       => '74',
				__( '73', 'monte' )       => '73',
				__( '72', 'monte' )       => '72',
				__( '71', 'monte' )       => '71',
				__( '70', 'monte' )       => '70',
				__( '69', 'monte' )       => '69',
				__( '68', 'monte' )       => '68',
				__( '67', 'monte' )       => '67',
				__( '66', 'monte' )       => '66',
				__( '65', 'monte' )       => '65',
				__( '64', 'monte' )       => '64',
				__( '63', 'monte' )       => '63',
				__( '62', 'monte' )       => '62',
				__( '61', 'monte' )       => '61',
				__( '60', 'monte' )       => '60',
				__( '59', 'monte' )       => '59',
				__( '58', 'monte' )       => '58',
				__( '57', 'monte' )       => '57',
				__( '56', 'monte' )       => '56',
				__( '55', 'monte' )       => '55',
				__( '54', 'monte' )       => '54',
				__( '53', 'monte' )       => '53',
				__( '52', 'monte' )       => '52',
				__( '51', 'monte' )       => '51',
				__( '50', 'monte' )       => '50',
				__( '49', 'monte' )       => '49',
				__( '48', 'monte' )       => '48',
				__( '47', 'monte' )       => '47',
				__( '46', 'monte' )       => '46',
				__( '45', 'monte' )       => '45',
				__( '44', 'monte' )       => '44',
				__( '43', 'monte' )       => '43',
				__( '42', 'monte' )       => '42',
				__( '41', 'monte' )       => '41',
				__( '40', 'monte' )       => '40',
				__( '39', 'monte' )       => '39',
				__( '38', 'monte' )       => '38',
				__( '37', 'monte' )       => '37',
				__( '36', 'monte' )       => '36',
				__( '35', 'monte' )       => '35',
				__( '34', 'monte' )       => '34',
				__( '33', 'monte' )       => '33',
				__( '32', 'monte' )       => '32',
				__( '31', 'monte' )       => '31',
				__( '30', 'monte' )       => '30',
				__( '29', 'monte' )       => '29',
				__( '28', 'monte' )       => '28',
				__( '27', 'monte' )       => '27',
				__( '26', 'monte' )       => '26',
				__( '25', 'monte' )       => '25',
				__( '24', 'monte' )       => '24',
				__( '23', 'monte' )       => '23',
				__( '22', 'monte' )       => '22',
				__( '21', 'monte' )       => '21',
				__( '20', 'monte' )       => '20',
				__( '19', 'monte' )       => '19',
				__( '18', 'monte' )       => '18',
				__( '17', 'monte' )       => '17',
				__( '16', 'monte' )       => '16',
				__( '15', 'monte' )       => '15',
				__( '14', 'monte' )       => '14',
				__( '13', 'monte' )       => '13',
				__( '12', 'monte' )       => '12',
				__( '11', 'monte' )       => '11',
				__( '10', 'monte' )       => '10',
				__( '9', 'monte' )       => '9',
				__( '8', 'monte' )       => '8',
				__( '7', 'monte' )       => '7',
				__( '6', 'monte' )       => '6',
				__( '5', 'monte' )       => '5',
				__( '4', 'monte' )       => '4',
				__( '3', 'monte' )       => '3',
				__( '2', 'monte' )       => '2',
				__( '1', 'monte' )       => '1',
			),
			),
			

		array(
			'type' => 'textfield',
			'heading' => __( 'Heading', 'monte' ),
			'param_name' => 'heading',
		),
		array(
			'type' => 'textarea',
			'heading' => __( 'Description', 'monte' ),
			'param_name' => 'description',
		),
		
		array(
			'type' => 'colorpicker',
			'heading' => __( 'Bar color', 'monte' ),
			'param_name' => 'barcolor',
			"value" => '#000000',
		),

			array(
	'type' => 'dropdown',
	'heading' => __( 'CSS Animation', 'monte' ),
	'param_name' => 'css_animation',
	'admin_label' => true,
	'value' => array(
		__( 'No', 'monte' ) => '',
		__( 'Top to bottom', 'monte' ) => 'top-to-bottom',
		__( 'Bottom to top', 'monte' ) => 'bottom-to-top',
		__( 'Left to right', 'monte' ) => 'left-to-right',
		__( 'Right to left', 'monte' ) => 'right-to-left',
		__( 'Appear from center', 'monte' ) => "appear",
		__( 'Fade In', 'monte' ) => "fadeIn"
	),
	'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'monte' )
),

	),
));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_Progress_round extends WPBakeryShortCode {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'color' => 'light',
            'percentage' => '95',
            'heading' => '',
            'description' => '',
            'barcolor' => '#000000',
            'css_animation' => '',
        ), $atts));

		if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
			$css_class = $this->getCSSAnimation($css_animation);
		}
		$output = '';
		
		if ($color == 'light') {
			
		
			$output .= '<div class="progress align-center'.esc_attr($css_class).'">';

			$output .= '<div class="skill-chart">';			
			$output .= '<div class="chart-white" data-color="'.esc_attr($barcolor).'" data-percent="'.esc_attr($percentage).'">';		
			$output .= '<h1 class="white">'.esc_attr($percentage).'%</h1>';				
			$output .= '</div></div>';			
		
			$output .= '<h3 class="white">'.esc_attr($heading).'</h3>';			
			$output .= '<p>'.esc_attr($description).'</p>';
			$output .= '</div>';					
								
			
		} else if ($color == 'dark') {
			
			$output .= '<div class="progress align-center '.esc_attr($css_class).'">';
			
			$output .= '<div class="skill-chart">';			
			$output .= '<div class="chart" data-color="'.esc_attr($barcolor).'" data-percent="'.esc_attr($percentage).'">';		
			$output .= '<h1>'.esc_attr($percentage).'%</h1>';				
			$output .= '</div></div>';				

			$output .= '<h3>'.esc_attr($heading).'</h3>';			
			$output .= '<p>'.esc_attr($description).'</p>';
			$output .= '</div>';	
		} else {
			
			$output .= '<div class="progress align-center'.esc_attr($css_class).'">';

			$output .= '<div class="skill-chart">';			
			$output .= '<div class="chart-white" data-color="'.esc_attr($barcolor).'" data-percent="'.esc_attr($percentage).'">';		
			$output .= '<h1 class="white">'.esc_attr($percentage).'%</h1>';				
			$output .= '</div></div>';			
		
			$output .= '<h3 class="white">'.esc_attr($heading).'</h3>';			
			$output .= '<p>'.esc_attr($description).'</p>';
			$output .= '</div>';	
		}
		

				
		
   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}






//  Quote

add_action( 'vc_before_init', 'monte_blockquote_integrateWithVC' );

function monte_blockquote_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Blockquote', 'monte' ),
	'base' => 'monte_blockquote',
	'show_settings_on_create' => true,
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),

	'params' => array(
	
	array(
			'type' => 'dropdown',
			'heading' => __( 'Blockquote style', 'monte' ),
			'param_name' => 'style',
			'value'      => array(
				__( 'Style 1 - Align left - Accent color border - no background', 'monte' )       => 'style1',
				__( 'Style 2 - Align left - Black border - no background', 'monte' )        => 'style2',
				__( 'Style 3 - Align left - Accent color border - light background', 'monte' )        => 'style3',
				__( 'Style 4 - Align left - Black border - light background', 'monte' )        => 'style4',
				
				__( 'Style 5 - Align right - Accent color border - no background', 'monte' )       => 'style5',
				__( 'Style 6 - Align right - Black border - no background', 'monte' )        => 'style6',
				__( 'Style 7 - Align right - Accent color border - light background', 'monte' )        => 'style7',
				__( 'Style 8 - Align right - Black border - light background', 'monte' )        => 'style8',
			),
			),
			
	array(
			'type' => 'textarea',
			'heading' => __( 'Content', 'monte' ),
			'param_name' => 'quote_content',
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Footer', 'monte' ),
			'param_name' => 'footer',
		),

			array(
	'type' => 'dropdown',
	'heading' => __( 'CSS Animation', 'monte' ),
	'param_name' => 'css_animation',
	'admin_label' => true,
	'value' => array(
		__( 'No', 'monte' ) => '',
		__( 'Top to bottom', 'monte' ) => 'top-to-bottom',
		__( 'Bottom to top', 'monte' ) => 'bottom-to-top',
		__( 'Left to right', 'monte' ) => 'left-to-right',
		__( 'Right to left', 'monte' ) => 'right-to-left',
		__( 'Appear from center', 'monte' ) => "appear",
		__( 'Fade In', 'monte' ) => "fadeIn"
	),
	'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'monte' )
),

	),
));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_Blockquote extends WPBakeryShortCode {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(

            'style' => 'style1',
            'quote_content' => '',
            'footer' => '',
            'css_animation' => '',
        ), $atts));

		if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
			$css_class = $this->getCSSAnimation($css_animation);
		}
			
		$output = '';
		
		if ($style == 'style1') {
			
			$output .= '<blockquote class="'.esc_attr($css_class).'">';
			$output .= '<p>'.esc_attr($quote_content).'</p>';
			$output .= '<footer>'.esc_attr($footer).'</footer>';
			$output .= '</blockquote>';
			
		} else
		if ($style == 'style2') {
			
			$output .= '<blockquote class="blockquote-dark '.esc_attr($css_class).'">';
			$output .= '<p>'.esc_attr($quote_content).'</p>';
			$output .= '<footer>'.esc_attr($footer).'</footer>';
			$output .= '</blockquote>';
			
		} else
		if ($style == 'style3') {
			
			$output .= '<blockquote class="quote-bg-color '.esc_attr($css_class).'">';
			$output .= '<p>'.esc_attr($quote_content).'</p>';
			$output .= '<footer>'.esc_attr($footer).'</footer>';
			$output .= '</blockquote>';
			
			
		} else
		if ($style == 'style4') {
			
			$output .= '<blockquote class="blockquote-dark quote-bg-color '.esc_attr($css_class).'">';
			$output .= '<p>'.esc_attr($quote_content).'</p>';
			$output .= '<footer>'.esc_attr($footer).'</footer>';
			$output .= '</blockquote>';
			
		} else
		if ($style == 'style5') {
			
			$output .= '<blockquote class="blockquote-reverse '.esc_attr($css_class).'">';
			$output .= '<p>'.esc_attr($quote_content).'</p>';
			$output .= '<footer>'.esc_attr($footer).'</footer>';
			$output .= '</blockquote>';
			
		} else
		if ($style == 'style6') {
			
			$output .= '<blockquote class="blockquote-reverse dark '.esc_attr($css_class).'">';
			$output .= '<p>'.esc_attr($quote_content).'</p>';
			$output .= '<footer>'.esc_attr($footer).'</footer>';
			$output .= '</blockquote>';
			
		} else
		if ($style == 'style7') {
			
			$output .= '<blockquote class="blockquote-reverse quote-bg-color '.esc_attr($css_class).'">';
			$output .= '<p>'.esc_attr($quote_content).'</p>';
			$output .= '<footer>'.esc_attr($footer).'</footer>';
			$output .= '</blockquote>';
			
		} else
		if ($style == 'style8') {
			
			$output .= '<blockquote class="blockquote-reverse dark quote-bg-color '.esc_attr($css_class).'">';
			$output .= '<p>'.esc_attr($quote_content).'</p>';
			$output .= '<footer>'.esc_attr($footer).'</footer>';
			$output .= '</blockquote>';
			
		}

   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}





//  Buttons

add_action( 'vc_before_init', 'monte_button_integrateWithVC' );

function monte_button_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Button', 'monte' ),
	'base' => 'monte_button',
	'show_settings_on_create' => true,
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),

	'params' => array(
	array(
			'type' => 'textfield',
			'heading' => __( 'Title', 'monte' ),
			'param_name' => 'title',
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Url', 'monte' ),
			'param_name' => 'url',
		),
	array(
			'type' => 'dropdown',
			'heading' => __( 'Button Size', 'monte' ),
			'param_name' => 'size',
			'value'      => array(
				__( 'Small', 'monte' )       => 'small',
				__( 'Medium', 'monte' )       => 'medium',
				__( 'Large', 'monte' )        => 'large',
			),
			),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Button / Border Color', 'monte' ),
			'param_name' => 'color',
			'value'      => array(
				__( 'Black + Accent', 'monte' )       => 'black_accent',
				__( 'Black + Black', 'monte' )        => 'black_black',
				__( 'Black + White', 'monte' )        => 'black_white',
				__( 'White + Accent', 'monte' )       => 'white_accent',
				__( 'White + white', 'monte' )        => 'white_white',
				__( 'White + Black', 'monte' )        => 'white_black',
			),
			),

			array(
	'type' => 'dropdown',
	'heading' => __( 'CSS Animation', 'monte' ),
	'param_name' => 'css_animation',
	'admin_label' => true,
	'value' => array(
		__( 'No', 'monte' ) => '',
		__( 'Top to bottom', 'monte' ) => 'top-to-bottom',
		__( 'Bottom to top', 'monte' ) => 'bottom-to-top',
		__( 'Left to right', 'monte' ) => 'left-to-right',
		__( 'Right to left', 'monte' ) => 'right-to-left',
		__( 'Appear from center', 'monte' ) => "appear",
		__( 'Fade In', 'monte' ) => "fadeIn"
	),
	'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'monte' )
),

	),
));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_Button extends WPBakeryShortCode {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'title' => '',
            'url' => '',
            'size' => 'large',
            'color' => 'black_accent',
            'css_animation' => '',
        ), $atts));

		if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
			$css_class = $this->getCSSAnimation($css_animation);
		}
			

		$output = '';
		
		$output = '<a href="'.esc_url($url).'" class="button '.esc_attr($color).' '.esc_attr($size).' '.esc_attr($css_class).'">'.esc_attr($title).'</a>';
	
   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}




//  SERVICE

add_action( 'vc_before_init', 'monte_service_box_integrateWithVC' );

function monte_service_box_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Service Box Shortcode', 'monte' ),
	'base' => 'monte_service_box',
	'show_settings_on_create' => true,
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),

	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => __( 'Box Style', 'monte' ),
			'param_name' => 'type',
			'value'      => array(
				__( 'Top Icon - light background scheme', 'monte' )       => 'service1',
				__( 'Left Icon - light background scheme', 'monte' )        => 'service2',
				__( 'Top Icon - dark background scheme', 'monte' )       => 'service3',
				__( 'Left Icon - dark background scheme', 'monte', 'monte' )        => 'service4',
			),
			),
			array(
	'type' => 'dropdown',
	'heading' => __( 'CSS Animation', 'monte' ),
	'param_name' => 'css_animation',
	'admin_label' => true,
	'value' => array(
		__( 'No', 'monte' ) => '',
		__( 'Top to bottom', 'monte' ) => 'top-to-bottom',
		__( 'Bottom to top', 'monte' ) => 'bottom-to-top',
		__( 'Left to right', 'monte' ) => 'left-to-right',
		__( 'Right to left', 'monte' ) => 'right-to-left',
		__( 'Appear from center', 'monte' ) => "appear",
		__( 'Fade In', 'monte' ) => "fadeIn"
	),
	'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'monte' )
),
array(
			'type' => 'textfield',
			'heading' => __( 'Fontawesome Icon', 'monte' ),
			'param_name' => 'icon',
			'description' => __( 'Enter a fornawesome icon code, e.g. "fa-music". See the cheatsheet <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet/" target=blank>here.</a>', 'monte' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Service title', 'monte' ),
			'param_name' => "title"
		),
				array(

			'type' => 'textarea',
			'holder' => 'div',
			'heading' => __( 'Description', 'monte' ),
			'param_name' => "description"
		),
		
		
			array(
			'type' => 'dropdown',
			'heading' => __( 'Display Read more link', 'monte' ),
			'param_name' => 'readmore',
			'value'      => array(
				__( 'Yes', 'monte' )       => 'yes',
				__( 'No', 'monte' )        => 'no',
			),
			),
			
			
		  array(
			'type' => 'textfield',
			'heading' => __( 'Read More URL', 'monte' ),
			'param_name' => "rm_url"
		),
		
		 array(
			'type' => 'textfield',
			'heading' => __( 'Read More Text', 'monte' ),
			'param_name' => "rm_text"
		),
		
	),

//'js_view' => 'VcTeamMembersView'
));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_Service_Box extends WPBakeryShortCode {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'type' => 'service1',
            'css_animation' => '',
            'icon' => '',
            'title' => '',
            'description' => '',
            'readmore' => 'yes',
            'rm_url' => '',
            'rm_text' => ''
        ), $atts));

		if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
			$css_class = $this->getCSSAnimation($css_animation);
		}

		$output = '';
		
		if ($type =='service1') {
								
		$output .= '<div class="service '.esc_attr($css_class).'">';
		$output .= '<i class="fa ' . esc_attr($icon) . ' "></i>';
		$output .= '<h3>' . esc_attr($title) . '</h3>';
		$output .= '<p>' . esc_attr($description) . '</p>';
		if ($readmore == 'yes') {
			$output .= '<span><a href="' . esc_url($rm_url) . '">' . esc_attr($rm_text) . '</a></span>';
		}
		
		$output .= '</div>';
								
		
		} else if ($type =='service2') {
		
		$output .= '<div class="row service '.esc_attr($css_class).'">';
		$output .= '<div class="col-md-2 col-sm-2 col-xs-2">';
		$output .= '<div class="service-icon">';
		$output .= '<i class="fa ' . esc_attr($icon) . ' "></i>';
		$output .= '</div></div>';
		
		
		$output .= '<div class="col-md-10 col-sm-10 col-xs-10">';
		$output .= '<h3>' . esc_attr($title) . '</h3>';
		$output .= '<p>' . esc_attr($description) . '</p>';
		
		if ($readmore == 'yes') {
			$output .= '<span><a href="' . esc_url($rm_url) . '">' . esc_attr($rm_text) . '</a></span>';
		}
		$output .= '</div></div>';
						

		} else if ($type =='service3') {
			
		$output .= '<div class="service service_dark_bg '.esc_attr($css_class).'">';
		$output .= '<i class="fa ' . esc_attr($icon) . ' "></i>';
		$output .= '<h3>' . esc_attr($title) . '</h3>';
		$output .= '<p>' . esc_attr($description) . '</p>';
		if ($readmore == 'yes') {
			$output .= '<span><a href="' . esc_url($rm_url) . '">' . esc_attr($rm_text) . '</a></span>';
		}
		
		$output .= '</div>';

		} else if ($type =='service4') {
			
		$output .= '<div class="row service service_dark_bg '.esc_attr($css_class).'">';
		$output .= '<div class="col-md-2 col-sm-2 col-xs-2">';
		$output .= '<div class="service-icon">';
		$output .= '<i class="fa ' . esc_attr($icon) . ' "></i>';
		$output .= '</div></div>';
		
		
		$output .= '<div class="col-md-10 col-sm-10 col-xs-10">';
		$output .= '<h3>' . esc_attr($title) . '</h3>';
		$output .= '<p>' . esc_attr($description) . '</p>';
		
		if ($readmore == 'yes') {
			$output .= '<span><a href="' . esc_url($rm_url) . '">' . esc_attr($rm_text) . '</a></span>';
		}
		$output .= '</div></div>';
		

		}
   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}




//  SOCIAL BUTTONS

add_action( 'vc_before_init', 'monte_social_integrateWithVC' );

function monte_social_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Social Buttons Shortcode', 'monte' ),
	'base' => 'monte_social',
	'show_settings_on_create' => true,
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),

	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => __( 'Icon Style', 'monte' ),
			'param_name' => 'type',
			'value'      => array(
				__( 'Dark - light background scheme', 'monte' )       => 'dark',
				__( 'Light - dark background scheme', 'monte' )        => 'light',
			),
			),
			
			
	array(
			'type' => 'dropdown',
			'heading' => __( 'Icon Align', 'monte' ),
			'param_name' => 'centered',
			'value'      => array(
				__( 'Center', 'monte' )       => 'yes',
				__( 'Left', 'monte' )        => 'no',
			),
			),
			
			
			array(
	'type' => 'dropdown',
	'heading' => __( 'CSS Animation', 'monte' ),
	'param_name' => 'css_animation',
	'admin_label' => true,
	'value' => array(
		__( 'No', 'monte' ) => '',
		__( 'Top to bottom', 'monte' ) => 'top-to-bottom',
		__( 'Bottom to top', 'monte' ) => 'bottom-to-top',
		__( 'Left to right', 'monte' ) => 'left-to-right',
		__( 'Right to left', 'monte' ) => 'right-to-left',
		__( 'Appear from center', 'monte' ) => "appear",
		__( 'Fade In', 'monte' ) => "fadeIn"
	),
	'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'monte' )
),


		array(
			'type' => 'textfield',
			'heading' => __( 'Social Icon', 'monte' ),
			'param_name' => 'icon',
			'description' => __( 'Enter a fornawesome icon code, e.g. "fa-music". See the cheatsheet <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet/" target=blank>here.</a>', 'monte' )
		),
		
		array(
			'type' => 'textfield',
			'heading' => __( 'Social Profile Url', 'monte' ),
			'param_name' => "profile_url"
		),
	
		
	),

//'js_view' => 'VcTeamMembersView'
));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_Social extends WPBakeryShortCode {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'type' => 'dark',
            'css_animation' => '',
            'icon' => '',
            'rm_url' => '',
            'centered' => 'yes',
            'profile_url' => ''
        ), $atts));

		if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
			$css_class = $this->getCSSAnimation($css_animation);
		}

		$output = '';
		
		if ($type =='light') {
			$social_class = 'socialize-light';
			} else {
			$social_class = 'socialize-dark';
			}
								
		if ($centered =='yes') {
			$centered_class = 'centered';
			} else {
			$centered_class = 'none';
			}


		$output .= '<div class="socialize-icon '.esc_attr($css_class).' '.esc_attr($social_class).' '.esc_attr($centered_class).'">';
		$output .= '<a href="' . esc_url($rm_url) . '"><i class="fa ' . esc_attr($icon) . ' "></i></a>';
		$output .= '</div>';
								
		
   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}





//  Team Carousel

add_action( 'vc_before_init', 'monte_carousel_integrateWithVC' );

function monte_carousel_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Team Carousel Shortcode', 'monte' ),
	'base' => 'monte_carousel',
	'show_settings_on_create' => true,
	"as_parent" => array('only' => 'monte_carousel_item'),
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),

	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => __( 'Number of members to display at once', 'monte' ),
			'param_name' => 'items',
			'value'      => array(
				__( '1', 'monte' )       => '1',
				__( '2', 'monte' )       => '2',
				__( '3', 'monte' )       => '3',
				__( '4', 'monte' )       => '4',
				__( '5', 'monte' )       => '5',
				__( '6', 'monte' )       => '6',
		
			),
			),
			
			
	array(
			'type' => 'dropdown',
			'heading' => __( 'Pagination', 'monte' ),
			'param_name' => 'pagination',
			'value'      => array(
				__( 'Yes', 'monte' )       => 'yes',
				__( 'No', 'monte' )        => 'no',
			),
			),
			
			
	array(
			'type' => 'dropdown',
			'heading' => __( 'Arrows', 'monte' ),
			'param_name' => 'arrows',
			'value'      => array(
				__( 'Yes', 'monte' )       => 'yes',
				__( 'No', 'monte' )        => 'no',
			),
		),
			

		array(
			'type' => 'dropdown',
			'heading' => __( 'Color Scheme', 'monte' ),
			'param_name' => 'color',
			'value'      => array(
				__( 'Dark - for light background', 'monte' )       => 'dark',
				__( 'Light - for dark background', 'monte' )        => 'light',
			),
		),

	
		
	),
 "js_view" => 'VcColumnView',
//'js_view' => 'VcTeamMembersView'
));
}


if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_monte_Carousel extends WPBakeryShortCodesContainer  {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'items' => '4',
            'pagination' => 'yes',
            'arrows' => 'yes',
            'color' => 'dark'
        ), $atts));


		$output = '';
		
		if ($items =='1') {
			$item_count = '1';
		} else if ($items =='2') {
			$item_count = '2';
		} else if ($items =='3') {
			$item_count = '3';
		} else if ($items =='4') {
			$item_count = '4';
		} else if ($items =='5') {
			$item_count = '5';
		} else if ($items =='6') {
			$item_count = '6';
		}
								
		if ($pagination =='yes') {
			$displaypagination = 'true';
			} else {
			$displaypagination = 'false';
			}

		if ($arrows =='yes') {
			$displayarrows = 'true';
			} else {
			$displayarrows = 'false';
			}

		if ($color =='dark') {
			$scheme = 'carousel-dark';
			} else {
			$scheme = 'carousel-light';
			}
			
			
		$output .= '<div class="team-carousel '.esc_attr($scheme).'" data-carousel-items="'.esc_attr($item_count).'" data-carousel-pagination="'.esc_attr($displaypagination).'" data-carousel-arrows="'.esc_attr($displayarrows).'">';
		$output .=  wpb_js_remove_wpautop( $content );
		$output .= '</div>';
								
		
   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}






//  Testimonials

add_action( 'vc_before_init', 'monte_testimonials_carousel_integrateWithVC' );

function monte_testimonials_carousel_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Testimonials Carousel Shortcode', 'monte' ),
	'base' => 'monte_testimonials_carousel',
	'show_settings_on_create' => true,
	"as_parent" => array('only' => 'monte_testimonial'),
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),

	'params' => array(
			
	array(
			'type' => 'dropdown',
			'heading' => __( 'Pagination', 'monte' ),
			'param_name' => 'pagination',
			'value'      => array(
				__( 'Yes', 'monte' )       => 'yes',
				__( 'No', 'monte' )        => 'no',
			),
			),
			
			
	array(
			'type' => 'dropdown',
			'heading' => __( 'Arrows', 'monte' ),
			'param_name' => 'arrows',
			'value'      => array(
				__( 'Yes', 'monte' )       => 'yes',
				__( 'No', 'monte' )        => 'no',
			),
		),
			

		array(
			'type' => 'dropdown',
			'heading' => __( 'Color Scheme', 'monte' ),
			'param_name' => 'color',
			'value'      => array(
				__( 'Dark - for light background', 'monte' )       => 'dark',
				__( 'Light - for dark background', 'monte' )        => 'light',
			),
		),

	
		
	),
 "js_view" => 'VcColumnView',
//'js_view' => 'VcTeamMembersView'
));
}


if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_monte_testimonials_Carousel extends WPBakeryShortCodesContainer  {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'pagination' => 'yes',
            'arrows' => 'no',
            'color' => 'light'
        ), $atts));


		$output = '';
		
								
		if ($pagination =='yes') {
			$displaypagination = 'true';
			} else {
			$displaypagination = 'false';
			}

		if ($arrows =='yes') {
			$displayarrows = 'true';
			} else {
			$displayarrows = 'false';
			}

		if ($color =='dark') {
			$scheme = 'carousel-dark';
			} else {
			$scheme = 'carousel-light';
			}
			
			
		$output .= '<div class="testimonial-carousel '.esc_attr($scheme).'" data-carousel-pagination="'.esc_attr($displaypagination).'" data-carousel-arrows="'.esc_attr($displayarrows).'">';
		$output .=  wpb_js_remove_wpautop( $content );
		$output .= '</div>';
								
		
   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}






//  Team Member

add_action( 'vc_before_init', 'monte_carousel_item_integrateWithVC' );

function monte_carousel_item_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Team Member', 'monte' ),
	'base' => 'monte_carousel_item',
	'show_settings_on_create' => true,
	"as_child" => array('only' => 'monte_carousel'),
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),
	
	'params' => array(
	 array(
		'type' => 'textfield',
		'heading' => __( 'Name', 'monte' ),
		'param_name' => 'name',
		),
			
			
	 array(
		'type' => 'textfield',
		'heading' => __( 'Job Title', 'monte' ),
		'param_name' => 'job',
		),
		
		array(
  	    'type' => 'attach_image',
		'heading' => __( 'Image', 'monte' ),
		'param_name' => 'image',
		),
		
		
	 array(
		'type' => 'textfield',
		'heading' => __( 'Social Service Icon 1', 'monte' ),
		'param_name' => 'icon1',
		'description' => __( 'Enter a fornawesome icon code, e.g. "fa-music". See the cheatsheet <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet/" target=blank>here.</a>', 'monte' )
		),
		
	 array(
		'type' => 'textfield',
		'heading' => __( 'Social Service Url 1', 'monte' ),
		'param_name' => 'url1',
		),	
		
		
		 array(
		'type' => 'textfield',
		'heading' => __( 'Social Service Icon 2', 'monte' ),
		'param_name' => 'icon2',
		'description' => __( 'Enter a fornawesome icon code, e.g. "fa-music". See the cheatsheet <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet/" target=blank>here.</a>', 'monte' )
		),
		
	 array(
		'type' => 'textfield',
		'heading' => __( 'Social Service Url 2', 'monte' ),
		'param_name' => 'url2',
		),	
		
		 array(
		'type' => 'textfield',
		'heading' => __( 'Social Service Icon 3', 'monte' ),
		'param_name' => 'icon3',
		'description' => __( 'Enter a fornawesome icon code, e.g. "fa-music". See the cheatsheet <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet/" target=blank>here.</a>', 'monte' )
		),
		
	 array(
		'type' => 'textfield',
		'heading' => __( 'Social Service Url 3', 'monte' ),
		'param_name' => 'url3',
		),	
		
		 array(
		'type' => 'textfield',
		'heading' => __( 'Social Service Icon 4', 'monte' ),
		'param_name' => 'icon4',
		'description' => __( 'Enter a fornawesome icon code, e.g. "fa-music". See the cheatsheet <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet/" target=blank>here.</a>', 'monte' )
		),
		
	 array(
		'type' => 'textfield',
		'heading' => __( 'Social Service Url 4', 'monte' ),
		'param_name' => 'url4',
		)
		),
// "js_view" => 'VcColumnView',
//"js_view" => 'VcMembersView',
));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_Carousel_Item extends WPBakeryShortCode  {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'name' => '',
            'job' => '',
            'image' => '',
            'icon1' => '',
            'url1' => '',
             'icon2' => '',
            'url2' => '',
             'icon3' => '',
            'url3' => '',
             'icon4' => '',
            'url4' => '',
        ), $atts));


		$output = '';
		

		$output .= '<div class="team-member">';
		$output .= '<div class="team-member-inner">';
		$output .= '<div class="team-member-content">';
		$output .= '<span>'.esc_attr($job).'</span>';
		$output .= '<h1>'.esc_attr($name).'</h1>';
		if ($icon1 && $url1) {
			$output .= '<a href="' . esc_url($url1) . '"><i class="fa '.esc_attr($icon1).'"></i></a>';
		}
		if ($icon2 && $url2) {
			$output .= '<a href="' . esc_url($url2) . '"><i class="fa '.esc_attr($icon2).'"></i></a>';
		}
		if ($icon3 && $url3) {
			$output .= '<a href="' . esc_url($url3) . '"><i class="fa '.esc_attr($icon3).'"></i></a>';
		}
		if ($icon4 && $url4) {
			$output .= '<a href="' . esc_url($url4) . '"><i class="fa '.esc_attr($icon4).'"></i></a>';
		}
		$output .= '';
		$output .= '';
		$output .= '';
		$output .= '</div>';
		$output .= wp_get_attachment_image( $image, "team" );
		$output .= '</div>';
		$output .= '</div>';

		
   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}






//  Team Member

add_action( 'vc_before_init', 'monte_testimonial_integrateWithVC' );

function monte_testimonial_integrateWithVC() {
	vc_map( array(
	"name" => __( 'Testimonial', 'monte' ),
	'base' => 'monte_testimonial',
	'show_settings_on_create' => true,
	"as_child" => array('only' => 'monte_testimonials_carousel'),
	//'icon' => 'icon-wpb-ui-tab-content',
	'category' => __( 'Monte Shortcodes', 'monte' ),
	
	'params' => array(
	 array(
		'type' => 'textfield',
		'heading' => __( 'Content', 'monte' ),
		'param_name' => 'testimonial',
		),
			
			
	 array(
		'type' => 'textfield',
		'heading' => __( 'Author', 'monte' ),
		'param_name' => 'author',
		),

		),
// "js_view" => 'VcColumnView',
//"js_view" => 'VcMembersView',
));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_Testimonial extends WPBakeryShortCode  {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'testimonial' => '',
            'author' => '',
        ), $atts));


		$output = '';
		


		$output .= '<div class="testimonial">';
		$output .= '<i class="fa fa-quote-left"></i>';
		$output .= '<div class="testimonial-content">'.esc_attr($testimonial).'</div>';
		$output .= '<h6>'.esc_attr($author).'</h6></div>';

		
   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}


//  LISTS

add_action( 'vc_before_init', 'monte_list_integrateWithVC' );

function monte_list_integrateWithVC() {
	vc_map( array(
	"name" => __( 'List', 'monte' ),
	'base' => 'monte_list',
	'show_settings_on_create' => true,
	'is_container' => true,
	"as_parent" => array('only' => 'monte_list_item'),
	'category' => __( 'Monte Shortcodes', 'monte' ),
	'description' => __( 'List Shortcodes', 'monte' ),
	'params' => array(
		array(
	'type' => 'dropdown',
	'heading' => __( 'CSS Animation', 'monte' ),
	'param_name' => 'css_animation',
	'admin_label' => true,
	'value' => array(
		__( 'No', 'monte' ) => '',
		__( 'Top to bottom', 'monte' ) => 'top-to-bottom',
		__( 'Bottom to top', 'monte' ) => 'bottom-to-top',
		__( 'Left to right', 'monte' ) => 'left-to-right',
		__( 'Right to left', 'monte' ) => 'right-to-left',
		__( 'Appear from center', 'monte' ) => "appear",
		__( 'Fade In', 'monte' ) => "fadeIn"
	),
	'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'monte' )
),
	array(
			'type' => 'dropdown',
			'heading' => __( 'Icon type', 'monte' ),
			'param_name' => 'type',
			
			'value'      => array(
				__( 'Bullet', 'monte' )       => 'bullet-list',
				__( 'Circle Arrow', 'monte' )        => 'arrow-circle-o-right',
				__( 'Arrow', 'monte' )        => 'arrow',
				__( 'Chevron', 'monte' )        => 'chevron',
				__( 'Check', 'monte' )        => 'check',
				__( 'Check Circle', 'monte' )        => 'check-circle',
				__( 'Paw', 'monte' )        => 'paw',
				__( 'Check Square', 'monte' )        => 'check-square',
				__( 'X', 'monte' )        => 'xclose',
				__( 'Forward', 'monte' )        => 'forward',
				__( 'Toggle', 'monte' )        => 'toggles',
				__( 'X cicrle', 'monte' )        => 'times',
	
			),
			)
	),
				//	'custom_markup' => '<ul class="item-list"> %content% </ul>',
				'default_content' => '
				[monte_list_item item_content="Duis urna eros vulputate at"]
				[monte_list_item item_content="Duis urna eros vulputate at"]
				[monte_list_item item_content="Duis urna eros vulputate at"]
				[monte_list_item item_content="Duis urna eros vulputate at"]
				',
				'js_view' => 'VcListView'
				));
				}
				


add_action( 'vc_before_init', 'monte_list_item_integrateWithVC' );

function monte_list_item_integrateWithVC() {
vc_map( array(
	'name' => __( 'List Item', 'monte' ),
	'base' => 'monte_list_item',
	'is_container' => false,
	"content_element" => true,
    "as_child" => array('only' => 'monte_list'),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => __( 'List Item Content', 'monte' ),
			'param_name' => "item_content"
		)
	),
'js_view' => 'VcListItemView'
) );
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
$attributes = array(
    array(
        'type' => 'dropdown',
        'heading' => "Overlap",
        'param_name' => 'row_overlap',
		'value'      => array(
				__( 'None', 'monte' )       => 'none',
				__( 'Top', 'monte' )       => 'top',
				__( 'Bottom', 'monte' )        => 'bottom',
				__( 'Both', 'monte' )        => 'both',
			),
        'description' => __( "Set row overlap (usable only for full-width stretched row)", "monte" )
    ),
    
	  array(
            "type" => "colorpicker",
            "holder" => "div",
            "class" => "",
            "heading" => __("Top Overlap Background Color", "js_composer" ),
            "param_name" => "overlap_top_bg",
            "value" => '#ffffff',
         ),
         
	  array(
            "type" => "colorpicker",
            "holder" => "div",
            "class" => "",
            "heading" => __("Bottom Overlap Background Color", "js_composer" ),
            "param_name" => "overlap_bottom_bg",
            "value" => '#ffffff',
         ),
);
vc_add_params( 'vc_row', $attributes ); // Note: 'vc_message' was used as a base for "Message box" element

}



function vc_theme_before_vc_row($atts, $content = null) {
	extract( shortcode_atts( array(
	'row_overlap' => 'none',
	'overlap_top_bg' => '#ffffff',
	), $atts ) );

	$output = '';
	
	$output .= '<div style="position: relative">';
	if (($row_overlap == 'top') or ($row_overlap == 'both')) {
		$output .= '<div data-overlap-top="true" data-overlap-top-bg="'.esc_attr($overlap_top_bg).'" class="overlapped-top" style="background: '.esc_attr($overlap_top_bg).'"></div>';
	}
   
   return $output;
}

function vc_theme_after_vc_row($atts, $content = null) {
		extract( shortcode_atts( array(
	'row_overlap' => 'none',
	'overlap_bottom_bg' => '#ffffff',
), $atts ) );
	$output = '';
	if (($row_overlap == 'bottom') or ($row_overlap == 'both')) {
		$output .= '<div data-overlap-bottom="true" data-overlap-bottom-bg="'.esc_attr($overlap_bottom_bg).'" class="overlapped-bottom" style="background: '.esc_attr($overlap_bottom_bg).'"></div>';
	}
	$output .= '</div>';
   return $output;
}




//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_List extends WPBakeryShortCodesContainer {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'type' => '', 'accent' => '', 'css_animation' => ''
        ), $atts));
		if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
			$css_class = $this->getCSSAnimation($css_animation);
		}
		$output = '';
		$output .= '<ul class="item-list '.esc_attr($type).' '.esc_attr($accent).' '.esc_attr($css_class).'">';
		$output .= "\n\t\t\t" . wpb_js_remove_wpautop( $content );
		$output .= '</ul>';				
							
   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}
		

		
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_monte_List_Item extends WPBakeryShortCode {
    	
		protected function content($atts, $content = null) {

        extract(shortcode_atts(array(
            'item_content' => ''
        ), $atts));
		$output = '<li>'.esc_attr($item_content).'</li>';
							
   
        return $output;
    }
		
		public function __construct( $settings ) {
		parent::__construct( $settings );
	}

    }
}






$attributes_tabs = array(
	'type' => 'dropdown',
			'heading' => __( 'Header color', 'monte' ),
			'param_name' => 'accent',
			
			'value'      => array(
				__( 'Primary Accent Color', 'monte' )       => 'primary',
				__( 'Secondary Accent Color', 'monte' )        => 'secondary',
			)
);
vc_add_param('vc_tabs', $attributes_tabs);

$attributes_accordion = array(
	'type' => 'dropdown',
			'heading' => __( 'Accent color', 'monte' ),
			'param_name' => 'accent',
			
			'value'      => array(
				__( 'Primary Accent Color', 'monte' )       => 'primary',
				__( 'Secondary Accent Color', 'monte' )        => 'secondary',
			)
);
vc_add_param('vc_accordion', $attributes_accordion);


function vc_theme_before_vc_tabs($atts, $content = null) {
	extract( shortcode_atts( array(
	'accent' => '',
), $atts ) );
   return '<div class="tabs-'.esc_attr($accent).'">';
}

function vc_theme_after_vc_tabs($atts, $content = null) {
   return '</div>';
}


function vc_theme_before_vc_accordion($atts, $content = null) {
	extract( shortcode_atts( array(
	'accent' => '',
), $atts ) );
   return '<div class="accordion-'.esc_attr($accent).'">';
}

function vc_theme_after_vc_accordion($atts, $content = null) {
   return '</div>';
}



$attributes_accordion = array(
	'type' => 'dropdown',
			'heading' => __( 'Accent color', 'monte' ),
			'param_name' => 'accent',
			
			'value'      => array(
				__( 'Primary Accent Color', 'monte' )       => 'primary',
				__( 'Secondary Accent Color', 'monte' )        => 'secondary',
			)
);
vc_add_param('vc_toggle', $attributes_accordion);


function vc_theme_before_vc_toggle($atts, $content = null) {
	extract( shortcode_atts( array(
	'accent' => '',
), $atts ) );
   return '<div class="toggle-'.esc_attr($accent).'">';
}

function vc_theme_after_vc_toggle($atts, $content = null) {
   return '</div>';
}

$setting_tabs = array (
  'category' => 'Monte Shortcodes'
);
vc_map_update('vc_tabs', $setting_tabs);



vc_remove_element("vc_progress_bar");
vc_remove_element("vc_pie");
vc_remove_element("vc_media_grid");
vc_remove_element("vc_basic_grid");
vc_remove_element("vc_masonry_grid");
vc_remove_element("vc_masonry_media_grid");
vc_remove_element("vc_teaser_grid");
vc_remove_element("vc_posts_grid");
vc_remove_element("vc_carousel");
vc_remove_element("vc_posts_slider");
vc_remove_element("vc_button");
vc_remove_element("vc_button2");
vc_remove_element("vc_cta_button");
vc_remove_element("vc_cta_button2");
vc_remove_element("vc_cta");
vc_remove_element("vc_widget_sidebar");
vc_remove_element("vc_message");
//vc_remove_element("vc_separator");
//vc_remove_element("vc_text_separator");

?>