<?php
global $smof_data;
if (array_key_exists('custom_css_code', $smof_data)) {
	$custom_css = $smof_data['custom_css_code'];
} 


if (array_key_exists('preloader', $smof_data)) {
	$preloader = $smof_data['preloader'];
}
if (array_key_exists('preloader_inner', $smof_data)) {
	$preloader_inner = $smof_data['preloader_inner'];
}
if (array_key_exists('preloader_background', $smof_data)) {
	$preloader_background = $smof_data['preloader_background'];
}
if (array_key_exists('preloader_font', $smof_data)) {
	$preloader_font = $smof_data['preloader_font'];
}
if (array_key_exists('preloader_logo', $smof_data)) {
	$preloader_logo = $smof_data['preloader_logo'];
}


if (array_key_exists('fullscreen_slider', $smof_data)) {
	$fullscreen_slider = $smof_data['fullscreen_slider'];
}



if (array_key_exists('menu_position', $smof_data)) {
	$menu_position = $smof_data['menu_position'];
}
if (array_key_exists('menu_transparent', $smof_data)) {
	$menu_transparent = $smof_data['menu_transparent'];
}
if (array_key_exists('menu_bg_color', $smof_data)) {
	$menubg = $smof_data['menu_bg_color'];
}
if (array_key_exists('submenu_bg_color', $smof_data)) {
	$submenubg = $smof_data['submenu_bg_color'];
}
if (array_key_exists('submenu_borders_color', $smof_data)) {
	$submenu_borders = $smof_data['submenu_borders_color'];
}
if (array_key_exists('menu_font_color', $smof_data)) {
	$menu_font_color = $smof_data['menu_font_color'];
}
if (array_key_exists('submenu_font_color', $smof_data)) {
	$submenu_font_color = $smof_data['submenu_font_color'];
}
if (array_key_exists('submenu_hover_font_color', $smof_data)) {
	$submenu_hover_font_color = $smof_data['submenu_hover_font_color'];
}
if (array_key_exists('menu_font_face', $smof_data)) {
	$menu_font_face = $smof_data['menu_font_face'];
}
if (array_key_exists('submenu_font_face', $smof_data)) {
	$submenu_font_face = $smof_data['submenu_font_face'];
}
if (array_key_exists('menu_font_size', $smof_data)) {
	$menu_font_size = $smof_data['menu_font_size'];
}
if (array_key_exists('menu_font_weight', $smof_data)) {
	$menu_font_weight = $smof_data['menu_font_weight'];
}
if (array_key_exists('submenu_font_size', $smof_data)) {
	$submenu_font_size = $smof_data['submenu_font_size'];
}
if (array_key_exists('submenu_font_weight', $smof_data)) {
	$submenu_font_weight = $smof_data['submenu_font_weight'];
}





if (array_key_exists('accent_color', $smof_data)) {
	$accent_color = $smof_data['accent_color'];
}
if (array_key_exists('background_color', $smof_data)) {
	$background_color = $smof_data['background_color'];
}
if (array_key_exists('page_heading_bg', $smof_data)) {
	$page_heading_bg = $smof_data['page_heading_bg'];
}
if (array_key_exists('page_heading_font', $smof_data)) {
	$page_heading_font = $smof_data['page_heading_font'];
}


if (array_key_exists('headings1_font_face', $smof_data)) {
	$headings1_font_face = $smof_data['headings1_font_face'];
}
if (array_key_exists('headings2_font_face', $smof_data)) {
	$headings2_font_face = $smof_data['headings2_font_face'];
}
if (array_key_exists('body_font_face', $smof_data)) {
	$body_font_face = $smof_data['body_font_face'];
}
if (array_key_exists('h_font_color', $smof_data)) {
	$h_font_color = $smof_data['h_font_color'];
}
if (array_key_exists('content_font_color', $smof_data)) {
	$content_font_color = $smof_data['content_font_color'];
}
if (array_key_exists('body_font_size', $smof_data)) {
	$body_font_size = $smof_data['body_font_size'];
}
if (array_key_exists('body_font_weight', $smof_data)) {
	$body_font_weight = $smof_data['body_font_weight'];
}
if (array_key_exists('h1_font_size', $smof_data)) {
	$h1_font_size = $smof_data['h1_font_size'];
}
if (array_key_exists('h1_font_weight', $smof_data)) {
	$h1_font_weight = $smof_data['h1_font_weight'];
}
if (array_key_exists('h2_font_size', $smof_data)) {
	$h2_font_size = $smof_data['h2_font_size'];
}
if (array_key_exists('h2_font_weight', $smof_data)) {
	$h2_font_weight = $smof_data['h2_font_weight'];
}
if (array_key_exists('h3_font_size', $smof_data)) {
	$h3_font_size = $smof_data['h3_font_size'];
}
if (array_key_exists('h3_font_weight', $smof_data)) {
	$h3_font_weight = $smof_data['h3_font_weight'];
}
if (array_key_exists('h4_font_size', $smof_data)) {
	$h4_font_size = $smof_data['h4_font_size'];
}
if (array_key_exists('h4_font_weight', $smof_data)) {
	$h4_font_weight = $smof_data['h4_font_weight'];
}
if (array_key_exists('h5_font_size', $smof_data)) {
	$h5_font_size = $smof_data['h5_font_size'];
}
if (array_key_exists('h5_font_weight', $smof_data)) {
	$h5_font_weight = $smof_data['h5_font_weight'];
}
if (array_key_exists('h6_font_size', $smof_data)) {
	$h6_font_size = $smof_data['h6_font_size'];
}
if (array_key_exists('h6_font_weight', $smof_data)) {
	$h6_font_weight = $smof_data['h6_font_weight'];
}


if (array_key_exists('footer_bg_color', $smof_data)) {
	$footer_bg_color = $smof_data['footer_bg_color'];
}
if (array_key_exists('footer_heading_size', $smof_data)) {
	$footer_heading_size = $smof_data['footer_heading_size'];
}
if (array_key_exists('footer_font_weight', $smof_data)) {
	$footer_font_weight = $smof_data['footer_font_weight'];
}
if (array_key_exists('footer_logo_bg', $smof_data)) {
	$footer_logo_bg = $smof_data['footer_logo_bg'];
}
if (array_key_exists('footer_icons_bg_color', $smof_data)) {
	$footer_icons_bg_color = $smof_data['footer_icons_bg_color'];
}
if (array_key_exists('footer_icons_color', $smof_data)) {
	$footer_icons_color = $smof_data['footer_icons_color'];
}


if (array_key_exists('menu_font_face', $smof_data)) {
	$used_fonts = array($smof_data['menu_font_face'], $smof_data['submenu_font_face'], $smof_data['body_font_face'], $smof_data['headings2_font_face'], $smof_data['headings1_font_face'] );
}


$default_font1 = 'Montserrat';
$default_font2 = 'Lato';
foreach ($used_fonts as $font) {
	if (($font !== $default_font1) AND ($font !== $default_font2)) {
		$linkfont = str_replace(' ', '+', $font) . ':100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7C';
		echo "<link href='http://fonts.googleapis.com/css?family=" . $linkfont . "&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css' />";
		}
	}

?>

<script type="text/javascript">

<?php if ($preloader) { ?>
var monte_preloader = '0';
<?php
if ($preloader == '1')  {
?>
var monte_preloader = '1';
<?php } } else { ?>
var monte_preloader = '0';
<?php } ?>	

<?php if ($preloader_inner) { ?>
var monte_preloader_inner = '1';
<?php
if ($preloader_inner == '1')  {
?>
var monte_preloader_inner = '0';
<?php } } else { ?>
var monte_preloader_inner = '1';
<?php } ?>	

<?php if ($menubg) { ?>
var menu_bg = '<?php echo esc_attr($menubg); ?>';
<?php } ?>	



<?php if ($fullscreen_slider) { ?>
	
	<?php if ($fullscreen_slider == '1') { ?>
		var fullscreen_slider = 1;
		var menu_position = 'ontop';
		
		<?php if ($menu_transparent == 1) { ?>
				var menu_transparent = 1;
			<?php } else { ?>
				var menu_transparent = 0;
			<?php } ?>
			
	<?php } else {?>
	var fullscreen_slider = 0;
		<?php if ($menu_position == 'above') {?>
			var menu_position = 'above';
			<?php if ($menu_transparent == 1) { ?>
				var menu_transparent = 1;
			<?php } else { ?>
				var menu_transparent = 0;
			<?php } ?>
		<?php } ?>
			
	<?php } ?>

<?php } else { ?>
	
var fullscreen_slider = 0;
var menu_position = 'ontop';
var menu_transparent = 1;
<?php }?>
</script>


<style type="text/css">


<?php if ($h1_font_size && $h1_font_weight) { ?>
h1, .page-heading h2
{
	font-size: <?php echo esc_attr($h1_font_size); ?>;
	font-weight: <?php echo esc_attr($h1_font_weight); ?>;
}
<?php } ?>

<?php if ($h2_font_size && $h2_font_weight) { ?>
h2
{
	font-size: <?php echo esc_attr($h2_font_size); ?>;
	font-weight: <?php echo esc_attr($h2_font_weight); ?>;
}
<?php } ?>

<?php if ($h3_font_size && $h3_font_weight) { ?>
h3
{
	font-size: <?php echo esc_attr($h3_font_size); ?>;
	font-weight: <?php echo esc_attr($h3_font_weight); ?>;
}
<?php } ?>

<?php if ($h4_font_size && $h4_font_weight) { ?>
h4
{
	font-size: <?php echo esc_attr($h4_font_size); ?>;
	font-weight: <?php echo esc_attr($h4_font_weight); ?>;
}
<?php } ?>

<?php if ($h5_font_size && $h5_font_weight) { ?>
h5
{
	font-size: <?php echo esc_attr($h5_font_size); ?>;
	font-weight: <?php echo esc_attr($h5_font_weight); ?>;
}
<?php } ?>

<?php if ($h6_font_size && $h6_font_weight) { ?>
h6
{
	font-size: <?php echo esc_attr($h6_font_size); ?>;
	font-weight: <?php echo esc_attr($h6_font_weight); ?>;
}
<?php } ?>




<?php if ($preloader_background) { ?>
#jpreOverlay, #qLoverlay {
	background: <?php echo esc_attr($preloader_background); ?>;
}
<?php } ?>

<?php if ($preloader_font) { ?>
#jpreSlide, #jprePercentage, #qLpercentage {
	color: <?php echo esc_attr($preloader_font); ?>;
}
<?php } ?>
<?php if ($preloader_logo) { ?>
#jpreBar, #qLbar {
	background: url('<?php echo esc_url($preloader_logo); ?>') 100px 100% no-repeat;
}
<?php } ?>

<?php if ($submenubg) { ?>
.dropdown-menu {
	background: <?php echo esc_attr($submenubg); ?>;
}
<?php } ?>


<?php if ($submenu_borders) { ?>
.dropdown-menu > li {
	border-bottom: 1px dotted <?php echo esc_attr($submenu_borders); ?>;
}
<?php } ?>

<?php if ($menu_font_color) { ?>
.navbar-nav a {
	color: <?php echo esc_attr($menu_font_color); ?>;
}
<?php } ?>


<?php if ($submenu_font_color) { ?>
.dropdown-menu > li > a {
	color: <?php echo esc_attr($submenu_font_color); ?>;
}
<?php } ?>
<?php if ($submenu_hover_font_color) { ?>
.dropdown-menu > li > a:hover {
	color: <?php echo esc_attr($submenu_hover_font_color); ?>;
}
<?php } ?>




<?php if ($menu_font_face) { ?>
.navbar-nav a {
	font-family: '<?php echo esc_attr($menu_font_face); ?>', sans-serif;
	font-size: <?php echo esc_attr($menu_font_size); ?>;
	font-weight: <?php echo esc_attr($menu_font_weight); ?>;
}
<?php } ?>
<?php if ($submenu_font_face) { ?>
.dropdown-menu > li > a {
	font-family: '<?php echo esc_attr($submenu_font_face); ?>', sans-serif;
	font-size: <?php echo esc_attr($submenu_font_size); ?>;
	font-weight: <?php echo esc_attr($submenu_font_weight); ?>;
}
<?php } ?>


<?php if ($accent_color) { ?>

.tp-caption.spinner1 span, .tp-caption.spinner2 span, .page-header h4 span, .page-header h5 span, .dropcap, 
.highlight, .navbar-default .navbar-toggle:hover span, .navbar-default .navbar-toggle:focus span, 
.counter_dark_accent.counter i, .counter_light_accent.counter i, .socialize-icon a:hover, .service i, 
.woocommerce .star-rating span:before, .woocommerce-page .star-rating span:before,
.post-left i, .post-info-bar i, .single-tags i, .error-page .return-home i
{
	color: <?php echo esc_attr($accent_color); ?>;
}
.colored, .dropcap-square, .dropcap-background, .monte-search, .testimonial-content:after,
.navbar-default .navbar-toggle:hover span, .navbar-default .navbar-toggle:focus span, 
.testimonial-carousel .owl-controls .owl-page.active span, .team-member-content h1:after, 
.testimonial-carousel .owl-controls.clickable .owl-page:hover span, .socialize-icon:after, 
.team-carousel .owl-controls .owl-page.active span, .socialize-icon:before, 
.team-carousel .owl-controls.clickable .owl-page:hover span, .button:before, .button:after, 
.button.white_accent:before, .button.white_accent:after, .tabs-primary li a:hover, 
.tabs-primary li.ui-tabs-active > a, .tabs-primary li.ui-tabs-active a:hover, .tabs-primary li.ui-tabs-active a:focus, 
.vc_toggle_active .vc_toggle_title, .prev-item:hover, .next-item:hover, .exit-item, .news-picture:after, 
.blog-date, .widget_search .form-group:after, .widget_search .form-group:before, 
.owl-theme .owl-controls.clickable .owl-buttons div:hover, 
.woocommerce ul.products li.product .onsale, .woocommerce-page ul.products li.product .onsale, 
.woocommerce .widget_price_filter .ui-slider .ui-slider-range, .woocommerce-page .widget_price_filter .ui-slider .ui-slider-range,
.sidebar .widget_categories li:hover:before, .tagcloud a:hover, #footer .tagcloud a:hover, 
.sidebar .widget_archive li:hover:before,
.sidebar .widget_nav_menu li:hover:before,
.sidebar .widget_pages li:hover:before,
.woocommerce.widget_product_categories li:hover:before,
.news-list-hover.nopicture
{
	background: <?php echo esc_attr($accent_color); ?>;
}
.news-list-hover.nopicture:hover{
	background: #000;
}
.sidebar .widget_categories li:hover,
.sidebar .widget_archive li:hover,
.sidebar .widget_nav_menu li:hover,
.sidebar .widget_pages li:hover,
.woocommerce.widget_product_categories li:hover{
	background-color: <?php echo esc_attr($accent_color); ?>;
	border-bottom: 1px solid <?php echo esc_attr($accent_color); ?>;

}
.accordion-primary .wpb_accordion_header 
{
	background-color: <?php echo esc_attr($accent_color); ?> !important;
}
#white-footer .footer-widget input:focus,
#footer .footer-widget input:focus, .comment-form input:focus 
{
	border-bottom: 1px solid <?php echo esc_attr($accent_color); ?> !important;
}
.footer-widget textarea:focus, .comment-form textarea:focus
{
	border: 1px solid <?php echo esc_attr($accent_color); ?> !important;
}
.form-control:focus, .screen-reader-text:focus, .wpcf7-form-control:focus,
.view-buttons:hover, .portfolio-hover a:hover 
{
	border: 2px solid <?php echo esc_attr($accent_color); ?> !important;
}
.blockquote-reverse, blockquote.pull-right
{
	border-right: 5px solid <?php echo esc_attr($accent_color); ?>;
}
blockquote
{
	border-left: 5px solid <?php echo esc_attr($accent_color); ?>;
}
.searchbox .form-group, .dropdown-menu
{
	border-bottom: 4px solid <?php echo esc_attr($accent_color); ?>;
}
.navbar-default .navbar-toggle:hover, .navbar-default .navbar-toggle:focus, 
.tagcloud a:hover, #footer .tagcloud a:hover
{
	border:1px solid <?php echo esc_attr($accent_color); ?>;
}
.button:hover
{
	border: 2px solid <?php echo esc_attr($accent_color); ?>; 
}
.active-item, .project-name:after, .navbar-nav .active-item-menu  
{
border-bottom: 2px solid <?php echo esc_attr($accent_color); ?>;
}
a.woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, .woocommerce #respond input#submit.alt:hover, 
.woocommerce #content input.button.alt:hover, .woocommerce-page a.button.alt:hover, .woocommerce-page button.button.alt:hover, .woocommerce-page input.button.alt:hover, 
.woocommerce-page #respond input#submit.alt:hover, .woocommerce-page #content input.button.alt:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, 
.woocommerce input.button:hover, .woocommerce #respond input#submit:hover, 
.woocommerce #content input.button:hover, .woocommerce-page a.button:hover, .woocommerce-page button.button:hover, .woocommerce-page input.button:hover, 
.woocommerce-page #respond input#submit:hover, .woocommerce-page #content input.button:hover, .single_add_to_cart_button:hover, 
.woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .woocommerce #respond input#submit:hover, 
.woocommerce #content input.button:hover, .woocommerce-page a.button:hover, .woocommerce-page button.button:hover, .woocommerce-page input.button:hover, 
.woocommerce-page #respond input#submit:hover, .woocommerce-page #content input.button:hover, .single_add_to_cart_button:hover
{
	border:2px solid <?php echo esc_attr($accent_color); ?>;
}

<?php } ?>
<?php if ($background_color) { ?>
body
{
	background: <?php echo esc_attr($background_color); ?>;
}
<?php } ?>

<?php if ($page_heading_bg) { ?>
.page-header
{
	background: <?php echo esc_attr($page_heading_bg); ?>;
}
<?php } ?>
<?php if ($page_heading_font) { ?>
.page-header h4, .page-header h5, .page-header a, .page-header span
{
	color: <?php echo esc_attr($page_heading_font); ?>;
}
<?php } ?>



<?php if ($body_font_face) { ?>
body, p, a 
{
	font-family: "<?php echo esc_attr($body_font_face); ?>", sans-serif;
}
<?php } ?>



<?php if ($headings1_font_face) { ?>

h1, h2, h3, .widget_shopping_cart_content a, .widget_products a, .return-home a, 
.woocommerce ul.product_list_widget li a, .woocommerce-page ul.product_list_widget li a, 
.woocommerce .woocommerce-result-count, .woocommerce-page .woocommerce-result-count, 
.inner-content .wpcf7-submit, .accordion-primary  .wpb_accordion_header a, 
.accordion-secondary .wpb_accordion_header a, .vc_toggle_title h4, .day, 
.comments-headline, .comment-reply-title, .sidebar h5, #wp-calendar caption, 
#wp-calendar thead th, .footer-widget .latest-news-list a, .footer-widget input[type=submit]
{
	font-family: "<?php echo esc_attr($headings1_font_face); ?>", sans-serif;
}

<?php } ?>

<?php if ($headings2_font_face) { ?>
h4, h5, h6, .testimonial-carousel .testimonial h6
{
	font-family: "<?php echo esc_attr($headings2_font_face); ?>", sans-serif;
}
<?php } ?>

<?php if ($h_font_color) { ?>
h1, h2, h3, h4, h5, h6,
.shipping_calculator h2 a, .woocommerce ul.products li.product .price ins, .woocommerce-page ul.products li.product .price ins,
.woocommerce ul.products li.product .price del, .woocommerce-page ul.products li.product .price del,
.woocommerce ul.products li.product .price ins, .woocommerce-page ul.products li.product .price ins, .price .amount,
.woocommerce-pagination li, .total strong, .woocommerce .woocommerce-result-count, .woocommerce-page .woocommerce-result-count,
.woocommerce-ordering select, .woocommerce .star-rating:before, .products h2, .producs h2, .price_label, .price_label .from, .price_label .to,
.dropcap-dark, .counter_dark_dark.counter i, .counter_dark_dark .counter-number h1, .counter_dark_accent .counter-number h1,
.testimonial-carousel.carousel-dark .testimonial i, .testimonial-carousel.carousel-dark .testimonial h6, .socialize-icon.socialize-dark,
.service span a, .service span a:after, .tabs-primary li > a, .tabs-secondary li > a, .accordion-primary .wpb_accordion_header, 
.accordion-primary .wpb_accordion_header a, .portfolio-buttons a, .portfolio-buttons .active a, .portfolio-buttons a:hover, 
.post-left .month, .post-left .day, .post-title-big, .post-info-bar a, .post-info-bar span, .pagination li,
.woocommerce-pagination li, .woocommerce-pagination li a, .woocommerce-pagination li span,
.pagination li a, .pagination li span, .single-tags a, .comment-name .name, .sidebar .widget_categories li a,
.sidebar .widget_archive li a,
.sidebar .widget_nav_menu li a,
.sidebar .widget_pages li a,
.woocommerce.widget_product_categories li a, .tagcloud a, #upper-footer i, #white-footer .footer-widget .latest-news-list a
{
	color: <?php echo esc_attr($h_font_color); ?>;
}
<?php } ?>

<?php if ($content_font_color) { ?>
body, p, a, 
.woocommerce .woocommerce-error, .woocommerce .woocommerce-info, .woocommerce .woocommerce-message, .woocommerce-page .woocommerce-error, 
.woocommerce-page .woocommerce-info, .woocommerce-page .woocommerce-message, .widget_shopping_cart_content a, .widget_products a, 
.woocommerce ul.product_list_widget li a, .woocommerce-page ul.product_list_widget li a, .searchbox .form-control, .comment-form input, 
.comment-form textarea, .form-control, .wpcf7-form-control, .widget_product_search #s
{
	color: <?php echo esc_attr($content_font_color); ?>;
}
<?php } ?>



<?php if ($body_font_size) { ?>
body, p, a 
{
	font-size: <?php echo esc_attr($body_font_size); ?>;
	font-weight: <?php echo esc_attr($body_font_weight); ?>;
}
<?php } ?>




<?php if ($footer_bg_color) { ?>
.footer-overlap, #footer
{
	background: <?php echo esc_attr($footer_bg_color); ?>;
}
<?php } ?>

<?php if ($footer_logo_bg) { ?>
#lower-footer
{
	background: <?php echo esc_attr($footer_logo_bg); ?>;
}
<?php } ?>
<?php if (array_key_exists('footer_icons_bg_color', $smof_data)) { ?>

#upper-footer
{
	background: <?php echo esc_attr($footer_icons_bg_color); ?>;
}
<?php } ?>

<?php if (array_key_exists('footer_icons_color', $smof_data)) { ?>
#upper-footer a
{
	color: <?php echo esc_attr($footer_icons_color); ?>;
}
<?php } ?>


<?php if ($footer_heading_size) { ?>
#footer h3
{
	font-size: <?php echo esc_attr($footer_heading_size); ?>;
}
<?php } ?>



<?php
if ($custom_css) { echo wp_filter_nohtml_kses($custom_css); }
?>
</style>