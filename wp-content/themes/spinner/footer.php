<?php global $smof_data;

if ($smof_data['footer_overlap'] == 1) { $overlap='footer-overlap'; } else { $overlap=''; }
if ($smof_data['footer_widget'] == 1) {
	if ($smof_data['widgetized_scheme'] == 'light') {
		echo '<section id="white-footer">';
	} else {
		echo '<section id="footer">';
	}
	?>
	<div class="<?php echo esc_attr($overlap);?>"></div>
		<div class="container">
			<div class="row">
				
				<?php if ($smof_data['footer_widgets'] == '1') { ?>
					
					<div class="col-md-12 padding-top-30 padding-bottom-80">
				<?php if ( is_active_sidebar( 'footer-widget1' ) ) : ?>
			
					<?php dynamic_sidebar( 'footer-widget1' ); ?>
			
				<?php endif; ?>
				</div>
				<?php } else if ($smof_data['footer_widgets'] == '2') { ?>
				<div class="col-md-6 padding-top-80 padding-bottom-80">
				<?php if ( is_active_sidebar( 'footer-widget1' ) ) : ?>
			
					<?php dynamic_sidebar( 'footer-widget1' ); ?>
			
				<?php endif; ?>
				</div>
				<div class="col-md-6 padding-top-80 padding-bottom-80">
					<?php if ( is_active_sidebar( 'footer-widget2' ) ) : ?>
			
					<?php dynamic_sidebar( 'footer-widget2' ); ?>
			
				<?php endif; ?>
				</div>
			
				<?php } else if ($smof_data['footer_widgets'] == '3') { ?>
						<div class="col-md-4 padding-top-80 padding-bottom-80">
				<?php if ( is_active_sidebar( 'footer-widget1' ) ) : ?>
			
					<?php dynamic_sidebar( 'footer-widget1' ); ?>
			
				<?php endif; ?>
				</div>
				<div class="col-md-4 padding-top-80 padding-bottom-80">
					<?php if ( is_active_sidebar( 'footer-widget2' ) ) : ?>
			
					<?php dynamic_sidebar( 'footer-widget2' ); ?>
			
				<?php endif; ?>
				</div>
				<div class="col-md-4 padding-top-80 padding-bottom-80">
				<?php if ( is_active_sidebar( 'footer-widget3' ) ) : ?>
			
					<?php dynamic_sidebar( 'footer-widget3' ); ?>
			
				<?php endif; ?>
				</div>
			
				<?php } else if ($smof_data['footer_widgets'] == '4') { ?>
						<div class="col-md-3 padding-top-80 padding-bottom-80">
				<?php if ( is_active_sidebar( 'footer-widget1' ) ) : ?>
			
					<?php dynamic_sidebar( 'footer-widget1' ); ?>
			
				<?php endif; ?>
				</div>
				<div class="col-md-3 padding-top-80 padding-bottom-80">
					<?php if ( is_active_sidebar( 'footer-widget2' ) ) : ?>
			
					<?php dynamic_sidebar( 'footer-widget2' ); ?>
			
				<?php endif; ?>
				</div>
				<div class="col-md-3 padding-top-80 padding-bottom-80">
				<?php if ( is_active_sidebar( 'footer-widget3' ) ) : ?>
			
					<?php dynamic_sidebar( 'footer-widget3' ); ?>
			
				<?php endif; ?>
				</div>
				<div class="col-md-3 padding-top-80 padding-bottom-80">
				<?php if ( is_active_sidebar( 'footer-widget4' ) ) : ?>
			
					<?php dynamic_sidebar( 'footer-widget4' ); ?>
			
				<?php endif; ?>
				</div>
					
				<?php } ?>
			</div>
		</div>
		
	</section>
<?php } 

if (array_key_exists('footer_icons', $smof_data)) {
if ($smof_data['footer_icons'] == 1) {?>
	<div id="upper-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12 align-center">
						<div class="block">
								<?php if ($smof_data['footer_facebook_url']) echo '<a href="'.esc_url($smof_data['footer_facebook_url']).'" class="social"><i class="fa fa-facebook"></i></a>'; ?>
								<?php if ($smof_data['footer_twitter_url']) echo '<a href="'.esc_url($smof_data['footer_twitter_url']).'" class="social"><i class="fa fa-twitter"></i></a>'; ?>
								<?php if ($smof_data['footer_google_url']) echo '<a href="'.esc_url($smof_data['footer_google_url']).'" class="social"><i class="fa fa-google-plus"></i></a>'; ?>
								<?php if ($smof_data['footer_dribbble_url']) echo '<a href="'.esc_url($smof_data['footer_dribbble_url']).'" class="social"><i class="fa fa-dribbble"></i></a>'; ?>
								<?php if ($smof_data['footer_youtube_url']) echo '<a href="'.esc_url($smof_data['footer_youtube_url']).'" class="social"><i class="fa fa-youtube"></i></a>'; ?>
								<?php if ($smof_data['footer_rss_url']) echo '<a href="'.esc_url($smof_data['footer_rss_url']).'" class="social"><i class="fa fa-rss"></i></a>'; ?>
								<?php if ($smof_data['footer_flickr_url']) echo '<a href="'.esc_url($smof_data['footer_flickr_url']).'" class="social"><i class="fa fa-flickr"></i></a>'; ?>
								<?php if ($smof_data['footer_linkedin_url']) echo '<a href="'.esc_url($smof_data['footer_linkedin_url']).'" class="social"><i class="fa fa-linkedin"></i></a>'; ?>
								<?php if ($smof_data['footer_vimeo_url']) echo '<a href="'.esc_url($smof_data['footer_vimeo_url']).'" class="social"><i class="fa fa-vimeo-square"></i></a>'; ?>
								<?php if ($smof_data['footer_tumblr_url']) echo '<a href="'.esc_url($smof_data['footer_tumblr_url']).'" class="social"><i class="fa fa-tumblr"></i></a>'; ?>
								<?php if ($smof_data['footer_pinterest_url']) echo '<a href="'.esc_url($smof_data['footer_pinterest_url']).'" class="social"><i class="fa fa-pinterest"></i></a>'; ?>
								<?php if ($smof_data['footer_github_url']) echo '<a href="'.esc_url($smof_data['footer_github_url']).'" class="social"><i class="fa fa-github"></i></a>'; ?>
								<?php if ($smof_data['footer_instagram_url']) echo '<a href="'.esc_url($smof_data['footer_instagram_url']).'" class="social"><i class="fa fa-instagram"></i></a>'; ?>
								<?php if ($smof_data['footer_email_url']) echo '<a href="mailto:'.esc_url($smof_data['footer_email_url']).'" class="social"><i class="fa fa-envelope-o"></i></a>'; ?>
								<?php if ($smof_data['footer_website_url']) echo '<a href="'.esc_url($smof_data['footer_website_url']).'" class="social"><i class="fa fa-align-justify"></i></a>'; ?>
						</div>
						<div class="block">
						</div>
				</div>
			</div>
		</div>
	</div>
<?php } } ?>




<?php if ($smof_data['footer_logo_section'] == 1) {?>
				<footer id="lower-footer">
						<div class="container">
							<div class="row padding-top-30 padding-bottom-30">
								<div class="col-md-12 align-center">
									<div class="block">
										<?php if ($smof_data['footer_logo']) { ?>
											<a href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url($smof_data['footer_logo']); ?>" class="padding-top-40" alt="<?php esc_attr(bloginfo('name')); ?>"></a>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</footer>

	
<?php } ?>	
		
		
<script type="text/javascript">
jQuery(document).ready(function() {
jQuery(function($) {
					jQuery('#switch').on('click', function() {
						jQuery(this).toggleClass('active');
					});
			});
	});			
				
<?php if (is_front_page()) { ?>
var monte_frontpage = 1;
<?php } else {?>	
var monte_frontpage = 0;
<?php } ?>
</script>

<?php wp_footer(); ?>
</body>
</html>