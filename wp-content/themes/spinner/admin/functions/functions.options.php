<?php

add_action('init','of_options');

if (!function_exists('of_options'))
{
	function of_options()
	{
		
		
		
		//Access the WordPress Categories via an Array
		$of_categories 		= array();  
		$of_categories_obj 	= get_categories('hide_empty=0');
		foreach ($of_categories_obj as $of_cat) {
		    $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;}
		$categories_tmp 	= array_unshift($of_categories, "Select a category:");    
	       
		//Access the WordPress Pages via an Array
		$of_pages 			= array();
		$of_pages_obj 		= get_pages('sort_column=post_parent,menu_order');    
		foreach ($of_pages_obj as $of_page) {
		    $of_pages[$of_page->ID] = $of_page->post_name; }
		$of_pages_tmp 		= array_unshift($of_pages, "Select a page:");       
	
		//Testing 
		$of_options_select 	= array("one","two","three","four","five"); 
		$of_options_radio 	= array("one" => "One","two" => "Two","three" => "Three","four" => "Four","five" => "Five");
		
		//Sample Homepage blocks for the layout manager (sorter)
		$of_options_homepage_blocks = array
		( 
			"disabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_one"		=> "Block One",
				"block_two"		=> "Block Two",
				"block_three"	=> "Block Three",
			), 
			"enabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_four"	=> "Block Four",
			),
		);


		//Stylesheets Reader
		$alt_stylesheet_path = LAYOUT_PATH;
		$alt_stylesheets = array();
		
		if ( is_dir($alt_stylesheet_path) ) 
		{
		    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) 
		    { 
		        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) 
		        {
		            if(stristr($alt_stylesheet_file, ".css" ) !== false)
					{
					$alt_stylesheets[] = $alt_stylesheet_file;
					}
					}
					}
					}

					$revolutionslider = array();
					$revolutionslider[0] = 'None';

					if(class_exists('RevSlider')){
					$slider = new RevSlider();
					$arrSliders = $slider->getArrSliders();
					foreach($arrSliders as $revSlider) {
					$revolutionslider[$revSlider->getAlias()] = $revSlider->getTitle();
					}
					}
		//Background Images Reader
		$bg_images_path = get_stylesheet_directory(). '/images/bg/'; // change this to where you store your bg images
		$bg_images_url = get_template_directory_uri().'/images/bg/'; // change this to where you store your bg images
		$bg_images = array();
		
		if ( is_dir($bg_images_path) ) {
		    if ($bg_images_dir = opendir($bg_images_path) ) { 
		        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {
		            if(stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {
		            	natsort($bg_images); //Sorts the array into a natural order
		                $bg_images[] = $bg_images_url . $bg_images_file;
		            }
		        }    
		    }
		}
		

		/*-----------------------------------------------------------------------------------*/
		/* TO DO: Add options/functions that use these */
		/*-----------------------------------------------------------------------------------*/
		
		//More Options
		$uploads_arr 		= wp_upload_dir();
		$all_uploads_path 	= $uploads_arr['path'];
		$all_uploads 		= get_option('of_uploads');
		$other_entries 		= array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
		$body_repeat 		= array("no-repeat","repeat-x","repeat-y","repeat");
		$body_pos 			= array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");
		
		// Image Alignment radio box
		$of_options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 
		
		// Image Links to Options
		$of_options_image_link_to = array("image" => "The Image","post" => "The Post"); 
		
		
		$social_icon_types = array(
			"facebook" => "Facebook",
			"twitter" => "Twitter",
			"google-plus" => "Google+",
			"dribbble " => "Dribbble",
			"youtube-play" => "YouTube",
			"rss" => "RSS",
			"flickr" => "Flickr",
			"linkedin" => "Linkedin",
			"vimeo-square" => "Vimeo",
			"tumblr" => "Tumblr",
			"pinterest" => "Pinterest",
			"github" => "Github",
			"instagram" => "Instagram",
			"behance" => "Behance",
			"envelope" => "Mail"
			); 
			
		$of_options_en_dis = array
		( 
			"disabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_one"		=> "Block One",
				"block_two"		=> "Block Two",
				"block_three"	=> "Block Three",
			), 
			"enabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_four"	=> "Block Four",
			),
		);

			$font_sizes = array();
			
			for ($i = 8; $i < 200; $i++){ 
			$size = $i.'px';
			$font_sizes[$size] = $size;
	}
		
			$footer_bg_scheme = array(
							"dark" => "Dark",
							"light" => "Light"
							);
							
			$widgets  = array(
							"1" => "1",
							"2" => "2",
							"3" => "3",
							"4" => "4"
							);
							
							
			$font_weight = array(
							"100" => "100",
							"200" => "200",
							"300" => "300",
							"400" => "400",
							"500" => "500",
							"600" => "600",
							"700" => "700",
							"800" => "800",
							"900" => "900",
							);
							
							
			$letter_spacing = array(
							"0.0pt" => "0.0pt",
							"0.1pt" => "0.1pt",
							"0.2pt" => "0.2pt",
							"0.3pt" => "0.3pt",
							"0.4pt" => "0.4pt",
							"0.5pt" => "0.5pt",
							"0.6pt" => "0.6pt",
							"0.7pt" => "0.7pt",
							"0.8pt" => "0.8pt",
							"0.9pt" => "0.9pt",
							"1.0pt" => "1.0pt",
							"1.1pt" => "1.1pt",
							"1.2pt" => "1.2pt",
							"1.3pt" => "1.3pt",
							"1.4pt" => "1.4pt",
							"1.5pt" => "1.5pt",
							"1.6pt" => "1.6pt",
							"1.7pt" => "1.7pt",
							"1.8pt" => "1.8pt",
							"1.9pt" => "1.9pt",
							"2.0pt" => "2.0pt",

							"2.1pt" => "2.1pt",
							"2.2pt" => "2.2pt",
							"2.3pt" => "2.3pt",
							"2.4pt" => "2.4pt",
							"2.5pt" => "2.5pt",
							"2.6pt" => "2.6pt",
							"2.7pt" => "2.7pt",
							"2.8pt" => "2.8pt",
							"2.9pt" => "2.9pt",
							"3.0pt" => "3.0pt",
							"3.1pt" => "3.1pt",
							"3.2pt" => "3.2pt",
							"3.3pt" => "3.3pt",
							"3.4pt" => "3.4pt",
							"3.5pt" => "3.5pt",
							"3.6pt" => "3.6pt",
							"3.7pt" => "3.7pt",
							"3.8pt" => "3.8pt",
							"3.9pt" => "3.9pt",
							"4.0pt" => "4.0pt",
						
							);				
							
			$google_fonts = array(
							"Arial" => "Arial",
							"ABeeZee" => "ABeeZee",
							"Abel" => "Abel",
							"Abril Fatface" => "Abril Fatface",
							"Aclonica" => "Aclonica",
							"Acme" => "Acme",
							"Actor" => "Actor",
							"Adamina" => "Adamina",
							"Advent Pro" => "Advent Pro",
							"Aguafina Script" => "Aguafina Script",
							"Akronim" => "Akronim",
							"Aladin" => "Aladin",
							"Aldrich" => "Aldrich",
							"Alegreya" => "Alegreya",
							"Alegreya SC" => "Alegreya SC",
							"Alex Brush" => "Alex Brush",
							"Alfa Slab One" => "Alfa Slab One",
							"Alice" => "Alice",
							"Alike" => "Alike",
							"Alike Angular" => "Alike Angular",
							"Allan" => "Allan",
							"Allerta" => "Allerta",
							"Allerta Stencil" => "Allerta Stencil",
							"Allura" => "Allura",
							"Almendra" => "Almendra",
							"Almendra Display" => "Almendra Display",
							"Almendra SC" => "Almendra SC",
							"Amarante" => "Amarante",
							"Amaranth" => "Amaranth",
							"Amatic SC" => "Amatic SC",
							"Amethysta" => "Amethysta",
							"Anaheim" => "Anaheim",
							"Andada" => "Andada",
							"Andika" => "Andika",
							"Angkor" => "Angkor",
							"Annie Use Your Telescope" => "Annie Use Your Telescope",
							"Anonymous Pro" => "Anonymous Pro",
							"Antic" => "Antic",
							"Antic Didone" => "Antic Didone",
							"Antic Slab" => "Antic Slab",
							"Anton" => "Anton",
							"Arapey" => "Arapey",
							"Arbutus" => "Arbutus",
							"Arbutus Slab" => "Arbutus Slab",
							"Architects Daughter" => "Architects Daughter",
							"Archivo Black" => "Archivo Black",
							"Archivo Narrow" => "Archivo Narrow",
							"Arimo" => "Arimo",
							"Arizonia" => "Arizonia",
							"Armata" => "Armata",
							"Artifika" => "Artifika",
							"Arvo" => "Arvo",
							"Asap" => "Asap",
							"Asset" => "Asset",
							"Astloch" => "Astloch",
							"Asul" => "Asul",
							"Atomic Age" => "Atomic Age",
							"Aubrey" => "Aubrey",
							"Audiowide" => "Audiowide",
							"Autour One" => "Autour One",
							"Average" => "Average",
							"Average Sans" => "Average Sans",
							"Averia Gruesa Libre" => "Averia Gruesa Libre",
							"Averia Libre" => "Averia Libre",
							"Averia Sans Libre" => "Averia Sans Libre",
							"Averia Serif Libre" => "Averia Serif Libre",
							"Bad Script" => "Bad Script",
							"Balthazar" => "Balthazar",
							"Bangers" => "Bangers",
							"Basic" => "Basic",
							"Battambang" => "Battambang",
							"Baumans" => "Baumans",
							"Bayon" => "Bayon",
							"Belgrano" => "Belgrano",
							"Belleza" => "Belleza",
							"BenchNine" => "BenchNine",
							"Bentham" => "Bentham",
							"Berkshire Swash" => "Berkshire Swash",
							"Bevan" => "Bevan",
							"Bigelow Rules" => "Bigelow Rules",
							"Bigshot One" => "Bigshot One",
							"Bilbo" => "Bilbo",
							"Bilbo Swash Caps" => "Bilbo Swash Caps",
							"Bitter" => "Bitter",
							"Black Ops One" => "Black Ops One",
							"Bokor" => "Bokor",
							"Bonbon" => "Bonbon",
							"Boogaloo" => "Boogaloo",
							"Bowlby One" => "Bowlby One",
							"Bowlby One SC" => "Bowlby One SC",
							"Brawler" => "Brawler",
							"Bree Serif" => "Bree Serif",
							"Bubblegum Sans" => "Bubblegum Sans",
							"Bubbler One" => "Bubbler One",
							"Buda" => "Buda",
							"Buenard" => "Buenard",
							"Butcherman" => "Butcherman",
							"Butterfly Kids" => "Butterfly Kids",
							"Cabin" => "Cabin",
							"Cabin Condensed" => "Cabin Condensed",
							"Cabin Sketch" => "Cabin Sketch",
							"Caesar Dressing" => "Caesar Dressing",
							"Cagliostro" => "Cagliostro",
							"Calligraffitti" => "Calligraffitti",
							"Cambo" => "Cambo",
							"Candal" => "Candal",
							"Cantarell" => "Cantarell",
							"Cantata One" => "Cantata One",
							"Cantora One" => "Cantora One",
							"Capriola" => "Capriola",
							"Cardo" => "Cardo",
							"Carme" => "Carme",
							"Carrois Gothic" => "Carrois Gothic",
							"Carrois Gothic SC" => "Carrois Gothic SC",
							"Carter One" => "Carter One",
							"Caudex" => "Caudex",
							"Cedarville Cursive" => "Cedarville Cursive",
							"Ceviche One" => "Ceviche One",
							"Changa One" => "Changa One",
							"Chango" => "Chango",
							"Chau Philomene One" => "Chau Philomene One",
							"Chela One" => "Chela One",
							"Chelsea Market" => "Chelsea Market",
							"Chenla" => "Chenla",
							"Cherry Cream Soda" => "Cherry Cream Soda",
							"Cherry Swash" => "Cherry Swash",
							"Chewy" => "Chewy",
							"Chicle" => "Chicle",
							"Chivo" => "Chivo",
							"Cinzel" => "Cinzel",
							"Cinzel Decorative" => "Cinzel Decorative",
							"Clicker Script" => "Clicker Script",
							"Coda" => "Coda",
							"Coda Caption" => "Coda Caption",
							"Codystar" => "Codystar",
							"Combo" => "Combo",
							"Comfortaa" => "Comfortaa",
							"Coming Soon" => "Coming Soon",
							"Concert One" => "Concert One",
							"Condiment" => "Condiment",
							"Content" => "Content",
							"Contrail One" => "Contrail One",
							"Convergence" => "Convergence",
							"Cookie" => "Cookie",
							"Copse" => "Copse",
							"Corben" => "Corben",
							"Courgette" => "Courgette",
							"Cousine" => "Cousine",
							"Coustard" => "Coustard",
							"Covered By Your Grace" => "Covered By Your Grace",
							"Crafty Girls" => "Crafty Girls",
							"Creepster" => "Creepster",
							"Crete Round" => "Crete Round",
							"Crimson Text" => "Crimson Text",
							"Croissant One" => "Croissant One",
							"Crushed" => "Crushed",
							"Cuprum" => "Cuprum",
							"Cutive" => "Cutive",
							"Cutive Mono" => "Cutive Mono",
							"Damion" => "Damion",
							"Dancing Script" => "Dancing Script",
							"Dangrek" => "Dangrek",
							"Dawning of a New Day" => "Dawning of a New Day",
							"Days One" => "Days One",
							"Delius" => "Delius",
							"Delius Swash Caps" => "Delius Swash Caps",
							"Delius Unicase" => "Delius Unicase",
							"Della Respira" => "Della Respira",
							"Denk One" => "Denk One",
							"Devonshire" => "Devonshire",
							"Didact Gothic" => "Didact Gothic",
							"Diplomata" => "Diplomata",
							"Diplomata SC" => "Diplomata SC",
							"Domine" => "Domine",
							"Donegal One" => "Donegal One",
							"Doppio One" => "Doppio One",
							"Dorsa" => "Dorsa",
							"Dosis" => "Dosis",
							"Dr Sugiyama" => "Dr Sugiyama",
							"Droid Sans" => "Droid Sans",
							"Droid Sans Mono" => "Droid Sans Mono",
							"Droid Serif" => "Droid Serif",
							"Duru Sans" => "Duru Sans",
							"Dynalight" => "Dynalight",
							"EB Garamond" => "EB Garamond",
							"Eagle Lake" => "Eagle Lake",
							"Eater" => "Eater",
							"Economica" => "Economica",
							"Electrolize" => "Electrolize",
							"Elsie" => "Elsie",
							"Elsie Swash Caps" => "Elsie Swash Caps",
							"Emblema One" => "Emblema One",
							"Emilys Candy" => "Emilys Candy",
							"Engagement" => "Engagement",
							"Englebert" => "Englebert",
							"Enriqueta" => "Enriqueta",
							"Erica One" => "Erica One",
							"Esteban" => "Esteban",
							"Euphoria Script" => "Euphoria Script",
							"Ewert" => "Ewert",
							"Exo" => "Exo",
							"Expletus Sans" => "Expletus Sans",
							"Fanwood Text" => "Fanwood Text",
							"Fascinate" => "Fascinate",
							"Fascinate Inline" => "Fascinate Inline",
							"Faster One" => "Faster One",
							"Fasthand" => "Fasthand",
							"Federant" => "Federant",
							"Federo" => "Federo",
							"Felipa" => "Felipa",
							"Fenix" => "Fenix",
							"Finger Paint" => "Finger Paint",
							"Fjalla One" => "Fjalla One",
							"Fjord One" => "Fjord One",
							"Flamenco" => "Flamenco",
							"Flavors" => "Flavors",
							"Fondamento" => "Fondamento",
							"Fontdiner Swanky" => "Fontdiner Swanky",
							"Forum" => "Forum",
							"Francois One" => "Francois One",
							"Freckle Face" => "Freckle Face",
							"Fredericka the Great" => "Fredericka the Great",
							"Fredoka One" => "Fredoka One",
							"Freehand" => "Freehand",
							"Fresca" => "Fresca",
							"Frijole" => "Frijole",
							"Fruktur" => "Fruktur",
							"Fugaz One" => "Fugaz One",
							"GFS Didot" => "GFS Didot",
							"GFS Neohellenic" => "GFS Neohellenic",
							"Gabriela" => "Gabriela",
							"Gafata" => "Gafata",
							"Galdeano" => "Galdeano",
							"Galindo" => "Galindo",
							"Gentium Basic" => "Gentium Basic",
							"Gentium Book Basic" => "Gentium Book Basic",
							"Geo" => "Geo",
							"Geostar" => "Geostar",
							"Geostar Fill" => "Geostar Fill",
							"Germania One" => "Germania One",
							"Gilda Display" => "Gilda Display",
							"Give You Glory" => "Give You Glory",
							"Glass Antiqua" => "Glass Antiqua",
							"Glegoo" => "Glegoo",
							"Gloria Hallelujah" => "Gloria Hallelujah",
							"Goblin One" => "Goblin One",
							"Gochi Hand" => "Gochi Hand",
							"Gorditas" => "Gorditas",
							"Goudy Bookletter 1911" => "Goudy Bookletter 1911",
							"Graduate" => "Graduate",
							"Grand Hotel" => "Grand Hotel",
							"Gravitas One" => "Gravitas One",
							"Great Vibes" => "Great Vibes",
							"Griffy" => "Griffy",
							"Gruppo" => "Gruppo",
							"Gudea" => "Gudea",
							"Habibi" => "Habibi",
							"Hammersmith One" => "Hammersmith One",
							"Hanalei" => "Hanalei",
							"Hanalei Fill" => "Hanalei Fill",
							"Handlee" => "Handlee",
							"Hanuman" => "Hanuman",
							"Happy Monkey" => "Happy Monkey",
							"Headland One" => "Headland One",
							"Henny Penny" => "Henny Penny",
							"Herr Von Muellerhoff" => "Herr Von Muellerhoff",
							"Holtwood One SC" => "Holtwood One SC",
							"Homemade Apple" => "Homemade Apple",
							"Homenaje" => "Homenaje",
							"IM Fell DW Pica" => "IM Fell DW Pica",
							"IM Fell DW Pica SC" => "IM Fell DW Pica SC",
							"IM Fell Double Pica" => "IM Fell Double Pica",
							"IM Fell Double Pica SC" => "IM Fell Double Pica SC",
							"IM Fell English" => "IM Fell English",
							"IM Fell English SC" => "IM Fell English SC",
							"IM Fell French Canon" => "IM Fell French Canon",
							"IM Fell French Canon SC" => "IM Fell French Canon SC",
							"IM Fell Great Primer" => "IM Fell Great Primer",
							"IM Fell Great Primer SC" => "IM Fell Great Primer SC",
							"Iceberg" => "Iceberg",
							"Iceland" => "Iceland",
							"Imprima" => "Imprima",
							"Inconsolata" => "Inconsolata",
							"Inder" => "Inder",
							"Indie Flower" => "Indie Flower",
							"Inika" => "Inika",
							"Irish Grover" => "Irish Grover",
							"Istok Web" => "Istok Web",
							"Italiana" => "Italiana",
							"Italianno" => "Italianno",
							"Jacques Francois" => "Jacques Francois",
							"Jacques Francois Shadow" => "Jacques Francois Shadow",
							"Jim Nightshade" => "Jim Nightshade",
							"Jockey One" => "Jockey One",
							"Jolly Lodger" => "Jolly Lodger",
							"Josefin Sans" => "Josefin Sans",
							"Josefin Slab" => "Josefin Slab",
							"Joti One" => "Joti One",
							"Judson" => "Judson",
							"Julee" => "Julee",
							"Julius Sans One" => "Julius Sans One",
							"Junge" => "Junge",
							"Jura" => "Jura",
							"Just Another Hand" => "Just Another Hand",
							"Just Me Again Down Here" => "Just Me Again Down Here",
							"Kameron" => "Kameron",
							"Karla" => "Karla",
							"Kaushan Script" => "Kaushan Script",
							"Kavoon" => "Kavoon",
							"Keania One" => "Keania One",
							"Kelly Slab" => "Kelly Slab",
							"Kenia" => "Kenia",
							"Khmer" => "Khmer",
							"Kite One" => "Kite One",
							"Knewave" => "Knewave",
							"Kotta One" => "Kotta One",
							"Koulen" => "Koulen",
							"Kranky" => "Kranky",
							"Kreon" => "Kreon",
							"Kristi" => "Kristi",
							"Krona One" => "Krona One",
							"La Belle Aurore" => "La Belle Aurore",
							"Lancelot" => "Lancelot",
							"Lato" => "Lato",
							"League Script" => "League Script",
							"Leckerli One" => "Leckerli One",
							"Ledger" => "Ledger",
							"Lekton" => "Lekton",
							"Lemon" => "Lemon",
							"Libre Baskerville" => "Libre Baskerville",
							"Life Savers" => "Life Savers",
							"Lilita One" => "Lilita One",
							"Limelight" => "Limelight",
							"Linden Hill" => "Linden Hill",
							"Lobster" => "Lobster",
							"Lobster Two" => "Lobster Two",
							"Londrina Outline" => "Londrina Outline",
							"Londrina Shadow" => "Londrina Shadow",
							"Londrina Sketch" => "Londrina Sketch",
							"Londrina Solid" => "Londrina Solid",
							"Lora" => "Lora",
							"Love Ya Like A Sister" => "Love Ya Like A Sister",
							"Loved by the King" => "Loved by the King",
							"Lovers Quarrel" => "Lovers Quarrel",
							"Luckiest Guy" => "Luckiest Guy",
							"Lusitana" => "Lusitana",
							"Lustria" => "Lustria",
							"Macondo" => "Macondo",
							"Macondo Swash Caps" => "Macondo Swash Caps",
							"Magra" => "Magra",
							"Maiden Orange" => "Maiden Orange",
							"Mako" => "Mako",
							"Marcellus" => "Marcellus",
							"Marcellus SC" => "Marcellus SC",
							"Marck Script" => "Marck Script",
							"Margarine" => "Margarine",
							"Marko One" => "Marko One",
							"Marmelad" => "Marmelad",
							"Marvel" => "Marvel",
							"Mate" => "Mate",
							"Mate SC" => "Mate SC",
							"Maven Pro" => "Maven Pro",
							"McLaren" => "McLaren",
							"Meddon" => "Meddon",
							"MedievalSharp" => "MedievalSharp",
							"Medula One" => "Medula One",
							"Megrim" => "Megrim",
							"Meie Script" => "Meie Script",
							"Merienda" => "Merienda",
							"Merienda One" => "Merienda One",
							"Merriweather" => "Merriweather",
							"Merriweather Sans" => "Merriweather Sans",
							"Metal" => "Metal",
							"Metal Mania" => "Metal Mania",
							"Metamorphous" => "Metamorphous",
							"Metrophobic" => "Metrophobic",
							"Michroma" => "Michroma",
							"Milonga" => "Milonga",
							"Miltonian" => "Miltonian",
							"Miltonian Tattoo" => "Miltonian Tattoo",
							"Miniver" => "Miniver",
							"Miss Fajardose" => "Miss Fajardose",
							"Modern Antiqua" => "Modern Antiqua",
							"Molengo" => "Molengo",
							"Molle" => "Molle",
							"Monda" => "Monda",
							"Monofett" => "Monofett",
							"Monoton" => "Monoton",
							"Monsieur La Doulaise" => "Monsieur La Doulaise",
							"Montaga" => "Montaga",
							"Montez" => "Montez",
							"Montserrat" => "Montserrat",
							"Montserrat Alternates" => "Montserrat Alternates",
							"Montserrat Subrayada" => "Montserrat Subrayada",
							"Moul" => "Moul",
							"Moulpali" => "Moulpali",
							"Mountains of Christmas" => "Mountains of Christmas",
							"Mouse Memoirs" => "Mouse Memoirs",
							"Mr Bedfort" => "Mr Bedfort",
							"Mr Dafoe" => "Mr Dafoe",
							"Mr De Haviland" => "Mr De Haviland",
							"Mrs Saint Delafield" => "Mrs Saint Delafield",
							"Mrs Sheppards" => "Mrs Sheppards",
							"Muli" => "Muli",
							"Mystery Quest" => "Mystery Quest",
							"Neucha" => "Neucha",
							"Neuton" => "Neuton",
							"New Rocker" => "New Rocker",
							"News Cycle" => "News Cycle",
							"Niconne" => "Niconne",
							"Nixie One" => "Nixie One",
							"Nobile" => "Nobile",
							"Nokora" => "Nokora",
							"Norican" => "Norican",
							"Nosifer" => "Nosifer",
							"Nothing You Could Do" => "Nothing You Could Do",
							"Noticia Text" => "Noticia Text",
							"Noto Sans" => "Noto Sans",
							"Noto Serif" => "Noto Serif",
							"Nova Cut" => "Nova Cut",
							"Nova Flat" => "Nova Flat",
							"Nova Mono" => "Nova Mono",
							"Nova Oval" => "Nova Oval",
							"Nova Round" => "Nova Round",
							"Nova Script" => "Nova Script",
							"Nova Slim" => "Nova Slim",
							"Nova Square" => "Nova Square",
							"Numans" => "Numans",
							"Nunito" => "Nunito",
							"Odor Mean Chey" => "Odor Mean Chey",
							"Offside" => "Offside",
							"Old Standard TT" => "Old Standard TT",
							"Oldenburg" => "Oldenburg",
							"Oleo Script" => "Oleo Script",
							"Oleo Script Swash Caps" => "Oleo Script Swash Caps",
							"Open Sans" => "Open Sans",
							"Open Sans Condensed" => "Open Sans Condensed",
							"Oranienbaum" => "Oranienbaum",
							"Orbitron" => "Orbitron",
							"Oregano" => "Oregano",
							"Orienta" => "Orienta",
							"Original Surfer" => "Original Surfer",
							"Oswald" => "Oswald",
							"Over the Rainbow" => "Over the Rainbow",
							"Overlock" => "Overlock",
							"Overlock SC" => "Overlock SC",
							"Ovo" => "Ovo",
							"Oxygen" => "Oxygen",
							"Oxygen Mono" => "Oxygen Mono",
							"PT Mono" => "PT Mono",
							"PT Sans" => "PT Sans",
							"PT Sans Caption" => "PT Sans Caption",
							"PT Sans Narrow" => "PT Sans Narrow",
							"PT Serif" => "PT Serif",
							"PT Serif Caption" => "PT Serif Caption",
							"Pacifico" => "Pacifico",
							"Paprika" => "Paprika",
							"Parisienne" => "Parisienne",
							"Passero One" => "Passero One",
							"Passion One" => "Passion One",
							"Patrick Hand" => "Patrick Hand",
							"Patrick Hand SC" => "Patrick Hand SC",
							"Patua One" => "Patua One",
							"Paytone One" => "Paytone One",
							"Peralta" => "Peralta",
							"Permanent Marker" => "Permanent Marker",
							"Petit Formal Script" => "Petit Formal Script",
							"Petrona" => "Petrona",
							"Philosopher" => "Philosopher",
							"Piedra" => "Piedra",
							"Pinyon Script" => "Pinyon Script",
							"Pirata One" => "Pirata One",
							"Plaster" => "Plaster",
							"Play" => "Play",
							"Playball" => "Playball",
							"Playfair Display" => "Playfair Display",
							"Playfair Display SC" => "Playfair Display SC",
							"Podkova" => "Podkova",
							"Poiret One" => "Poiret One",
							"Poller One" => "Poller One",
							"Poly" => "Poly",
							"Pompiere" => "Pompiere",
							"Pontano Sans" => "Pontano Sans",
							"Port Lligat Sans" => "Port Lligat Sans",
							"Port Lligat Slab" => "Port Lligat Slab",
							"Prata" => "Prata",
							"Preahvihear" => "Preahvihear",
							"Press Start 2P" => "Press Start 2P",
							"Princess Sofia" => "Princess Sofia",
							"Prociono" => "Prociono",
							"Prosto One" => "Prosto One",
							"Puritan" => "Puritan",
							"Purple Purse" => "Purple Purse",
							"Quando" => "Quando",
							"Quantico" => "Quantico",
							"Quattrocento" => "Quattrocento",
							"Quattrocento Sans" => "Quattrocento Sans",
							"Questrial" => "Questrial",
							"Quicksand" => "Quicksand",
							"Quintessential" => "Quintessential",
							"Qwigley" => "Qwigley",
							"Racing Sans One" => "Racing Sans One",
							"Radley" => "Radley",
							"Raleway" => "Raleway",
							"Raleway Dots" => "Raleway Dots",
							"Rambla" => "Rambla",
							"Rammetto One" => "Rammetto One",
							"Ranchers" => "Ranchers",
							"Rancho" => "Rancho",
							"Rationale" => "Rationale",
							"Redressed" => "Redressed",
							"Reenie Beanie" => "Reenie Beanie",
							"Revalia" => "Revalia",
							"Ribeye" => "Ribeye",
							"Ribeye Marrow" => "Ribeye Marrow",
							"Righteous" => "Righteous",
							"Risque" => "Risque",
							"Roboto" => "Roboto",
							"Roboto Condensed" => "Roboto Condensed",
							"Roboto Slab" => "Roboto Slab",
							"Rochester" => "Rochester",
							"Rock Salt" => "Rock Salt",
							"Rokkitt" => "Rokkitt",
							"Romanesco" => "Romanesco",
							"Ropa Sans" => "Ropa Sans",
							"Rosario" => "Rosario",
							"Rosarivo" => "Rosarivo",
							"Rouge Script" => "Rouge Script",
							"Ruda" => "Ruda",
							"Rufina" => "Rufina",
							"Ruge Boogie" => "Ruge Boogie",
							"Ruluko" => "Ruluko",
							"Rum Raisin" => "Rum Raisin",
							"Ruslan Display" => "Ruslan Display",
							"Russo One" => "Russo One",
							"Ruthie" => "Ruthie",
							"Rye" => "Rye",
							"Sacramento" => "Sacramento",
							"Sail" => "Sail",
							"Salsa" => "Salsa",
							"Sanchez" => "Sanchez",
							"Sancreek" => "Sancreek",
							"Sansita One" => "Sansita One",
							"Sarina" => "Sarina",
							"Satisfy" => "Satisfy",
							"Scada" => "Scada",
							"Schoolbell" => "Schoolbell",
							"Seaweed Script" => "Seaweed Script",
							"Sevillana" => "Sevillana",
							"Seymour One" => "Seymour One",
							"Shadows Into Light" => "Shadows Into Light",
							"Shadows Into Light Two" => "Shadows Into Light Two",
							"Shanti" => "Shanti",
							"Share" => "Share",
							"Share Tech" => "Share Tech",
							"Share Tech Mono" => "Share Tech Mono",
							"Shojumaru" => "Shojumaru",
							"Short Stack" => "Short Stack",
							"Siemreap" => "Siemreap",
							"Sigmar One" => "Sigmar One",
							"Signika" => "Signika",
							"Signika Negative" => "Signika Negative",
							"Simonetta" => "Simonetta",
							"Sintony" => "Sintony",
							"Sirin Stencil" => "Sirin Stencil",
							"Six Caps" => "Six Caps",
							"Skranji" => "Skranji",
							"Slackey" => "Slackey",
							"Smokum" => "Smokum",
							"Smythe" => "Smythe",
							"Sniglet" => "Sniglet",
							"Snippet" => "Snippet",
							"Snowburst One" => "Snowburst One",
							"Sofadi One" => "Sofadi One",
							"Sofia" => "Sofia",
							"Sonsie One" => "Sonsie One",
							"Sorts Mill Goudy" => "Sorts Mill Goudy",
							"Source Code Pro" => "Source Code Pro",
							"Source Sans Pro" => "Source Sans Pro",
							"Special Elite" => "Special Elite",
							"Spicy Rice" => "Spicy Rice",
							"Spinnaker" => "Spinnaker",
							"Spirax" => "Spirax",
							"Squada One" => "Squada One",
							"Stalemate" => "Stalemate",
							"Stalinist One" => "Stalinist One",
							"Stardos Stencil" => "Stardos Stencil",
							"Stint Ultra Condensed" => "Stint Ultra Condensed",
							"Stint Ultra Expanded" => "Stint Ultra Expanded",
							"Stoke" => "Stoke",
							"Strait" => "Strait",
							"Sue Ellen Francisco" => "Sue Ellen Francisco",
							"Sunshiney" => "Sunshiney",
							"Supermercado One" => "Supermercado One",
							"Suwannaphum" => "Suwannaphum",
							"Swanky and Moo Moo" => "Swanky and Moo Moo",
							"Syncopate" => "Syncopate",
							"Tangerine" => "Tangerine",
							"Taprom" => "Taprom",
							"Tauri" => "Tauri",
							"Telex" => "Telex",
							"Tenor Sans" => "Tenor Sans",
							"Text Me One" => "Text Me One",
							"The Girl Next Door" => "The Girl Next Door",
							"Tienne" => "Tienne",
							"Tinos" => "Tinos",
							"Titan One" => "Titan One",
							"Titillium Web" => "Titillium Web",
							"Trade Winds" => "Trade Winds",
							"Trocchi" => "Trocchi",
							"Trochut" => "Trochut",
							"Trykker" => "Trykker",
							"Tulpen One" => "Tulpen One",
							"Ubuntu" => "Ubuntu",
							"Ubuntu Condensed" => "Ubuntu Condensed",
							"Ubuntu Mono" => "Ubuntu Mono",
							"Ultra" => "Ultra",
							"Uncial Antiqua" => "Uncial Antiqua",
							"Underdog" => "Underdog",
							"Unica One" => "Unica One",
							"UnifrakturCook" => "UnifrakturCook",
							"UnifrakturMaguntia" => "UnifrakturMaguntia",
							"Unkempt" => "Unkempt",
							"Unlock" => "Unlock",
							"Unna" => "Unna",
							"VT323" => "VT323",
							"Vampiro One" => "Vampiro One",
							"Varela" => "Varela",
							"Varela Round" => "Varela Round",
							"Vast Shadow" => "Vast Shadow",
							"Vibur" => "Vibur",
							"Vidaloka" => "Vidaloka",
							"Viga" => "Viga",
							"Voces" => "Voces",
							"Volkhov" => "Volkhov",
							"Vollkorn" => "Vollkorn",
							"Voltaire" => "Voltaire",
							"Waiting for the Sunrise" => "Waiting for the Sunrise",
							"Wallpoet" => "Wallpoet",
							"Walter Turncoat" => "Walter Turncoat",
							"Warnes" => "Warnes",
							"Wellfleet" => "Wellfleet",
							"Wendy One" => "Wendy One",
							"Wire One" => "Wire One",
							"Yanone Kaffeesatz" => "Yanone Kaffeesatz",
							"Yellowtail" => "Yellowtail",
							"Yeseva One" => "Yeseva One",
							"Yesteryear" => "Yesteryear",
							"Zeyada" => "Zeyada"
						);		

/*-----------------------------------------------------------------------------------*/
/* The Options Array */
/*-----------------------------------------------------------------------------------*/

// Set the Options Array
global $of_options;
$of_options = array();



			
			
			


				


// BASIC SETTINGS

$of_options[] = array( 	"name" 		=> "Basic Settings",
						"type" 		=> "heading"
				);
				
			
				
		$of_options[] = array( 	
						"name" 		=> "Enable Page Preloader",
						"id" 		=> "preloader", 
						"std" 		=> 1,
						"folds"		=> 0,
						"type" 		=> "switch"
				);
				
				
				$of_options[] = array( 	
						"name" 		=> "Disable Preloader outside Homepage",
						"id" 		=> "preloader_inner", 
						"fold" 		=> "preloader",
						"std" 		=> 0,
						"type" 		=> "switch"
				);
				
			$of_options[] = array( 	
				"name" 		=> "Preloader Background Color",
				"id" 		=> "preloader_background",
				"std" 		=> "#000000",
				"fold" 		=> "preloader",
				"type" 		=> "color"
				);
				
			$of_options[] = array( 	
				"name" 		=> "Preloader Font Color",
				"id" 		=> "preloader_font",
				"std" 		=> "#ffffff",
				"fold" 		=> "preloader",
				"type" 		=> "color"
				);
				
				$of_options[] = array( 	
				"name" 		=> "Preloader Logo",
				"desc" 		=> "Upload preloader logo image",
				"id" 		=> "preloader_logo",
				"fold" 		=> "preloader",
				"std" 		=> "",
				"type" 		=> "upload"
				);
	
				
		$of_options[] = array( 	
				"name" 		=> "Logo",
				"desc" 		=> "Upload logo image",
				"id" 		=> "logo",
				"std" 		=> "",
				"type" 		=> "upload"
				);
				
				
				$of_options[] = array( 	
						"name" 		=> "Enable Search Icon",
						"id" 		=> "enable_search", 
						"std" 		=> 1,
						"type" 		=> "switch"
				);
				
				
				
				
			$of_options[] = array( 	
				"name" 		=> "WooCommerce Page Title",
				"id" 		=> "woo_title",
				"std" 		=> "sp<span>i</span>nner store",
				"type" 		=> "text",
				);
				
						$of_options[] = array( 	
				"name" 		=> "WooCommerce Page Subtitle",
				"id" 		=> "woo_subtitle",
				"std" 		=> "welcome! we are a creative digital agency<BR>focused on end <span>user</span>",
				"type" 		=> "textarea",
				);
				
					
		



				
						
		$of_options[] = array( 	
				"name" 		=> "Favicon",
				"desc" 		=> "Upload favicon",
				"id" 		=> "favicon",
				"std" 		=> "",
				"type" 		=> "upload"
				);
				
				$of_options[] = array( 	"name" 		=> "Custom CSS",
						"desc" 		=> "Paste your custom CSS here.",
						"id" 		=> "custom_css_code",
						"std" 		=> "",
						"type" 		=> "textarea"
				);
									
				
				
				
				
	$of_options[] = array( 	"name" 		=> "Color Settings",
						"type" 		=> "heading"
				);
				
				
									
				
				$of_options[] = array( 	
				"name" 		=> "Primary Accent color",
				"id" 		=> "accent_color",
				"std" 		=> "#dc143c",
				"type" 		=> "color"
				);
			
			
			
			$of_options[] = array( 	
			"name" 		=> "Content Background Color",
			"id" 		=> "background_color",
			"std" 		=> "#ffffff",
			"type" 		=> "color"
			);
			
					
			$of_options[] = array( 	
			"name" 		=> "Default Page Heading Bg Color",
			"id" 		=> "page_heading_bg",
			"std" 		=> "#000000",
			"type" 		=> "color"
			);
			
		$of_options[] = array( 	
			"name" 		=> "Default Page Heading Font Color",
			"id" 		=> "page_heading_font",
			"std" 		=> "#ffffff",
			"type" 		=> "color"
			);
			

				
				
// MENU SETTINGS

$of_options[] = array( 	"name" 		=> "Menu Settings",
						"type" 		=> "heading"
				);
			
			
			$of_options[] = array( 	
			"name" 		=> "Menu position",
			"desc" 		=> "Above slider position can only be used for non-fullscreen slider size",
			"id" 		=> "menu_position",
			"std" 		=> "ontop",
			"type" 		=> "select",
			"options" 	=> array("above" => "Above Slider (relative)","ontop" => "On top of the slider (absolute)"),
			);		
			
			
				$of_options[] = array( 	
						"name" 		=> "Make transparent on starter position - over slider",
						"id" 		=> "menu_transparent", 
						"desc" 		=> "This option can only be used if 'On top of the slider' position is used for menu / topbar positioning",
						"std" 		=> 1,
						"folds"		=> 0,
						"type" 		=> "switch"
				);	
				
				
			$of_options[] = array( 	
						"name" 		=> "Make fullwidth",
						"id" 		=> "menu_fullwidth", 
						"std" 		=> 1,
						"folds"		=> 0,
						"type" 		=> "switch"
				);	
						
		$of_options[] = array( 	
			"name" 		=> "Menu background color",
			"desc" 		=> "Choose background color for navigation bar",
			"id" 		=> "menu_bg_color",
			"std" 		=> "#000000",
			"type" 		=> "color"
			);	
			
			
		$of_options[] = array( 	
			"name" 		=> "Submenu background color",
			"desc" 		=> "Choose font color for submenu background",
			"id" 		=> "submenu_bg_color",
			"std" 		=> "#000000",
			"type" 		=> "color"
			);

			
		$of_options[] = array( 	
			"name" 		=> "Submenu borders color",
			"id" 		=> "submenu_borders_color",
			"std" 		=> "#ffffff",
			"type" 		=> "color"
			);
					
			
			
		$of_options[] = array( 	
			"name" 		=> "Menu font color",
			"desc" 		=> "Choose font color for main navigation",
			"id" 		=> "menu_font_color",
			"std" 		=> "#ffffff",
			"type" 		=> "color"
			);	
			
			$of_options[] = array( 	
			"name" 		=> "Submenu font color",
			"desc" 		=> "Choose font color for submenu",
			"id" 		=> "submenu_font_color",
			"std" 		=> "#757575",
			"type" 		=> "color"
			);	
			
			$of_options[] = array( 	
			"name" 		=> "Submenu hover font color",
			"desc" 		=> "Choose font color for submenu",
			"id" 		=> "submenu_hover_font_color",
			"std" 		=> "#ffffff",
			"type" 		=> "color"
			);	
			
			
				
			$of_options[] = array( 	
			"name" 		=> "Menu font face",
			"desc" 		=> "Choose a font for main navigation",
			"id" 		=> "menu_font_face",
			"std" 		=> "Montserrat",
			"type" 		=> "select",
			"options" 	=> $google_fonts,
			);		
			
			$of_options[] = array( 	
			"name" 		=> "Submenu font face",
			"desc" 		=> "Choose a font for submenu links",
			"id" 		=> "submenu_font_face",
			"std" 		=> "Lato",
			"type" 		=> "select",
			"options" 	=> $google_fonts,
			);	
			
			
			
			$of_options[] = array( 	
			"name" 		=> "Menu font size",
			"desc" 		=> "Choose font size for main navigation",
			"id" 		=> "menu_font_size",
			"std" 		=> "14px",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_sizes,
			);		
			
				

			$of_options[] = array( 	
			"name" 		=> "Menu font weight",
			"id" 		=> "menu_font_weight",
			"std" 		=> "500",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_weight,
			);
					
			
			$of_options[] = array( 	
			"name" 		=> "Submenu font size",
			"desc" 		=> "Choose font size for submenu links",
			"id" 		=> "submenu_font_size",
			"std" 		=> "14px",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_sizes,
			);		
			

			$of_options[] = array( 	
			"name" 		=> "Submenu font weight",
			"id" 		=> "submenu_font_weight",
			"std" 		=> "400",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_weight,
			);


				

$of_options[] = array( 	"name" 		=> "Typography",
						"type" 		=> "heading"
				);
				
				
	$of_options[] = array( 	
			"name" 		=> "H1, H2, H3 font face",
			"id" 		=> "headings1_font_face",
			"std" 		=> "Montserrat",
			"type" 		=> "select",
			"options" 	=> $google_fonts,
			);	
			
		$of_options[] = array( 	
			"name" 		=> "H3, H4, H5 font face",
			"id" 		=> "headings2_font_face",
			"std" 		=> "Lato",
			"type" 		=> "select",
			"options" 	=> $google_fonts,
			);	
			
			
			
		$of_options[] = array( 	
			"name" 		=> "Headings font color",
			"id" 		=> "h_font_color",
			"std" 		=> "#000000",
			"type" 		=> "color",
			);
			
			
	$of_options[] = array( 	
			"name" 		=> "Content font face",
			"id" 		=> "body_font_face",
			"std" 		=> "Lato",
			"type" 		=> "select",
			"options" 	=> $google_fonts,
			);		
			
			
		$of_options[] = array( 	
			"name" 		=> "Content font color",
			"id" 		=> "content_font_color",
			"std" 		=> "#000000",
			"type" 		=> "color"
			);	
			
			
		$of_options[] = array( 	
			"name" 		=> "Body font size",
			"id" 		=> "body_font_size",
			"std" 		=> "15px",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_sizes,
			);		
					
		
		$of_options[] = array( 	
			"name" 		=> "Body font weight",
			"id" 		=> "body_font_weight",
			"std" 		=> "400",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_weight,
			);
			

		$of_options[] = array( 	
			"name" 		=> "Heading 1 font size (H1)",
			"id" 		=> "h1_font_size",
			"std" 		=> "48px",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_sizes,
			);	
			
				$of_options[] = array( 	
			"name" 		=> "H1 font weight",
			"id" 		=> "h1_font_weight",
			"std" 		=> "600",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_weight,
			);	
					
				
			$of_options[] = array( 	
			"name" 		=> "Heading 2 font size (H2)",
			"id" 		=> "h2_font_size",
			"std" 		=> "32px",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_sizes,
			);			
				
			$of_options[] = array( 	
			"name" 		=> "H2 font weight",
			"id" 		=> "h2_font_weight",
			"std" 		=> "600",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_weight,
			);	
			
				
				$of_options[] = array( 	
			"name" 		=> "Heading 3 font size (H3)",
			"id" 		=> "h3_font_size",
			"std" 		=> "24px",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_sizes,
			);	
				
				
			
			$of_options[] = array( 	
			"name" 		=> "H3 font weight",
			"id" 		=> "h3_font_weight",
			"std" 		=> "400",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_weight,
			);	
			
			
				$of_options[] = array( 	
			"name" 		=> "Heading 4 font size (H4)",
			"id" 		=> "h4_font_size",
			"std" 		=> "22px",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_sizes,
			);	
			
				
			$of_options[] = array( 	
			"name" 		=> "H4 font weight",
			"id" 		=> "h4_font_weight",
			"std" 		=> "300",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_weight,
			);	
			
				$of_options[] = array( 	
			"name" 		=> "Heading 5 font size (H5)",
			"id" 		=> "h5_font_size",
			"std" 		=> "20px",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_sizes,
			);	
			
			
			$of_options[] = array( 	
			"name" 		=> "H5 font weight",
			"id" 		=> "h5_font_weight",
			"std" 		=> "300",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_weight,
			);	
			
				$of_options[] = array( 	
			"name" 		=> "Heading 6 font size (H6)",
			"id" 		=> "h6_font_size",
			"std" 		=> "18px",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_sizes,
			);		
				
			
			
			$of_options[] = array( 	
			"name" 		=> "H6 font weight",
			"id" 		=> "h6_font_weight",
			"std" 		=> "300",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_weight,
			);	
			
			
// SLIDER SETTINGS

$of_options[] = array( 	"name" 		=> "Slider Settings",
						"type" 		=> "heading"
				);
				

	$of_options[] = array( 	
						"name" 		=> "Revolution Slider",
						"desc" 		=> "The script is included in your theme package.",
						"id" 		=> "revolution_switch", 
						"std" 		=> 0,
						"folds"		=> 1,
						"type" 		=> "switch"
				);
				
				
	$of_options[] = array( 	
						"name" 		=> "Slider select",
						"desc" 		=> "Select a slider you'd like to use in your header",
						"id" 		=> "revolution_slider_id", 
						"fold" 	=> "revolution_switch",
		         		"type" 		=> "select",
						"options" 	=> $revolutionslider
			
				);
				
				
			$of_options[] = array( 	
						"name" 		=> "Slider Overlap",
						"desc" 		=> "Select this option if you wish to use bottom slant overlap",
						"id" 		=> "slider_overlay", 
						"fold" 	=> "revolution_switch",
		         		"type" 		=> "switch",
		         		"std" 		=> 1,
				);
				
				$of_options[] = array( 	
						"name" 		=> "Slider Overlap Background",
						"id" 		=> "slider_overlay_bg", 
						"fold" 	=> "revolution_switch",
		         		"type" 		=> "color",
		         		"std" 		=> "#ffffff",
				);
				
			$of_options[] = array( 	
						"name" 		=> "Fullscreen slider",
						"desc" 		=> "Check this option if you wish the slider height to be equal to window height",
						"id" 		=> "fullscreen_slider", 
						"std" 		=> 0,
						"folds"		=> 0,
						"type" 		=> "switch"
				);		


// FOOTER SETTINGS

				
$of_options[] = array( 	"name" 		=> "Footer Settings",
						"type" 		=> "heading"
				);
				
				
				
				$of_options[] = array( 	
						"name" 		=> "Footer Widgetized Section",
						"desc" 		=> "Footer Widgetized Section",
						"id" 		=> "footer_widget", 
						"std" 		=> 1,
						"folds"		=> 0,
						"type" 		=> "switch"
				);
				
			
			$of_options[] = array( 	
						"name" 		=> "Footer Widgetized Section Overlap",
						"id" 		=> "footer_overlap", 
						"fold" 		=> "footer_widget",
						"std" 		=> 1,
						"folds"		=> 0,
						"type" 		=> "switch"
				);
				
				
					$of_options[] = array( 	
			"name" 		=> "Footer Widgets",
			"id" 		=> "footer_widgets",
			"std" 		=> "1",
			"type" 		=> "select",
			"fold" 		=> "footer_widget",
			"mod" 		=> "mini",
			"options" 	=> $widgets,
			);	
			
				
			$of_options[] = array( 	
			"name" 		=> "Widgetized Section Color Scheme",
			"id" 		=> "widgetized_scheme",
			"std" 		=> "dark",
			"fold" 		=> "footer_widget",
			"type" 		=> "select",
			"options" 	=> $footer_bg_scheme,
			);
				
			$of_options[] = array( 	
			"name" 		=> "Widgetized background color",
			"desc" 		=> "Choose widgetized footer section background color",
			"id" 		=> "footer_bg_color",
			"fold" 		=> "footer_widget",
			"std" 		=> "#000000",
			"type" 		=> "color"
			);	
				
				
						
			$of_options[] = array( 	
			"name" 		=> "Footer Heading Size",
			"id" 		=> "footer_heading_size",
			"std" 		=> "14px",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_sizes,
			);	
				
			
					$of_options[] = array( 	
			"name" 		=> "Footer Heading font weight",
			"id" 		=> "footer_font_weight",
			"std" 		=> "500",
			"type" 		=> "select",
			"mod" 		=> "mini",
			"options" 	=> $font_weight,
			);
					
					
					$of_options[] = array( 	
						"name" 		=> "Footer Logo Section",
						"id" 		=> "footer_logo_section", 
						"std" 		=> 1,
						"folds"		=> 0,
						"type" 		=> "switch"
				);
						
					
						$of_options[] = array( 	
			"name" 		=> "Logo section background color",
			"id" 		=> "footer_logo_bg",
			"fold" 		=> "footer_logo_section",
			"std" 		=> "#000000",
			"type" 		=> "color"
			);	
				
										$of_options[] = array( 	
				"name" 		=> "Footer Logo",
				"desc" 		=> "Upload footer logo",
				"fold" 		=> "footer_logo_section",
				"id" 		=> "footer_logo",
				"std" 		=> "",
				"type" 		=> "upload"
				);	
					
						
			$of_options[] = array( 	
						"name" 		=> "Footer Social Section",
						"desc" 		=> "Footer Social Section",
						"id" 		=> "footer_icons", 
						"std" 		=> 1,
						"folds"		=> 0,
						"type" 		=> "switch"
				);
				
		$of_options[] = array( 	
			"name" 		=> "Social section background color",
			"id" 		=> "footer_icons_bg_color",
			"fold" 		=> "footer_icons",
			"std" 		=> "#ffffff",
			"type" 		=> "color"
			);	
			
				

		$of_options[] = array( 	"name" 		=> "Footer social icons color",
								"id" 		=> "footer_icons_color", 
								"fold" 		=> "footer_icons",
								"std" 		=> "#000000",
								"type" 		=> "color"
						);		
						
				
				
	$of_options[] = array( 	
						"name" 		=> "Facebook",
						"desc" 		=> "Enter Facebook Url",
						"id" 		=> "footer_facebook_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "",
						"type" 		=> "text"
				);
				
			$of_options[] = array( 	"name" 		=> "Twitter",
						"desc" 		=> "Enter Twitter Url",
						"id" 		=> "footer_twitter_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "#",
						"type" 		=> "text"
				);
				
			$of_options[] = array( 	"name" 		=> "Google+",
						"desc" 		=> "Enter Google+ Url",
						"id" 		=> "footer_google_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "",
						"type" 		=> "text"
				);
				
			$of_options[] = array( 	"name" 		=> "Dribbble",
						"desc" 		=> "Enter Dribbble Url",
						"id" 		=> "footer_dribbble_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "",
						"type" 		=> "text"
				);
				
			$of_options[] = array( 	"name" 		=> "YouTube",
						"desc" 		=> "Enter YouTube Url",
						"id" 		=> "footer_youtube_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "#",
						"type" 		=> "text"
				);
				
			$of_options[] = array( 	"name" 		=> "RSS",
						"desc" 		=> "Enter RSS Url",
						"id" 		=> "footer_rss_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "",
						"type" 		=> "text"
				);
				
			$of_options[] = array( 	"name" 		=> "Flickr",
						"desc" 		=> "Enter Flickr Url",
						"id" 		=> "footer_flickr_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "",
						"type" 		=> "text"
				);
				
				
				
			$of_options[] = array( 	"name" 		=> "Linkedin",
						"desc" 		=> "Enter Linkedin Url",
						"id" 		=> "footer_linkedin_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "#",
						"type" 		=> "text"
				);
				
			$of_options[] = array( 	"name" 		=> "Vimeo",
						"desc" 		=> "Enter Vimeo Url",
						"id" 		=> "footer_vimeo_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "",
						"type" 		=> "text"
				);
				
			$of_options[] = array( 	"name" 		=> "Tumblr",
						"desc" 		=> "Enter Tumblr Url",
						"id" 		=> "footer_tumblr_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "",
						"type" 		=> "text"
				);
				
				
			$of_options[] = array( 	"name" 		=> "Pinterest",
						"desc" 		=> "Enter Pinterest Url",
						"id" 		=> "footer_pinterest_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "",
						"type" 		=> "text"
				);
				

				
			$of_options[] = array( 	"name" 		=> "Github",
						"desc" 		=> "Enter github Url",
						"id" 		=> "footer_github_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "",
						"type" 		=> "text"
				);
				
				
			$of_options[] = array( 	"name" 		=> "Instagram",
						"desc" 		=> "Enter Instagram Url",
						"id" 		=> "footer_instagram_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "",
						"type" 		=> "text"
				);
				
				
			$of_options[] = array( 	"name" 		=> "Mail",
						"desc" 		=> "Enter E-mail",
						"id" 		=> "footer_email_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "",
						"type" 		=> "text"
				);
				
			$of_options[] = array( 	"name" 		=> "Website",
						"desc" 		=> "Enter Website Url",
						"id" 		=> "footer_website_url", 
						"fold" 		=> "footer_icons",
						"std" 		=> "",
						"type" 		=> "text"
				);
				
				

				
				
// BACKUP OPTIONS

$of_options[] = array( 	"name" 		=> "Backup Options",
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-slider.png"
				);
				
$of_options[] = array( 	"name" 		=> "Backup and Restore Options",
						"id" 		=> "of_backup",
						"std" 		=> "",
						"type" 		=> "backup",
						"desc" 		=> 'You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.',
				);
				
$of_options[] = array( 	"name" 		=> "Transfer Theme Options Data",
						"id" 		=> "of_transfer",
						"std" 		=> "",
						"type" 		=> "transfer",
						"desc" 		=> 'You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click "Import Options".',
				);			
				
			
				
	}//End function: of_options()
}//End chack if function exists: of_options()
?>
