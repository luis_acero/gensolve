<?php get_header(); ?>
<?php get_template_part('navigation');?>
<?php global $smof_data;
$breadcrums1 = $smof_data['page_heading_font'];
$breadcrums2 = $smof_data['accent_color'];

$overlap = $smof_data['page_heading_bg'];
?>
<div>
	<div class="page-header error bg padding-top-120 padding-bottom-130">
		<div <?php post_class(); ?> >
			<div class="container">
				<div class="row">
					
					
					<div class="col-md-6 align-center wow fadeIn">
						<h4><?php _e('Ooooops...', 'monte'); ?></h4>
					</div>
					<div class="col-md-6 wow fadeIn padding-top-15">
							<h5><?php _e('Looks like something went wrong', 'monte'); ?></h5>
						
						<?php monte_the_breadcrumb($breadcrums1, $breadcrums2); ?>
					</div>
					

				</div>
			</div>
		</div>
		
		
	</div>	
		<div class="slider-overlap" style="background:<?php echo esc_attr($overlap); ?>"></div>
	</div>
		
		<div class="page-content padding-top-200 padding-bottom-200">
			<div class="container">
				<div class="row">
					<div class="col-md-12 align-center error-page">
						
					
						
						
						<h2><?php _e('The page you are looking for', 'monte'); ?> <BR><?php _e('has not been found.', 'monte'); ?></h2>
							
							
						<div class="return-home wow fadeIn">
							<a href="<?php echo esc_url(home_url()); ?>"><i class="fa fa-home"></i></a>
						</div>


						<p>
							<?php _e('Try checking the URL for errors, and hit refresh,', 'monte'); ?>
							
							<?php _e('or click on the', 'monte'); ?>
							<BR>
							
							<?php _e('button above to go to ', 'monte'); ?>
							<strong>
								<?php _e('main page.', 'monte'); ?>
							</strong>
						</p>
						<?php wp_link_pages(); ?> 
						
						
						
					</div>
				</div>
			</div>
		</div>
			
<?php get_footer(); ?>