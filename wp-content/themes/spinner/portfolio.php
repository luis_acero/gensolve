<?php get_header();
/*
 Template name: Portfolio Template
 */
?>
<?php get_template_part('navigation');

if (is_front_page()) { get_template_part('slider'); } 

	$divid = sanitize_html_class($post -> ID);
	$pagetitle = get_the_title();
	$active_menu = esc_attr(get_post_meta($post -> ID, "monte_active_menu_item", true));
	
	$header_type = esc_attr(get_post_meta($post -> ID, "monte_header_type", true));
	$include_slider = esc_attr(get_post_meta($post -> ID, "monte_fw_slider", true));
	$slider_id = esc_attr(get_post_meta($post -> ID, "monte_slider_shortcode", true));
		
	$images = get_post_meta(get_the_ID(), 'monte_header_image', false);
	if ($images) {
		
	$image_src = wp_get_attachment_image_src($images[0], 'full');
	$url = esc_url($image_src[0]);
	}
	
	
	$header_bg = esc_attr(get_post_meta($post -> ID, "monte_header_bg_color", true));
	$heading_color = esc_attr(get_post_meta($post -> ID, "monte_heading_font_color", true));
	$breadcrums1 = esc_attr(get_post_meta($post -> ID, "monte_breadcrums1_color", true));
	$breadcrums2 = esc_attr(get_post_meta($post -> ID, "monte_breadcrums2_color", true));
	
	$alt_page_title = wp_kses_post(get_post_meta($post -> ID, "monte_alt_page_title", true));
	$page_subtitle = wp_kses_post(get_post_meta($post -> ID, "monte_page_subtitle", true));
	
	$page_overlap = esc_attr(get_post_meta($post -> ID, "monte_page_header_overlap", true));
	$page_overlap_bg = esc_attr(get_post_meta($post -> ID, "monte_page_header_overlap_bg", true));
	$title_font_size = esc_attr(get_post_meta($post -> ID, "monte_title_font_size", true));

	if ($alt_page_title) {
		$title = $alt_page_title;
	} else {
		$title = $pagetitle;
	}
?>

<?php if (!is_front_page()) {
	if  (($include_slider == 1) && ($slider_id != '')){
		echo '<div class="header">';
		putRevSlider($slider_id);
		echo '</div>';
	} else { ?>

	
<div <?php if ($header_type == 'color'){ ?> style="background:<?php echo esc_attr($header_bg);?>" <?php } ; ?> <?php if (($header_type == 'image') and ($image_src != '')) { ?> style="background:url(<?php echo esc_url($url);?>)" <?php } ; ?> >
	<div class="page-header <?php echo esc_attr($header_type); ?> padding-top-120 padding-bottom-130" id="<?php echo esc_attr($divid); ?>" >
			<div class="container">
				<div class="row">
					<div class="col-md-6 align-center wow fadeIn">
						<h4 style="color: <?php echo esc_attr($heading_color);?>; font-size: <?php echo esc_attr($title_font_size);?>"><?php echo wp_kses_post($title); ?></h4>
					</div>
					<div class="col-md-6 wow fadeIn padding-top-15">
						<?php if ($page_subtitle) { ?>
							<h5 style="color: <?php echo esc_attr($heading_color);?>"><?php echo esc_attr($page_subtitle);?></h5>
						<?php } else {?>
							<div class="empty padding-top-20"></div>
						<?php }?>
						<?php monte_the_breadcrumb($breadcrums1, $breadcrums2); ?>
					</div>
					

				</div>
			</div>
			</div>
			<?php if ($page_overlap == 'yes') { ?>
			<div class="slider-overlap" style="background:<?php echo esc_attr($page_overlap_bg);?>"></div>
			<?php } ?>
		</div>		
			
		
<?php		
} }?>	

<div class="page-content padding-top-80 padding-bottom-150">
			<div class="container">

				<div class="row">
					<div class="col-md-12 padding-bottom-20 wow align-center fadeIn">
							<?php					
							$portfolio_categories = get_terms('portfolio_categories');
							if ($portfolio_categories):?>
							<ul class="portfolio-buttons wow fadeIn" id="clearajax">
								<li class="active">
								<a href="#" class="all"><?php _e('All', 'monte'); ?></a>
							</li>
							<?php foreach ($portfolio_categories as $portfolio_category):?>
								<li><a href="#" class="filter-<?php echo esc_attr($portfolio_category -> slug); ?>"><?php echo esc_attr($portfolio_category -> name); ?></a></li>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
			</div>
	
	
	
			<div class="container-fluid">
				<div class="row padding-top-40 portfolioHolder" id="portfolioholder">
					
					<div id="wrapper-folio">
								<?php 
					$query_p1 = new WP_Query('post_type=portfolio &posts_per_page=-1');
					if($query_p1->have_posts()): while($query_p1->have_posts()) : $query_p1->the_post(); 
					$portfolio_title = get_the_title($post->id);
			
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'portfolio-thumb' );
					$url = $thumb['0'];
					
					
					
					$categories = get_the_terms( get_the_ID(), 'portfolio_categories' );
					if($categories) : 
						$slug = '';
						foreach ($categories as $category) {
						$slug .= 'filter-';
						$slug .= $category->slug;
						$slug .= ' ';
					} endif;		
					

						?>
	
		
						
						<div class="col-md-3 portfolio eight single portfolioitems hidden portfolio-standalone" data-id="id-<?php echo esc_attr($post -> ID); ?>" data-type="<?php echo esc_attr($slug); ?>">

							<div class="portfolio-picture" style="background-image:url(<?php echo esc_url($url); ?>)">
								<div class="portfolio-hover fol">
									<h6 class="categories"><?php echo strip_tags ( get_the_term_list( $post->ID, 'portfolio_categories', " ",", " ) ); ?></h6>
									<h2 class="project-name"><?php echo get_the_title($post -> ID); ?></h2>
									
									
									<a href="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post -> ID)); ?>" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
									<a href="<?php the_permalink(); ?>"><i class="fa fa-unlink"></i></a>
									
									
								</div>
							</div>

						</div>
						<?php	
					endwhile;
					endif;
					?>	
						
					</div>
				</div>
					<div class="row">
					<div class="col-md-12 align-center padding-top-50" id="loadmore">

						<a href="#" class="button dark col3 eight"><?php _e('MORE PROJECTS', 'monte'); ?></a>
					</div>
				</div>
			</div>
	</div>
	
	
	

<script>
jQuery('.menu-item').each(function() {
	if ( jQuery(this).hasClass('<?php echo esc_attr($active_menu); ?>') ) {
		jQuery(this).addClass('active');
	}
});
</script>
<?php get_footer(); ?>