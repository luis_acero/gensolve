<?php global $smof_data;
if ($smof_data['menu_fullwidth'] == '1') { $container = "container-fluid fullnav"; } else {
	{ $container = "container"; }
}

?>

		<div class="navbar navbar-default navbar-fixed-top sticky" role="navigation">

			<div class="<?php echo esc_attr($container);?>">
				<div class="row">

					<div class="col-md-12">

						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only"><?php _e('Toggle Navigation', 'monte'); ?></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>

							<?php if ($smof_data['logo']) { ?>
								<a class="logo" href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo esc_url($smof_data['logo']); ?>" id="logo" alt="<?php esc_attr(bloginfo('name')); ?>"></a>
							<?php } else { ?>
								<a class="logo" href="<?php echo esc_url(home_url()); ?>"><?php esc_attr(bloginfo('name')); ?></a>
							<?php } ?>
						</div>

						<div class="navbar-collapse collapse">
							
												<?php 
							if ($smof_data['enable_search'] == 1) { ?>
								<a href="#" class="monte-search" id="searchicon"><i class="fa fa-search"></i></a>
									<div class="searchbox">
										<form method="get" id="searchforms" action="<?php echo esc_url(home_url('/')); ?>" role="search" class="search">
											<div class="form-group">
												<input type="text" class="field form-control searchfrield" name="s" value="<?php echo esc_attr(get_search_query()); ?>" id="ss" placeholder="type and press enter">
											</div>
										</form>
										
										
									</div>
							
						<?php }
					
						?>
						
						

						<?php 
					if (has_nav_menu( 'primary-menu' )) {
   					wp_nav_menu(array('container' => '', 'menu_id' => 'primary-menu', 'menu_class' => 'nav navbar-nav navbar-right', 'theme_location' => 'primary-menu'));
					} else echo '<a href="'.esc_url(home_url()).'/wp-admin/nav-menus.php">Configure Menu</a>';
					
				
			?> 
						
						
							
						</div>

					</div>
				</div>
			</div>

		</div>