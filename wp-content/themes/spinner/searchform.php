<form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>" role="search" class="search">
	<div class="form-group">
		<input type="text" class="field form-control" name="s" value="<?php echo esc_attr(get_search_query()); ?>" id="s" placeholder="TYPE AND HIT ENTER">
	</div>
</form>