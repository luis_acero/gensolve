<?php get_header(); ?>
<?php
get_template_part('navigation');
if (is_front_page()) { get_template_part('slider'); } 

if (have_posts()) : while (have_posts()) : the_post();

	$divid = sanitize_html_class($post -> ID);
	$pagetitle = get_the_title();
	$active_menu = esc_attr(get_post_meta($post -> ID, "monte_active_menu_item", true));
	
	$header_type = esc_attr(get_post_meta($post -> ID, "monte_header_type", true));
	$include_slider = esc_attr(get_post_meta($post -> ID, "monte_fw_slider", true));
	$slider_id = esc_attr(get_post_meta($post -> ID, "monte_slider_shortcode", true));
		
	$images = get_post_meta(get_the_ID(), 'monte_header_image', false);
	if ($images) {
		
	$image_src = wp_get_attachment_image_src($images[0], 'full');
	$url = esc_url($image_src[0]);
	}
	
	
	$header_bg = esc_attr(get_post_meta($post -> ID, "monte_header_bg_color", true));
	$heading_color = esc_attr(get_post_meta($post -> ID, "monte_heading_font_color", true));
	$breadcrums1 = esc_attr(get_post_meta($post -> ID, "monte_breadcrums1_color", true));
	$breadcrums2 = esc_attr(get_post_meta($post -> ID, "monte_breadcrums2_color", true));
	
	$alt_page_title = wp_kses_post(get_post_meta($post -> ID, "monte_alt_page_title", true));
	$page_subtitle = wp_kses_post(get_post_meta($post -> ID, "monte_page_subtitle", true));
	
	$page_overlap = esc_attr(get_post_meta($post -> ID, "monte_page_header_overlap", true));
	$page_overlap_bg = esc_attr(get_post_meta($post -> ID, "monte_page_header_overlap_bg", true));
	$title_font_size = esc_attr(get_post_meta($post -> ID, "monte_title_font_size", true));

	if ($alt_page_title) {
		$title = $alt_page_title;
	} else {
		$title = $pagetitle;
	}
	
?>

<?php if (!is_front_page()) {
	if  (($include_slider == 1) && ($slider_id != '')){
		echo '<div class="header">';
		putRevSlider(esc_attr($slider_id));
		if ($smof_data['slider_overlay'] == 1) {
			echo '<div class="slider-overlap" style="background:'.$smof_data['slider_overlay_bg'].'"></div>';
		} 
		echo '</div>';
	} else { ?>

<div <?php if ($header_type == 'color'){ ?> style="background:<?php echo esc_attr($header_bg);?>" <?php } ; ?> <?php if (($header_type == 'image') and ($image_src != '')) { ?> style="background:url(<?php echo esc_url($url);?>)" <?php } ; ?> >
	<div class="page-header <?php echo esc_attr($header_type); ?> padding-top-120 padding-bottom-130" id="<?php echo esc_attr($divid); ?>" >
			<div class="container">
				<div class="row">
					<div class="col-md-6 align-center wow fadeIn">
						<h4 style="color: <?php echo esc_attr($heading_color);?>; font-size: <?php echo esc_attr($title_font_size);?>"><?php echo wp_kses_post($title); ?></h4>
					</div>
					<div class="col-md-6 wow fadeIn padding-top-15">
						<?php if ($page_subtitle) { ?>
							<h5 style="color: <?php echo esc_attr($heading_color);?>"><?php echo esc_attr($page_subtitle);?></h5>
						<?php } else {?>
							<div class="empty padding-top-20"></div>
						<?php }?>
						<?php monte_the_breadcrumb($breadcrums1, $breadcrums2); ?>
					</div>
					

				</div>
			</div>
			</div>
			<?php if ($page_overlap == 'yes') { ?>
			<div class="slider-overlap" style="background:<?php echo esc_attr($page_overlap_bg);?>"></div>
			<?php } ?>
		</div>		
		
<?php		
} }?>		
		
		<div class="page-content <?php if (!is_front_page()) { ?>padding-top-70<?php }?> padding-bottom-80">
			<div class="container">
				<div class="row inner-content-min">
					<div class="col-md-12 inner-content clearfix">
						<?php the_content(); ?>
					</div>
						<div class="col-md-12 blog-content">
							<?php comments_template(); ?>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	

	

<script>
jQuery('.menu-item').each(function() {
	if ( jQuery(this).hasClass('<?php echo esc_attr($active_menu); ?>') ) {
		jQuery(this).addClass('active');
	}
});
</script>
<?php
endwhile;
endif;
?>
<?php get_footer(); ?>