<?php get_header();
/*
 Template name: Blog Template
 */
get_template_part('navigation');


if (have_posts()) : while (have_posts()) : the_post();

	$divid = sanitize_html_class($post -> ID);
	$pagetitle = get_the_title();
	$active_menu = esc_attr(get_post_meta($post -> ID, "monte_active_menu_item", true));
	
	$header_type = esc_attr(get_post_meta($post -> ID, "monte_header_type", true));
	$include_slider = esc_attr(get_post_meta($post -> ID, "monte_fw_slider", true));
	$slider_id = esc_attr(get_post_meta($post -> ID, "monte_slider_shortcode", true));
		
	$images = get_post_meta(get_the_ID(), 'monte_header_image', false);
	if ($images) {
		
	$image_src = wp_get_attachment_image_src($images[0], 'full');
	$url = esc_url($image_src[0]);
	}
	
	
	$header_bg = esc_attr(get_post_meta($post -> ID, "monte_header_bg_color", true));
	$heading_color = esc_attr(get_post_meta($post -> ID, "monte_heading_font_color", true));
	$breadcrums1 = esc_attr(get_post_meta($post -> ID, "monte_breadcrums1_color", true));
	$breadcrums2 = esc_attr(get_post_meta($post -> ID, "monte_breadcrums2_color", true));
	
	$alt_page_title = wp_kses_post(get_post_meta($post -> ID, "monte_alt_page_title", true));
	$page_subtitle = wp_kses_post(get_post_meta($post -> ID, "monte_page_subtitle", true));
	
	$page_overlap = esc_attr(get_post_meta($post -> ID, "monte_page_header_overlap", true));
	$page_overlap_bg = esc_attr(get_post_meta($post -> ID, "monte_page_header_overlap_bg", true));
	$title_font_size = esc_attr(get_post_meta($post -> ID, "monte_title_font_size", true));
	
	$disable_header = esc_attr(get_post_meta($post -> ID, "monte_disable_header", true));
	$disable_footer1 = esc_attr(get_post_meta($post -> ID, "monte_disable_widgetized", true));
	$disable_footer2 = esc_attr(get_post_meta($post -> ID, "monte_disable_social", true));
	$disable_footer3 = esc_attr(get_post_meta($post -> ID, "monte_disable_logo", true));
	
	$bg1 = esc_attr(get_post_meta($post -> ID, "monte_bg1", true));
	$font1 = esc_attr(get_post_meta($post -> ID, "monte_font1", true));
	$bg2 = esc_attr(get_post_meta($post -> ID, "monte_bg2", true));
	$font2 = esc_attr(get_post_meta($post -> ID, "monte_font2", true));
	$bg3 = esc_attr(get_post_meta($post -> ID, "monte_bg3", true));
	$font3 = esc_attr(get_post_meta($post -> ID, "monte_font3", true));
	$bg4 = esc_attr(get_post_meta($post -> ID, "monte_bg4", true));
	$font4 = esc_attr(get_post_meta($post -> ID, "monte_font4", true));
	$bg5 = esc_attr(get_post_meta($post -> ID, "monte_bg5", true));
	$font5 = esc_attr(get_post_meta($post -> ID, "monte_font5", true));
	$bg6 = esc_attr(get_post_meta($post -> ID, "monte_bg6", true));
	$font6 = esc_attr(get_post_meta($post -> ID, "monte_font6", true));

	if ($alt_page_title) {
		$title = $alt_page_title;
	} else {
		$title = $pagetitle;
	}
	
	endwhile;
	endif;
	
	$display_type = esc_attr(get_post_meta($post -> ID, "monte_blog_type", true));
	if (is_front_page() && $display_type == 'sidebar_list') { get_template_part('slider'); } 
?>


<?php if ($display_type == 'spinner_style') { ?>
	<?php if ($disable_header == 1) { ?>
<div <?php if ($header_type == 'color'){ ?> style="background:<?php echo esc_attr($header_bg);?>" <?php } ; ?> <?php if (($header_type == 'image') and ($image_src != '')) { ?> style="background:url(<?php echo esc_url($url);?>)" <?php } ; ?> >
	<div class="page-header-bg <?php echo esc_attr($header_type); ?> padding-top-60">
			<div class="container">
				<div class="row">
				</div>
			</div>
			</div>
		</div>	
	<?php } else { ?>
		
		<div <?php if ($header_type == 'color'){ ?> style="background:<?php echo esc_attr($header_bg);?>" <?php } ; ?> <?php if (($header_type == 'image') and ($image_src != '')) { ?> style="background:url(<?php echo esc_url($url);?>)" <?php } ; ?> >
	<div class="page-header <?php echo esc_attr($header_type); ?> padding-top-120 padding-bottom-130" id="<?php echo esc_attr($divid); ?>" >
			<div class="container">
				<div class="row">
					<div class="col-md-6 align-center wow fadeIn">
						<h4 style="color: <?php echo esc_attr($heading_color);?>; font-size: <?php echo esc_attr($title_font_size);?>"><?php echo wp_kses_post($title); ?></h4>
					</div>
					<div class="col-md-6 wow fadeIn padding-top-15">
						<?php if ($page_subtitle) { ?>
							<h5 style="color: <?php echo esc_attr($heading_color);?>"><?php echo esc_attr($page_subtitle);?></h5>
						<?php } else {?>
							<div class="empty padding-top-20"></div>
						<?php }?>
						<?php monte_the_breadcrumb($breadcrums1, $breadcrums2); ?>
					</div>
					

				</div>
			</div>
			</div>
		</div>	
	<?php }	?>
		
	
	<div id="<?php echo esc_attr($divid); ?>" class="page-content styled">

		<?php 		
					$pagenumber = (get_query_var('paged')) ? get_query_var('paged') : 1;
					query_posts("post_type=post &posts_per_page=6 &paged=" . get_query_var('paged')); 
			
					$i=0;
					
					$postsperpage = 6;
					
					$page = (get_query_var('page')) ? get_query_var('page') : 1;
					$num_of_posts = $wp_query->found_posts;
		
					$postsnumber = $pagenumber * $postsperpage;
					$onpage = $postsnumber - $postsperpage;
					
					if(have_posts()): while(have_posts()) : the_post(); 
					$post_name = $post->post_name;
    				$post_id = get_the_ID();
					$i++;
					
					
					$final = $onpage + $i;

					if ($i == 1) {
						$bg = $bg1;
						$font = $font1;
					} else if ($i == 2) {
						$bg = $bg2;
						$font = $font2;
					} else if ($i == 3) {
						$bg = $bg3;
						$font = $font3;
					} else if ($i == 4) {
						$bg = $bg4;
						$font = $font4;
					} else if ($i == 5) {
						$bg = $bg5;
						$font = $font5;
					} else if ($i == 6) {
						$bg = $bg6;
						$font = $font6;
					}
					?>
					
					
		<div class="container-fluid post<?php echo esc_attr($i); ?> blog-style" style="background: <?php echo esc_attr($bg); ?>">
				<div class="col-md-12">
				<div class="container">
				<div class="row padding-top-50 padding-bottom-50">
					<div class="col-md-9">
						<span class="styled-links" style="color: <?php echo esc_attr($font); ?>"><?php echo '' . __('on ', 'monte') . ' ' . get_the_date(); ?> <?php echo '' . __('in ', 'monte') . ' ' . get_the_category_list(', ', 'single', $post -> ID); ?></span>
						<a href="<?php the_permalink(); ?>"><h1 style="color: <?php echo esc_attr($font); ?>"><?php the_title(); ?></h1></a>
						<p style="color: <?php echo esc_attr($font); ?>">
							<?php echo monte_get_masonry_excerpt(); ?>
						</p>
					</div>
					<div class="col-md-3 blog-style-icon">
						<?php if (get_post_format() == '') { ?>
							<i style="color: <?php echo esc_attr($font); ?>" class="fa fa-camera"></i>
						<?php } else if (get_post_format() == 'gallery') { ?>
							<i style="color: <?php echo esc_attr($font); ?>" class="fa fa-camera"></i>
						<?php } else if (get_post_format() == 'video') { ?>
							<i style="color: <?php echo esc_attr($font); ?>" class="fa fa-video-camera"></i>
						<?php } else if (get_post_format() == 'audio') { ?>
							<i style="color: <?php echo esc_attr($font); ?>" class="fa fa-microphone"></i>
						<?php } else if (get_post_format() == 'quote') { ?>
							<i style="color: <?php echo esc_attr($font); ?>" class="fa fa-quote-right"></i>
						<?php } ?>
						
					</div>
				</div>
				</div>
				</div>
				
				<?php if (($i == $postsperpage) || ($final == $num_of_posts)) { ?>
					<?php if(get_next_posts_link() || get_previous_posts_link()) { ?>
						<div class="col-md-12 padding-bottom-60 align-center">
							<div class="pagination special-pagination wow fadeIn" style="color: <?php echo esc_attr($font); ?>">
								
							
							<?php 
								
										 $args = array(
											'prev_next'          => False,
											'show_all'           => True,
										
										);
								
											echo paginate_links($args); 
										 	?>				
				
							</div>
						</div>
					<?php } ?>
				<?php } ?>
				
			</div>

					
					<?php
					endwhile;
					endif;
					?>
							
					
		
				<?php 
				wp_reset_postdata();
				?>

</div>



<?php } else if ($display_type == 'sidebar_list') { ?>
	
<?php if (!is_front_page()) {
	if  (($include_slider == 1) && ($slider_id != '')){
		echo '<div class="header">';
		putRevSlider($slider_id);
		if ($smof_data['slider_overlay'] == 1) {
			echo '<div class="slider-overlap" style="background:'.$smof_data['slider_overlay_bg'].'"></div>';
		} 
		echo '</div>';
	} else { ?>

	
<div <?php if ($header_type == 'color'){ ?> style="background:<?php echo esc_attr($header_bg);?>" <?php } ; ?> <?php if (($header_type == 'image') and ($image_src != '')) { ?> style="background:url(<?php echo esc_url($url);?>)" <?php } ; ?> >
	<div class="page-header <?php echo esc_attr($header_type); ?> padding-top-120 padding-bottom-130" id="<?php echo esc_attr($divid); ?>" >
			<div class="container">
				<div class="row">
					<div class="col-md-6 align-center wow fadeIn">
						<h4 style="color: <?php echo esc_attr($heading_color);?>; font-size: <?php echo esc_attr($title_font_size);?>"><?php echo wp_kses_post($title); ?></h4>
					</div>
					<div class="col-md-6 wow fadeIn padding-top-15">
						<?php if ($page_subtitle) { ?>
							<h5 style="color: <?php echo esc_attr($heading_color);?>"><?php echo esc_attr($page_subtitle);?></h5>
						<?php } else {?>
							<div class="empty padding-top-20"></div>
						<?php }?>
						<?php monte_the_breadcrumb($breadcrums1, $breadcrums2); ?>
					</div>
					

				</div>
			</div>
			</div>
			<?php if ($page_overlap == 'yes') { ?>
			<div class="slider-overlap" style="background:<?php echo esc_attr($page_overlap_bg);?>"></div>
			<?php } ?>
		</div>		
			
		
<?php		
} }?>	

	
	<div class="page-content <?php if (!is_front_page()) { ?>padding-top-70<?php }?> padding-bottom-80">
		<div class="container">
			<div class="row">
				<div class="col-md-9 blog-list clearfix">
					<div class="blog-content">
						<?php query_posts("post_type=post &posts_per_page=5 &paged=" . get_query_var('paged')); 
						if(have_posts()): while(have_posts()) : the_post(); 
						 $post_name = $post->post_name;
    					$post_id = get_the_ID();
						?>
						<div class="post fitvid <?php if (is_sticky()) {?>stickypost<?php } ?>">
							<?php get_template_part( 'post-formats/single', get_post_format() );  ?>
								<div class="row inner-content">
									<div class="col-md-2 col-sm-2 col-xs-3 align-center post-left">
										<i class="fa fa-camera"></i>
										<h2 class="month"><?php echo the_time( 'M' ); ?></h2>
										<h1 class="day"><?php echo the_time( 'd' ); ?></h1>
									</div>
									<div class="col-md-10 col-sm-10 col-xs-9">
										<a href="<?php the_permalink(); ?>"><h1 class="post-title-big"><?php the_title(); ?></h1></a>
										

											<div class="post-info-bar">
											<div class="info-item">
												<i class="fa fa-pencil-square"></i>
												<span><?php echo '' . __('by', 'monte') . ' ' . get_the_author_link(); ?></span>
											</div>
											<div class="info-item">
												<i class="fa fa-calendar"></i>
												<span><?php echo '' . __('on ', 'monte') . ' ' . get_the_date(); ?></span>
											</div>
											<div class="info-item">
												<i class="fa fa-folder-open"></i>
												<span><?php echo '' . __('in ', 'monte') . ' ' . get_the_category_list(', ', 'single', $post -> ID); ?></span>
											</div>
											<div class="info-item">
												<i class="fa fa-comments"></i>
												<span><a href="<?php comments_link(); ?>"><?php comments_number( 'No Comments', '1 Comment', '% Comments' ); ?></a></span>
											</div>
										</div>

										<div class="post-content wow fadeIn">
											<p><?php echo monte_get_blog_excerpt(); ?> </p>

										</div>
										
										<div class="readmore">
											<a class="button darkgrey" href="<?php the_permalink(); ?>"><?php echo __('Read more', 'monte'); ?></a>
										</div>
										

									</div>

								</div>
						</div>
					
							<?php
							endwhile;
							endif;
							?>
						</div>
						<div class="pagination wow fadeIn">
							<?php if(get_next_posts_link() || get_previous_posts_link()) {
								
										 $args = array(
											'prev_next'          => False,
										
										);
								
											echo paginate_links($args); 
										 	} ?>				
						</div>
						<?php 
						wp_reset_postdata();
						?>
				</div>
				<div class="col-md-3 sidebar">
					<?php get_sidebar('blog'); ?>
				</div>
			</div>
		</div>
	</div>

<?php } ?>	


<script>
jQuery('.menu-item').each(function() {
	if ( jQuery(this).hasClass('<?php echo esc_attr($active_menu); ?>') ) {
		jQuery(this).addClass('active');
	}
	
});
<?php if ($display_type == 'spinner_style') { ?>
	<?php if ($disable_footer1 == 1) { ?>
	var disable_footer1 = 1;
	<?php } else { ?>
	var disable_footer1 = 0;	
	<?php }?>
	<?php if ($disable_footer2 == 1) { ?>
	var disable_footer2 = 1;
	<?php } ?>
	<?php if ($disable_footer3 == 1) { ?>
	var disable_footer3 = 1;
	<?php } ?>
<?php } ?>
</script>
<?php get_footer(); ?>