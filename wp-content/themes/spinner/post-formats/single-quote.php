<?php $quote = esc_attr(get_post_meta(get_the_ID(), 'monte_featured_quote', true));
$source = esc_attr(get_post_meta(get_the_ID(), 'monte_featured_source', true));
?>
	<blockquote class="blockquote-dark quote-bg-color wow fadeIn">
	<p><?php echo esc_attr($quote); ?></p>
		<footer><?php echo esc_attr($source); ?></footer>
	</blockquote>
	