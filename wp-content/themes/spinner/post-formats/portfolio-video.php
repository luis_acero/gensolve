<div class="fitvid">
	<?php
	if (get_post_meta(get_the_ID(), 'monte_portfolio_video_type', true) == 'youtube') {
		echo '<iframe class="wow fadeIn" width="1280" height="720" src="http://www.youtube.com/embed/' . get_post_meta(get_the_ID(), 'monte_portfolio_video_embed', true) . '" style="border:0;" allowfullscreen></iframe>';
	} else if (get_post_meta(get_the_ID(), 'monte_portfolio_video_type', true) == 'vimeo') {
		echo '<iframe src="//player.vimeo.com/video/' . get_post_meta(get_the_ID(), 'monte_portfolio_video_embed', true) . '?title=0&amp;byline=0&amp;portrait=0&amp;color=cbcbcb" width="960" height="540" style="border:0;"  allowfullscreen></iframe>';
	}
	?>
</div>