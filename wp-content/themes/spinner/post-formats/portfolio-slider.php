<div class="portfolio-featured-slider">
	<?php
	$slides = get_post_meta(get_the_ID(), 'monte_portfolio_slides', false);
	if ($slides) {
		echo '<div class="flexslider"><ul class="slides">';
		foreach ($slides as $slide) {
			$image_src = wp_get_attachment_image_src($slide, 'full');
			$image_src2 = wp_get_attachment_image_src($slide, 'portfolio-feat');
			$image_src = $image_src[0];
			$image_src2 = $image_src2[0];
			echo '<li><a href="' . esc_url($image_src) . '" data-rel="prettyPhoto[ex-' . get_the_ID() . ']"><img src="' . esc_url($image_src2) . '" alt="portfolio"></a></li>';
		};
		echo '</ul></div>';
	}
?>
</div>