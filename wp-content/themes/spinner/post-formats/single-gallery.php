<?php
	$slides = get_post_meta(get_the_ID(), 'monte_featured_slides', false);
	if ($slides) {
		echo '<div class="flexslider wow fadeIn"><ul class="slides">';
		foreach ($slides as $slide) {

			$image_src = wp_get_attachment_image_src($slide, 'full');
			$image_src2 = wp_get_attachment_image_src($slide, 'post-featured-image');
			$image_src = $image_src[0];
			$image_src2 = $image_src2[0];

			echo '<li><a href="' . esc_url($image_src) . '" data-rel="prettyPhoto[' . get_the_ID() . ']"><img src="' . esc_attr($image_src2) . '" alt=""></a></li>';
		};
		echo '</ul></div>';
	}
	?>