<?php if ( has_post_thumbnail() ) {
?>

<div class="news-picture">
	<a data-rel="prettyPhoto" href="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post -> ID)); ?>" title="<?php esc_attr(the_title()); ?>"> 
		<div class="news-picture" style="background-image:url('<?php echo wp_get_attachment_url(get_post_thumbnail_id($post -> ID)); ?>')">
			<div class="blog-date">
				<span class="day"><?php echo the_time( 'd' ); ?></span>
				<span class="month"><?php echo the_time( 'M' ); ?></span>
			</div>
		</div> 
	</a>
</div>
<?php } ?>