<?php if ( post_password_required() ) : ?>
<p class="access-denied"><?php _e('Access denied. This post is password protected.', 'monte'); ?></p>
<?php
return;
endif;
?>
<div id="comments"></div>
<?php if ( have_comments() ) : ?>
<h5 class="comments-headline"><?php printf(_n(__('1 Comment', 'monte'), '%1$s ' . __('Comments', 'monte') . '', get_comments_number()), number_format_i18n(get_comments_number())); ?>
</h5>
<ul class="comments">
	<?php wp_list_comments(array('avatar_size' => '100', 'type' => 'comment', 'callback' => 'monte_comment')); ?>

</ul>

 <div class="comments-pagination wow fadeIn animated">
  <?php paginate_comments_links(); ?> 
 </div>


<?php endif; ?>

<?php
	$commenter = wp_get_current_commenter();
	$req = get_option('require_name_email');
//	$aria_req = ($req ? ' aria-required="true"' : '');

	if (esc_attr($commenter['comment_author']) != '') { $val_author = esc_attr($commenter['comment_author']);
	} else { $val_author = 'Name';
	}
	if (esc_attr($commenter['comment_author_email']) != '') { $val_author_email = esc_attr($commenter['comment_author_email']);
	} else { $val_author_email = 'E-mail';
	}
	if (esc_attr($commenter['comment_author_url']) != '' && esc_attr($commenter['comment_author_url']) != 'http://') { $val_author_url = esc_attr($commenter['comment_author_url']);
	} else { $val_author_url = 'http://';
	}

	$fields = array('author' => '
	<div class="comment-form">
		<div class="commentform row">
	
		<div class="col-md-4 col-sm-4 col-xs-12">
			<input type="text" class="form-control txtFld required" id="author" placeholder="Name" name="author" aria-required="true" />
		</div>
		', 'email' => '
		<div class="col-md-4 col-sm-4 col-xs-12">
			<input type="text" class="form-control txtFld required" id="email" placeholder="Email" name="email" aria-required="true" />
		</div>
		', 'url' => '
		<div class="col-md-4 col-sm-4 col-xs-12">
			<input type="text" class="form-control txtFld" id="url" placeholder="Website" name="url" />
		</div>
		</div>
	</div>
	' , );
		$comments_args = array(
		'fields' => $fields, 
		'title_reply' => '' . __('Leave a comment', 'monte') . '', 
		'title_reply_to' => '' . __('Leave a reply to', 'monte') . ' %s', 
		'comment_notes_after' => '', 
		'comment_field' => ' <textarea class="form-control txtAra comment-textarea comment required" id="comment" name="comment" rows="2" placeholder="Comment" aria-required="true"></textarea>',
		'comment_notes_after' => '
		<button type="submit" class="button dark" id="submit-new">
		<span>'.__('Submit').'</span>
		</button>
		');
		comment_form($comments_args);
	?>