<?php get_header(); 
get_template_part('navigation');
?> 

<?php
if (have_posts()) : while (have_posts()) : the_post();

	$divid = sanitize_html_class($post -> ID);
	$pagetitle = get_the_title();
	$active_menu = esc_attr(get_post_meta($post -> ID, "monte_active_menu_item", true));
	
	$header_type = esc_attr(get_post_meta($post -> ID, "monte_portfolio_header_type", true));
	$include_slider = esc_attr(get_post_meta($post -> ID, "monte_portfolio_fw_slider", true));
	$slider_id = esc_attr(get_post_meta($post -> ID, "monte_portfolio_post_shortcode", true));
		
	$images = get_post_meta(get_the_ID(), 'monte_portfolio_header_image', false);
	if ($images) {
		
	$image_src = wp_get_attachment_image_src($images[0], 'full');
	$url = esc_url($image_src[0]);
	}
	
	$header_bg = esc_attr(get_post_meta($post -> ID, "monte_portfolio_header_bg_color", true));
	$heading_color = esc_attr(get_post_meta($post -> ID, "monte_portfolio_heading_font_color", true));
	$breadcrums1 = esc_attr(get_post_meta($post -> ID, "monte_portfolio_breadcrums1_color", true));
	$breadcrums2 = esc_attr(get_post_meta($post -> ID, "monte_portfolio_breadcrums2_color", true));
	
	$alt_page_title = wp_kses_post(get_post_meta($post -> ID, "monte_alt_portfolio_title", true));
	$page_subtitle = wp_kses_post(get_post_meta($post -> ID, "monte_portfolio_subtitle", true));
	
	$page_overlap = esc_attr(get_post_meta($post -> ID, "monte_portfolio_header_overlap", true));
	$page_overlap_bg = esc_attr(get_post_meta($post -> ID, "monte_portfolio_header_overlap_bg", true));
	$title_font_size = esc_attr(get_post_meta($post -> ID, "monte_portfolio_title_font_size", true));
	
	$portfolio_type = esc_attr(get_post_meta($post -> ID, "monte_portfolio_item_type", true));
	
	$previous_posts_image = '<i class="fa fa-angle-left"></i>';
	$next_posts_image = '<i class="fa fa-angle-right"></i>';
	
	
	if ($alt_page_title) {
		$title = $alt_page_title;
	} else {
		$title = $pagetitle;
	}
	
	
?>


<?php if (!is_front_page()) {
	if  (($include_slider == 1) && ($slider_id != '')){
		echo '<div class="header">';
		putRevSlider($slider_id);
		if ($smof_data['slider_overlay'] == 1) {
			echo '<div class="slider-overlap" style="background:'.$smof_data['slider_overlay_bg'].'"></div>';
		} 
		echo '</div>';
	} else { ?>


	
<div <?php if ($header_type == 'color'){ ?> style="background:<?php echo esc_attr($header_bg);?>" <?php } ; ?> <?php if (($header_type == 'image') and ($image_src != '')) { ?> style="background:url(<?php echo esc_url($url);?>)" <?php } ; ?> >
	<div class="page-header <?php echo esc_attr($header_type); ?> padding-top-120 padding-bottom-130" id="<?php echo esc_attr($divid); ?>" >
			<div class="container">
				<div class="row">
					<div class="col-md-6 align-center wow fadeIn">
						<h4 style="color: <?php echo esc_attr($heading_color);?>; font-size: <?php echo esc_attr($title_font_size);?>"><?php echo wp_kses_post($title); ?></h4>
					</div>
					<div class="col-md-6 wow fadeIn padding-top-15">
						<?php if ($page_subtitle) { ?>
							<h5 style="color: <?php echo esc_attr($heading_color);?>"><?php echo esc_attr($page_subtitle);?></h5>
						<?php } else {?>
							<div class="empty padding-top-20"></div>
						<?php }?>
						<?php monte_the_breadcrumb($breadcrums1, $breadcrums2); ?>
					</div>
					

				</div>
			</div>
			</div>
			<?php if ($page_overlap == 'yes') { ?>
			<div class="slider-overlap" style="background:<?php echo esc_attr($page_overlap_bg);?>"></div>
			<?php } ?>
		</div>	
		
<?php		
} }?>		



		<div class="page-content padding-top-80 padding-bottom-100">
			<div class="portfolio-item-navigation">
				<div class="container">
					<div class="row">
						<div class="col-md-12 align-center">
							<?php if(get_previous_post()) { ?>
								<span class="prev-item">
								<?php previous_post_link('%link',  $previous_posts_image );?>
								</span>
							<?php } ?>
							
							
							<?php if(get_next_post()) { ?>
								<span class="next-item">
								<?php next_post_link('%link',  $next_posts_image );?>
								</span>
							<?php } ?>
						</div>
					</div>
				</div>

			</div>

			<div id="ajaxContainer">
				<div class="portfolio-item-navigation-ajax">
					<div class="container">
						<div class="row">
							<div class="col-md-12 align-center padding-top-50">
								
									<?php if(get_previous_post()) { ?>
										<span class="prev-item prev_post">
										<?php previous_post_link('%link',  $previous_posts_image );?>
								
										</span>
									<?php } ?>
										<a href="#" class="exit-item close">x</a>
			
									<?php if(get_next_post()) { ?>
										<span class="next-item next_post">
										<?php next_post_link('%link',  $next_posts_image );?>
										</span>
									<?php } ?>
							</div>
						</div>
					</div>
				</div>

				<div class="page-content padding-top-80">
					<div class="container">
						<div class="row">
							<div class="col-md-8">

								<div class="singleportfolio wow fadeIn">
								<?php
									if ($portfolio_type == 'slider') { get_template_part( 'post-formats/portfolio', 'slider');
									} else if ($portfolio_type == 'video') { get_template_part( 'post-formats/portfolio', 'video');
									} else { get_template_part( 'post-formats/portfolio', 'image');
									}
								?>	

								</div>

							</div>
							<div class="col-md-4 portfolio-desc">

								<h2 class="portfolio-title"><?php the_title(); ?></h2>

								<?php echo the_content(); ?>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div>



<script>
jQuery('.menu-item').each(function() {
	if ( jQuery(this).hasClass('<?php echo esc_attr($active_menu); ?>') ) {
		jQuery(this).addClass('active');
	}
});
</script>
<?php 
endwhile;
endif;
?> 


<?php get_footer(); ?>