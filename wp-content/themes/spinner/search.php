<?php get_header(); ?> 
<?php get_template_part('navigation'); ?>
<?php global $smof_data;
$breadcrums1 = $smof_data['page_heading_font'];
$breadcrums2 = $smof_data['accent_color'];
$overlap = $smof_data['background_color'];
?>


<div>
	<div class="page-header error bg padding-top-120 padding-bottom-130">
		<div <?php post_class(); ?> >
			<div class="container">
				<div class="row">
					
					
					<div class="col-md-6 align-center wow fadeIn">
						<h4><?php printf(('%s'), get_search_query()); ?></h4>
					</div>
					<div class="col-md-6 wow fadeIn padding-top-15">
							<h5><?php _e('Search results', 'monte'); ?></h5>
						
						<?php monte_the_breadcrumb($breadcrums1, $breadcrums2); ?>
					</div>
					

				</div>
			</div>
		</div>
		
		
	</div>	
		<div class="slider-overlap" style="background:<?php echo esc_attr($overlap); ?>"></div>
	</div>
	
	
	<div class="page-content padding-top-80 padding-bottom-80">
		<div class="container">
			<div class="row">
				<div class="col-md-9 blog-list clearfix">
					<div class="blog-content">
						<?php
						if(have_posts()): while(have_posts()) : the_post(); 
						 $post_name = $post->post_name;
    					$post_id = get_the_ID();
						?>
						<div class="post fitvid">
							<?php get_template_part( 'post-formats/single', get_post_format() );  ?>
								<div class="row inner-content">
									<div class="col-md-2 col-sm-2 col-xs-3 align-center post-left">
										<i class="fa fa-camera"></i>
										<h2 class="month"><?php echo the_time( 'M' ); ?></h2>
										<h1 class="day"><?php echo the_time( 'd' ); ?></h1>
									</div>
									<div class="col-md-10 col-sm-10 col-xs-9">
										<a href="<?php the_permalink(); ?>"><h1 class="post-title-big"><?php the_title(); ?></h1></a>
										

										<div class="post-info-bar">
											<div class="info-item">
												<i class="fa fa-pencil-square"></i>
												<span><?php echo '' . __('by', 'monte') . ' ' . get_the_author_link(); ?></span>
											</div>
											<div class="info-item">
												<i class="fa fa-calendar"></i>
												<span><?php echo '' . __('on ', 'monte') . ' ' . get_the_date(); ?></span>
											</div>
											<div class="info-item">
												<i class="fa fa-folder-open"></i>
												<span><?php echo '' . __('in ', 'monte') . ' ' . get_the_category_list(', ', 'single', $post -> ID); ?></span>
											</div>
											<div class="info-item">
												<i class="fa fa-comments"></i>
												<span><a href="<?php comments_link(); ?>"><?php comments_number( 'No Comments', '1 Comment', '% Comments' ); ?></a></span>
											</div>
										</div>

										<div class="post-content wow fadeIn">
											<p><?php echo monte_get_blog_excerpt(); ?> </p>

										</div>
										<div class="readmore">
											<a class="button darkgrey" href="<?php the_permalink(); ?>"><?php echo __('Read more', 'monte'); ?></a>
										</div>

									</div>

								</div>
						</div>
					
							<?php
							endwhile;
							else: ?>
								<div class="post fitvid align-center">
										<h1 class="post-title-big"><?php echo __('No matching results found', 'monte'); ?></h1>
								</div>
								
							<?php endif;
							?>
						</div>
							<div class="pagination wow fadeIn">
							<?php if(get_next_posts_link() || get_previous_posts_link()) {
								
										 $args = array(
											'prev_next'          => False,
										
										);
								
											echo paginate_links($args); 
										 	} ?>				
						</div>
				</div>
				<div class="col-md-3 sidebar">
					<?php get_sidebar('blog'); ?>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>