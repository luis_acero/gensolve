<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title('|', true, 'right'); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php
global $smof_data;
if ($smof_data['favicon']) {
	echo '<link rel="shortcut icon" href="' . esc_url($smof_data['favicon']) . '"/>';
}
?>
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div class="preloader"></div>
	