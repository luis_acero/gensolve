<?php
global $smof_data;
if ($smof_data['revolution_switch'] == 1) {
	$slider_id = esc_attr($smof_data['revolution_slider_id']);
	if ($slider_id !== 'None') {
		echo '<div class="header">';
		putRevSlider(esc_attr($slider_id));
		if ($smof_data['slider_overlay'] == 1) {
			echo '<div class="slider-overlap" style="background:'.$smof_data['slider_overlay_bg'].'"></div></div>';
		} else {
		echo '</div>';
		}
	}

}
?>

	
