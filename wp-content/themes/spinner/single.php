<?php get_header(); ?>
<?php
get_template_part('navigation');
if (is_front_page()) { get_template_part('slider'); } 
?>
<?php
if (have_posts()) : while (have_posts()) : the_post();

	$divid = sanitize_html_class($post -> ID);
	$pagetitle = get_the_title();
	$active_menu = esc_attr(get_post_meta($post -> ID, "monte_active_menu_item", true));
	
	$header_type = esc_attr(get_post_meta($post -> ID, "monte_post_header_type", true));
	$include_slider = esc_attr(get_post_meta($post -> ID, "monte_post_fw_slider", true));
	$slider_id = esc_attr(get_post_meta($post -> ID, "monte_slider_post_shortcode", true));
		
	$images = get_post_meta(get_the_ID(), 'monte_post_header_image', false);
	if ($images) {
		
	$image_src = wp_get_attachment_image_src($images[0], 'full');
	$url = esc_url($image_src[0]);
	}
	
	$header_bg = esc_attr(get_post_meta($post -> ID, "monte_post_header_bg_color", true));
	$heading_color = esc_attr(get_post_meta($post -> ID, "monte_post_heading_font_color", true));
	$breadcrums1 = esc_attr(get_post_meta($post -> ID, "monte_post_breadcrums1_color", true));
	$breadcrums2 = esc_attr(get_post_meta($post -> ID, "monte_post_breadcrums2_color", true));
	
	$alt_page_title = wp_kses_post(get_post_meta($post -> ID, "monte_alt_post_title", true));
	$page_subtitle = wp_kses_post(get_post_meta($post -> ID, "monte_post_subtitle", true));
	
	$page_overlap = esc_attr(get_post_meta($post -> ID, "monte_post_header_overlap", true));
	$page_overlap_bg = esc_attr(get_post_meta($post -> ID, "monte_post_header_overlap_bg", true));
	$title_font_size = esc_attr(get_post_meta($post -> ID, "monte_post_title_font_size", true));

	if ($alt_page_title) {
		$title = $alt_page_title;
	} else {
		$title = $pagetitle;
	}
	
	
	
?>

<?php if (!is_front_page()) {
	if  (($include_slider == 1) && ($slider_id != '')){
		echo '<div class="header">';
		putRevSlider($slider_id);
			if ($smof_data['slider_overlay'] == 1) {
			echo '<div class="slider-overlap" style="background:'.$smof_data['slider_overlay_bg'].'"></div>';
		} 
		echo '</div>';
	} else { ?>


	
<div <?php if ($header_type == 'color'){ ?> style="background:<?php echo esc_attr($header_bg);?>" <?php } ; ?> <?php if (($header_type == 'image') and ($image_src != '')) { ?> style="background:url(<?php echo esc_url($url);?>)" <?php } ; ?> >
	<div class="page-header <?php echo esc_attr($header_type); ?> padding-top-120 padding-bottom-130" id="<?php echo esc_attr($divid); ?>" >
			<div class="container">
				<div class="row">
					<div class="col-md-6 align-center wow fadeIn">
						<h4 style="color: <?php echo esc_attr($heading_color);?>; font-size: <?php echo esc_attr($title_font_size);?>"><?php echo wp_kses_post($title); ?></h4>
					</div>
					<div class="col-md-6 wow fadeIn padding-top-15">
						<?php if ($page_subtitle) { ?>
							<h5 style="color: <?php echo esc_attr($heading_color);?>"><?php echo esc_attr($page_subtitle);?></h5>
						<?php } else {?>
							<div class="empty padding-top-20"></div>
						<?php }?>
						<?php monte_the_breadcrumb($breadcrums1, $breadcrums2); ?>
					</div>
					

				</div>
			</div>
			</div>
			<?php if ($page_overlap == 'yes') { ?>
			<div class="slider-overlap" style="background:<?php echo esc_attr($page_overlap_bg);?>"></div>
			<?php } ?>
		</div>	
		
<?php		
} }?>		


		<div class="page-content <?php if (!is_front_page()) { ?>padding-top-70<?php }?> padding-bottom-80">
			<div class="container">
				<div class="row">
					<div class="col-md-9 blog-list clearfix">




						<div class="blog-content">
							<div class="singlepost fitvid clearfix">
								<?php get_template_part( 'post-formats/single', get_post_format() );  ?>
								<div class="row inner-content">
									<div class="col-md-2 col-sm-2 col-xs-3 align-center post-left">
										<i class="fa fa-camera"></i>
										<h2 class="month"><?php echo the_time( 'M' ); ?></h2>
										<h1 class="day"><?php echo the_time( 'd' ); ?></h1>
									</div>
									
									
									<div class="col-md-10 col-sm-10 col-xs-9">
										<h1 class="post-title-big"><?php the_title(); ?></h1>
											
										<div class="post-info-bar">
											<div class="info-item">
												<i class="fa fa-pencil-square"></i>
												<span><?php echo '' . __('by', 'monte') . ' ' . get_the_author_link(); ?></span>
											</div>
											<div class="info-item">
												<i class="fa fa-calendar"></i>
												<span><?php echo '' . __('on ', 'monte') . ' ' . get_the_date(); ?></span>
											</div>
											<div class="info-item">
												<i class="fa fa-folder-open"></i>
												<span><?php echo '' . __('in ', 'monte') . ' ' . get_the_category_list(', ', 'single', $post -> ID); ?></span>
											</div>
											<div class="info-item">
												<i class="fa fa-comments"></i>
												<span><a href="<?php comments_link(); ?>"><?php comments_number( 'No Comments', '1 Comment', '% Comments' ); ?></a></span>
											</div>
										</div>
										
										


										

										<div class="post-content">
										
										<?php the_content('', FALSE); ?>
									</div>
									<?php
										if (has_tag()) {
										the_tags('<div class="single-tags"><i class="fa fa-tags"> </i> ', ', ', '</div>');
										}
										?>
					
								</div>
				

							</div>
							</div>
							<?php comments_template(); ?>
						</div>
						
						
						
						
						
						
					</div>
			
					<div class="col-md-3 sidebar">
						<?php get_sidebar('blog'); ?>
					</div>
				</div>
			</div>
	
		</div> 
	


<script>
jQuery('.menu-item').each(function() {
	if ( jQuery(this).hasClass('<?php echo esc_attr($active_menu); ?>') ) {
		jQuery(this).addClass('active');
	}
	
});
</script>
<?php 
endwhile;
 else : ?>
<p><?php _e('No posts found', 'monte'); ?></p>
<?php
endif;
?>
<?php get_footer(); ?>