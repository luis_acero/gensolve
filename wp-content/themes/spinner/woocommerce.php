<?php get_header(); ?>
<?php
get_template_part('navigation');

global $smof_data;
$overlap = $smof_data['background_color'];

if (array_key_exists('woo_title', $smof_data)) {
	$title = $smof_data['woo_title'];
} 

if (array_key_exists('woo_subtitle', $smof_data)) {
	$subtitle = $smof_data['woo_subtitle'];
} 

if (is_front_page()) { get_template_part('slider'); } 
?>



<div>
	<div class="page-header error bg padding-top-120 padding-bottom-130">
		<div <?php post_class(); ?> >
			<div class="container">
				<div class="row">
					
					
					<?php if ($subtitle) { ?>
					<div class="col-md-6 align-center wow fadeIn">
						<?php if ($title) { ?>
						<h4><?php echo wp_kses_post($title); ?></h4>	
						<?php } else { ?>
						<h4><?php woocommerce_page_title(); ?></h4>
						<?php } ?>
					</div>
					<div class="col-md-6 wow fadeIn padding-top-15">
						<h5><?php echo wp_kses_post($subtitle); ?></h5>
					</div>
					
					<?php } else { ?>
					
					<div class="col-md-12 align-center wow fadeIn">
						<?php if ($title) { ?>
						<h4><?php echo wp_kses_post($title); ?></h4>	
						<?php } else { ?>
						<h4><?php woocommerce_page_title(); ?></h4>
						<?php } ?>
					</div>
					
					<?php } ?>
					

				</div>
			</div>
		</div>
		
		
	</div>	
		<div class="slider-overlap" style="background:<?php echo esc_attr($overlap); ?>"></div>
	</div>
	
	
	
	
	

<div class="page-content padding-top-80 padding-bottom-80">
			<div class="container">
				<div class="row">
					<div class="col-md-9 blog-list inner-content">
						<?php woocommerce_content(); ?>
					</div>
					<div class="col-md-3 sidebar">
						<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('store-sidebar') ) ?>
					</div>
				</div>
			</div>
		</div>
		
		




<?php get_footer(); ?>