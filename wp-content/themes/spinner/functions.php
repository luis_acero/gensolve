<?php

function monte_the_breadcrumb ($b1, $b2) {
     
    // Settings
    $separator  = '&gt;';
    $id         = 'breadcrumbs';
    $class      = 'breadcrumbs';
    $home_title = 'Home';
     
    // Get the query & post information
    global $post,$wp_query;
    $category = get_the_category();
     
    // Build the breadcrums
    echo '<ul id="' . esc_attr($id) . '" class="' . esc_attr($class) . '">';
     
    // Do not display on the homepage
    if ( !is_front_page() ) {
         
        // Home page
        echo '<li class="item-home"><a style="color: '.esc_attr($b1).'" class="bread-link bread-home" href="' . get_home_url() . '" title="' . esc_attr($home_title) . '">' . esc_attr($home_title) . '</a></li>';
		
        echo '<li class="separator separator-home" style="color: '.esc_attr($b2).'"> ' . esc_attr($separator) . ' </li>';
         
        if ( is_single() ) {
             
            // Single post (Only display the first category)
            if ($category) {
            echo '<li class="item-cat item-cat-' . esc_attr($category[0]->term_id) . ' item-cat-' . esc_attr($category[0]->category_nicename) . '">
            <a style="color: '.esc_attr($b1).'" class="bread-cat bread-cat-' . esc_attr($category[0]->term_id) . ' bread-cat-' . esc_attr($category[0]->category_nicename) . '
            " href="' . get_category_link($category[0]->term_id ) . '" title="' . esc_attr($category[0]->cat_name) . '">' . esc_attr($category[0]->cat_name) . '</a></li>';
            echo '<li style="color: '.esc_attr($b2).'" class="separator separator-' . $category[0]->term_id . '"> ' . esc_attr($separator) . ' </li>';
			}
            echo '<li class="item-current item-' . esc_attr($post->ID) . '"><span style="color: '.esc_attr($b2).'" class="bread-current bread-' . esc_attr($post->ID) . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
             
        } else if ( is_category() ) {
             
            // Category page
            echo '<li class="item-current item-cat-' . esc_attr($category[0]->term_id) . ' item-cat-' . esc_attr($category[0]->category_nicename) . '">
            <span style="color: '.$b2.'" class="bread-current bread-cat-' . esc_attr($category[0]->term_id) . ' bread-cat-' . esc_attr($category[0]->category_nicename) . '">' . esc_attr($category[0]->cat_name) . '</span></li>';
             
        } else if ( is_page() ) {
             
            // Standard page
            if( $post->post_parent ){
                 $parents ='';
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                 
                // Get parents in the right order
                $anc = array_reverse($anc);
                 
                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . esc_attr($ancestor) . '"><a style="color: '.esc_attr($b1).'" class="bread-parent bread-parent-' . esc_attr($ancestor) . '
                    " href="' . esc_attr(get_permalink($ancestor)) . '" title="' . esc_attr(get_the_title($ancestor)) . '">' . esc_attr(get_the_title($ancestor)) . '</a></li>';
                    $parents .= '<li style="color: '.esc_attr($b2).'" class="separator separator-' . esc_attr($ancestor) . '"> ' . esc_attr($separator) . ' </li>';
                }
                 
                // Display parent pages
                echo wp_kses_post($parents);
                 
                // Current page
                echo '<li class="item-current item-' . esc_attr($post->ID) . '"><span style="color: '.esc_attr($b2).'" title="' . get_the_title() . '"> ' . get_the_title() . '</span></li>';
                 
            } else {
                 
                // Just display current page if not parents
                echo '<li class="item-current item-' . esc_attr($post->ID) . '"><span style="color: '.esc_attr($b2).'" class="bread-current bread-' . esc_attr($post->ID) . '"> ' . get_the_title() . '</span></li>';
                 
            }
             
        } else if ( is_tag() ) {
             
            // Tag page
             
            // Get tag information
            $term_id = get_query_var('tag_id');
            $taxonomy = 'post_tag';
            $args ='include=' . $term_id;
            $terms = get_terms( $taxonomy, $args );
             
            // Display the tag name
            echo '<li class="item-current item-tag-' . esc_attr($terms[0]->term_id) . ' item-tag-' . esc_attr($terms[0]->slug) . '"><span style="color: '.esc_attr($b2).'" 
            class="bread-current bread-tag-' . esc_attr($terms[0]->term_id) . ' bread-tag-' . esc_attr($terms[0]->slug) . '">' . esc_attr($terms[0]->name) . '</span></li>';
         
        } elseif ( is_day() ) {
             
            // Day archive
             
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a style="color: '.esc_attr($b1).'" class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li style="color: '.esc_attr($b2).'" class="separator separator-' . get_the_time('Y') . '"> ' . esc_attr($separator) . ' </li>';
             
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a style="color: '.$b1.'" class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li style="color: '.esc_attr($b2).'" class="separator separator-' . get_the_time('m') . '"> ' . esc_attr($separator) . ' </li>';
             
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><span style="color: '.esc_attr($b2).'" class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</span></li>';
             
        } else if ( is_month() ) {
             
            // Month Archive
             
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a style="color: '.esc_attr($b1).'" class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li style="color: '.esc_attr($b2).'" class="separator separator-' . get_the_time('Y') . '"> ' . esc_attr($separator) . ' </li>';
             
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><span style="color: '.esc_attr($b1).'" class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</span></li>';
             
        } else if ( is_year() ) {
             
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><span style="color: '.esc_attr($b2).'" class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</span></li>';
             
        } else if ( is_author() ) {
             
            // Auhor archive
             
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
             
            // Display author name
            echo '<li class="item-current item-current-' . esc_attr($userdata->user_nicename) . '">
            <span style="color: '.esc_attr($b2).'" class="bread-current bread-current-' . esc_attr($userdata->user_nicename) . '" 
            title="' . esc_attr($userdata->display_name) . '">' . 'Author: ' . esc_attr($userdata->display_name) . '</span></li>';
         
        } else if ( get_query_var('paged') ) {
             
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><span style="color: '.esc_attr($b2).'" class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</span></li>';
             
        } else if ( is_search() ) {
         
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><span style="color: '.esc_attr($b2).'" class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</span></li>';
         
        } elseif ( is_404() ) {
             
            // 404 page
            echo '<li style="color: '.esc_attr($b1).'">' . 'Error 404' . '</li>';
        }
         
    }
     
	 
    echo '</ul>';
     
}





add_theme_support('woocommerce');
/**
 * Hook in on activation
 */
 
// Change number or products per row to 3

add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
function loop_columns() {
return 3; // 3 products per row
}
}

global $pagenow;
if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) add_action( 'init', 'monte_woocommerce_image_dimensions', 1 );
 

/**
 * Define image sizes
 */
function monte_woocommerce_image_dimensions() {
  	$catalog = array(
		'width' 	=> '500',	// px
		'height'	=> '500',	// px
		'crop'		=> 1 		// true
	);
 
	$single = array(
		'width' 	=> '700',	// px
		'height'	=> '700',	// px
		'crop'		=> 1 		// true
	);
 
	$thumbnail = array(
		'width' 	=> '300',	// px
		'height'	=> '300',	// px
		'crop'		=> 0 		// false
	);
 
	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
	update_option( 'shop_single_image_size', $single ); 		// Single product image
	update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
}
// set up textdomain
load_theme_textdomain('monte', get_template_directory() . '/lang');

/* Flush rewrite rules */
function monte_flush_rewrite_rules() {
	flush_rewrite_rules();
}
add_action('after_switch_theme', 'monte_flush_rewrite_rules');


/* Custom Pagination */


/* Custom Excerpts */


function monte_get_frontpage_excerpt(){
//	$permalink = get_permalink($post->ID);
	$excerpt = get_the_content();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 130);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'';
	return $excerpt;
}

function monte_get_portfolio_excerpt(){
//	$permalink = get_permalink($post->ID);
	$excerpt = get_the_content();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 150);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.' [...]';
	return $excerpt;
}

function monte_get_blog_excerpt(){
//	$permalink = get_permalink($post->ID);
	$excerpt = get_the_content();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 1000);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'';
	return $excerpt;
}


function monte_get_masonry_excerpt(){
	$excerpt = get_the_content();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 180);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'';
	return $excerpt;
}

function monte_get_masonry_excerpt2(){
	$excerpt = get_the_content();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 120);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'';
	return $excerpt;
}

function monte_get_masonry_excerpt3(){
	$excerpt = get_the_content();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 160);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'';
	return $excerpt;
}

function monte_get_masonry_excerpt4(){
	$excerpt = get_the_content();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 220);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'';
	return $excerpt;
}


// wp title modifier

function monte_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'twentytwelve' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'monte_wp_title', 10, 2 );

// load scripts
function monte_main_scripts() {
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), true);
	wp_enqueue_script('bootstrap');
	wp_enqueue_script('preloader', get_template_directory_uri() . '/js/jpreloader.min.js', array('jquery'), true);
	wp_enqueue_script('preloader');
	wp_enqueue_script('retina', get_template_directory_uri() . '/js/retina.js', array('jquery'), true);
	wp_enqueue_script('retina');
	wp_enqueue_script('count', get_template_directory_uri() . '/js/jquery.plugin.min.js', array('jquery'), true);
	wp_enqueue_script('count');
	wp_enqueue_script('wow', get_template_directory_uri() . '/js/wow.min.js', array('jquery'),  true);
	wp_enqueue_script('wow');
	wp_enqueue_script('masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'),  false);
	wp_enqueue_script('masonry');
	wp_enqueue_script('easypiechart', get_template_directory_uri() . '/js/jquery.easypiechart.js', array('jquery'),  true);
	wp_enqueue_script('easypiechart');
	wp_enqueue_script('scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0', true);
	wp_enqueue_script('scripts');
	wp_enqueue_script('prettyPhoto', get_template_directory_uri() . '/js/jquery.prettyPhoto.js', array('jquery'),  true);
	wp_enqueue_script('prettyPhoto');
	wp_enqueue_script('fitVids', get_template_directory_uri() . '/js/jquery.fitvids.js', array('jquery'), '1.0.3', true);
	wp_enqueue_script('fitVids');
	wp_enqueue_script('flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array('jquery'), '2.2.0', true);
	wp_enqueue_script('flexslider');
	wp_enqueue_script('easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js', array('jquery'), '1.3.0', true);
	wp_enqueue_script('easing');
	wp_enqueue_script('quicksand', get_template_directory_uri() . '/js/jquery.quicksand.js', array('jquery'), '1.0.0', true);
	wp_enqueue_script('quicksand');
	wp_enqueue_script('waypoints', get_template_directory_uri() . '/js/waypoints.min.js', array('jquery'), '1.0.0', true);
	wp_enqueue_script('waypoints');
	wp_enqueue_script('carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.0.0', true);
	wp_enqueue_script('carousel');
}

add_action('wp_enqueue_scripts', 'monte_main_scripts');


//load styles




function monte_main_styles() {

	wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('bootstrap');
	wp_register_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.css');
	wp_enqueue_style('font-awesome');
	wp_register_style('main', get_template_directory_uri() . '/style.css');
	wp_enqueue_style('main');
	wp_register_style('theme', get_template_directory_uri() . '/css/theme.css');
	wp_enqueue_style('theme');
	wp_register_style('animate', get_template_directory_uri() . '/css/animate.css');
	wp_enqueue_style('animate');
	wp_register_style('prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css');
	wp_enqueue_style('prettyPhoto');
	wp_register_style('flexslider', get_template_directory_uri() . '/css/flexslider.css');
	wp_enqueue_style('flexslider');
	wp_register_style('carousel', get_template_directory_uri() . '/css/owl.carousel.css');
	wp_enqueue_style('carousel');
	wp_register_style('woo', get_template_directory_uri() . '/css/woo.css');
	wp_enqueue_style('woo');
	

}

add_action('wp_enqueue_scripts', 'monte_main_styles');


function monte_load_custom_wp_admin_style() {
        wp_register_style( 'admin', get_template_directory_uri() . '/css/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'admin' );
		wp_register_script('metabox', get_template_directory_uri() . '/js/metabox.js', array('jquery'));
		wp_enqueue_script('metabox');
}
add_action( 'admin_enqueue_scripts', 'monte_load_custom_wp_admin_style' );




// custom portfolio link to trigger ajax in multipage mode
function monte_custom_previous_post_link($format, $link, $in_same_cat = false, $excluded_categories = '', $previous = true) {
if ( $previous && is_attachment() )
    $post = & get_post($GLOBALS['post']->post_parent);
else
    $post = get_adjacent_post($in_same_cat, $excluded_categories, $previous);

if ( !$post )
    return;

$link = get_permalink($post) . '?ajax=1';

$format = str_replace('%link', $link, $format);

$adjacent = $previous ? 'previous' : 'next';
echo apply_filters( "{$adjacent}_post_link", $format, $link );
}

// custom portfolio link to trigger ajax in multipage mode
function monte_custom_next_post_link($format, $link, $in_same_cat = false, $excluded_categories = '', $previous = true) {
if ( $previous && is_attachment() )
    $post = & get_post($GLOBALS['post']->post_parent);
else
    $post = get_adjacent_post($in_same_cat, $excluded_categories, $previous);

if ( !$post )
    return;

$link = get_permalink($post) . '?ajax=1';

$format = str_replace('%link', $link, $format);

$adjacent = $previous ? 'previous' : 'next';
echo apply_filters( "{$adjacent}_post_link", $format, $link );
}

// load google fonts
function monte_add_google_fonts() {
	wp_register_style('Montserrat', 'http://fonts.googleapis.com/css?family=Montserrat:400,700');
	wp_enqueue_style('Montserrat');
	wp_register_style('Lato', 'http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic');
	wp_enqueue_style('Lato');
}

add_action('wp_enqueue_scripts', 'monte_add_google_fonts');

// Disable admin bar display
//add_filter('show_admin_bar', '__return_false');



//Slightly Modified Options Framework

require_once ('admin/index.php');

// metaboxes
if ( class_exists( 'RW_Meta_Box' ) ) {
include_once (get_template_directory() . '/inc/metaboxes.php');
}

//visual composer addon

if (class_exists('WPBakeryShortCode')) {
	include_once (get_template_directory() . '/inc/vc.php');
}

// plugin activator
include_once (get_template_directory() . '/inc/class-tgm-plugin-activation.php');
include_once (get_template_directory() . '/inc/plugin-activator.php');




// post types
add_theme_support('post-formats', array('gallery', 'quote', 'audio', 'video'));

// blog sidebar
register_sidebar(array('name' => __('Blog Sidebar', 'monte'), 'id' => 'blog-sidebar', 'description' => __('Blog widgets.', 'monte'), 'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInRight">', 'after_widget' => '</div>', 'before_title' => '<h5>', 'after_title' => '</h5>'));

// store sidebar
register_sidebar(array('name' => __('Store Sidebar', 'monte'), 'id' => 'store-sidebar', 'description' => __('Store widgets.', 'monte'), 'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInRight">', 'after_widget' => '</div>', 'before_title' => '<h5>', 'after_title' => '</h5>'));


// page sidebar
register_sidebar(array('name' => __('Page Sidebar', 'monte'), 'id' => 'page-sidebar', 'description' => __('Page widgets.', 'monte'), 'before_widget' => '<div id="%1$s" class="widget %2$s wow fadeInRight">', 'after_widget' => '</div>', 'before_title' => '<h5>', 'after_title' => '</h5>'));


register_sidebar(array('name' => __('Footer Widget 1', 'monte'), 'id' => 'footer-widget1', 'description' => __('Footer Widget 1', 'monte'), 'before_widget' => '<div id="%1$s" class="footer-widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>'));
register_sidebar(array('name' => __('Footer Widget 2', 'monte'), 'id' => 'footer-widget2', 'description' => __('Footer Widget 2', 'monte'), 'before_widget' => '<div id="%1$s" class="footer-widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>'));
register_sidebar(array('name' => __('Footer Widget 3', 'monte'), 'id' => 'footer-widget3', 'description' => __('Footer Widget 3', 'monte'), 'before_widget' => '<div id="%1$s" class="footer-widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>'));
register_sidebar(array('name' => __('Footer Widget 4', 'monte'), 'id' => 'footer-widget4', 'description' => __('Footer Widget 4', 'monte'), 'before_widget' => '<div id="%1$s" class="footer-widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>'));




// post thumbnails
if (function_exists('add_theme_support')) {
	add_theme_support('post-thumbnails');
}

// thumbnails sizes
set_post_thumbnail_size(880, 330, true);
add_image_size('post-featured-image', 890, 500, true);
add_image_size('post-featured-image-grids', 580, 300, true);
add_image_size('portfolio-feat', 900, 600, true);
add_image_size('portfolio-thumb', 600, 400, true);
add_image_size('recent-widget-thumb', 75, 75, true);
add_image_size('team', 500, 500, true);


// register menu
function monte_register_menus() {
	register_nav_menus(array('primary-menu' => 'Primary Navigation Menu'));
}
add_action('init', 'monte_register_menus');

function monte_nav_menu_add_classes( $items, $args ) {
    //Add first item class
    $items[1]->classes[] = 'menu-item-first';

    //Add last item class
    $i = count($items);
    while($items[$i]->menu_item_parent != 0 && $i > 0) {
        $i--;
    }
    $items[$i]->classes[] = 'menu-item-last';

    return $items;
}
 
add_filter( 'wp_nav_menu_objects', 'monte_nav_menu_add_classes', 10, 2 );


// adjust navigation for one-page display



//  feed links
add_theme_support('automatic-feed-links');


// content max width
if (!isset($content_width))
	$content_width = 960;


function monte_enqueue_comments_reply() {
	if (get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}

add_action('comment_form_before', 'monte_enqueue_comments_reply');



//add_filter('woocommerce_show_page_title', false);


/* 
 * RECENT POSTS
 * WIDGET
*/


class monte_Widget_Recent_Posts_Footer extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_recent_entries', 'description' => __( "Your site&#8217;s most recent Posts.", "monte") );
		parent::__construct('recent-posts-footer', __('Recent Posts - Footer', "monte"), $widget_ops);
		$this->alt_option_name = 'widget_recent_entries';

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	function widget($args, $instance) {
		$cache = wp_cache_get('widget_recent_posts', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo esc_attr($cache[ $args['widget_id'] ]);
			return;
		}

		ob_start();
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts - Footer' , "monte");
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 10;
		if ( ! $number )
 			$number = 10;
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		$r = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true ) ) );
		if ($r->have_posts()) :
?>
<?php echo wp_kses_post($before_widget); ?>
<?php
	if ($title)
		echo wp_kses_post($before_title) . wp_kses_post($title) . wp_kses_post($after_title);
?>
<ul class="latest-news-list">
<?php while ( $r->have_posts() ) : $r->the_post();
?>

<li>
<a href="<?php the_permalink(); ?>" class="block"><?php the_title(); ?></a>
<?php
$category = get_the_category(); 
?>
<i class="uppercase"><?php echo wp_kses_post($category[0]->cat_name); ?></i><i>|</i><i> <?php echo get_the_date(); ?></i>
</li>
<?php endwhile; ?>
</ul>
<?php echo wp_kses_post($after_widget); ?>
<?php
// Reset the global $the_post as this query will have stomped on it
wp_reset_postdata();

endif;

$cache[$args['widget_id']] = ob_get_flush();
wp_cache_set('widget_recent_posts', $cache, 'widget');
}

function update( $new_instance, $old_instance ) {
$instance = $old_instance;
$instance['title'] = strip_tags($new_instance['title']);
$instance['number'] = (int) $new_instance['number'];
$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
$this->flush_widget_cache();

$alloptions = wp_cache_get( 'alloptions', 'options' );
if ( isset($alloptions['widget_recent_entries']) )
delete_option('widget_recent_entries');

return $instance;
}

function flush_widget_cache() {
wp_cache_delete('widget_recent_posts', 'widget');
}

function form( $instance ) {
$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
?>
<p><label for="<?php echo esc_attr($this -> get_field_id('title')); ?>"><?php _e('Title:', 'monte'); ?>
</label>
<input class="widefat" id="<?php echo esc_attr($this -> get_field_id('title')); ?>" name="<?php echo esc_attr($this -> get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

<p><label for="<?php echo esc_attr($this -> get_field_id('number')); ?>"><?php _e('Number of posts to show:', 'monte'); ?>
</label>
<input id="<?php echo esc_attr($this -> get_field_id('number')); ?>" name="<?php echo esc_attr($this -> get_field_name('number')); ?>" type="text" value="<?php echo esc_attr($number); ?>" size="3" /></p>

<p><input class="checkbox" type="checkbox" <?php checked($show_date); ?> id="<?php echo esc_attr($this -> get_field_id('show_date')); ?>" name="<?php echo esc_attr($this -> get_field_name('show_date')); ?>" />
<label for="<?php echo esc_attr($this -> get_field_id('show_date')); ?>"><?php _e('Display post date?', 'monte'); ?>
</label></p>
<?php
}
}





class monte_Widget_Recent_Posts extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_recent_entries', 'description' => __( "Your site&#8217;s most recent Posts.", "monte") );
		parent::__construct('recent-posts', __('Recent Posts', "monte"), $widget_ops);
		$this->alt_option_name = 'widget_recent_entries';

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	function widget($args, $instance) {
		$cache = wp_cache_get('widget_recent_posts', 'widget');

		if ( !is_array($cache) )
			$cache = array();

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo esc_attr($cache[ $args['widget_id'] ]);
			return;
		}

		ob_start();
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts' , "monte");
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 10;
		if ( ! $number )
 			$number = 10;
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		$r = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true ) ) );
		if ($r->have_posts()) :
?>
<?php echo wp_kses_post($before_widget); ?>
<?php
	if ($title)
		echo wp_kses_post($before_title) . wp_kses_post($title) . wp_kses_post($after_title);
?>
<ul class="latest-news-list">
<?php while ( $r->have_posts() ) : $r->the_post();
?>

<?php if (wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()))) { ?>
<li style="background:url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>')">
	<div class="news-list-hover">
		<h3><?php the_title(); ?></h3>
		<a href="<?php the_permalink(); ?>"><i class="fa fa-search"></i></a>
	</div>
</li>
<?php } else {?>
<li>
	<div class="news-list-hover nopicture">
		<h3><?php the_title(); ?></h3>
		<a href="<?php the_permalink(); ?>"><i class="fa fa-search"></i></a>
	</div>
</li>	
<?php } ?>

<?php endwhile; ?>
</ul>
<?php echo  wp_kses_post($after_widget); ?>
<?php
// Reset the global $the_post as this query will have stomped on it
wp_reset_postdata();

endif;

$cache[$args['widget_id']] = ob_get_flush();
wp_cache_set('widget_recent_posts', $cache, 'widget');
}

function update( $new_instance, $old_instance ) {
$instance = $old_instance;
$instance['title'] = strip_tags($new_instance['title']);
$instance['number'] = (int) $new_instance['number'];
$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
$this->flush_widget_cache();

$alloptions = wp_cache_get( 'alloptions', 'options' );
if ( isset($alloptions['widget_recent_entries']) )
delete_option('widget_recent_entries');

return $instance;
}

function flush_widget_cache() {
wp_cache_delete('widget_recent_posts', 'widget');
}

function form( $instance ) {
$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
?>
<p><label for="<?php echo esc_attr($this -> get_field_id('title')); ?>"><?php _e('Title:', 'monte'); ?>
</label>
<input class="widefat" id="<?php echo esc_attr($this -> get_field_id('title')); ?>" name="<?php echo esc_attr($this -> get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

<p><label for="<?php echo esc_attr($this -> get_field_id('number')); ?>"><?php _e('Number of posts to show:', 'monte'); ?>
</label>
<input id="<?php echo esc_attr($this -> get_field_id('number')); ?>" name="<?php echo esc_attr($this -> get_field_name('number')); ?>" type="text" value="<?php echo esc_attr($number); ?>" size="3" /></p>

<p><input class="checkbox" type="checkbox" <?php checked($show_date); ?> id="<?php echo esc_attr($this -> get_field_id('show_date')); ?>" name="<?php echo esc_attr($this -> get_field_name('show_date')); ?>" />
<label for="<?php echo esc_attr($this -> get_field_id('show_date')); ?>"><?php _e('Display post date?', 'monte'); ?>
</label></p>
<?php
}
}

add_action( 'widgets_init', create_function( '', 'register_widget( "monte_Widget_Recent_Posts" );' ) );
add_action( 'widgets_init', create_function( '', 'register_widget( "monte_Widget_Recent_Posts_Footer" );' ) );

function monte_comment($comment, $args, $depth) {
$GLOBALS['comment'] = $comment;
extract($args, EXTR_SKIP);

if ( 'div' == $args['style'] ) {
$tag = 'div';
$add_below = 'comment';
} else {
$tag = 'li ';
$add_below = 'div-comment';
}
?>
<<?php echo wp_kses_post($tag) ?>
<?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?>
 id="comment-<?php comment_ID() ?>">
<?php if ( 'div' != $args['style'] ) :
?>
<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
<?php endif; ?>
<div class="comment-name">
<?php
	if ($args['avatar_size'] != 0)
		echo get_avatar($comment, $args['avatar_size']);
?>
<?php printf(__('<span class="fn name">%s</span>'), get_comment_author_link()); ?>
<span class="reply">
(<?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>)
</span>

</div>
<?php if ( $comment->comment_approved == '0' ) :
?>
<div class="comment-date"><em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'monte'); ?>
	</em>
<br /></div>
<?php endif; ?>

<div class="comment-date"><a href="<?php echo htmlspecialchars(get_comment_link($comment -> comment_ID)); ?>">
<?php
printf(__('%1$s at %2$s', 'monte'), get_comment_date(), get_comment_time());
 ?>
 </a><?php edit_comment_link(__('(Edit)', 'monte'), '  ', ''); ?>
</div>

<?php comment_text(); ?>
<?php if ( 'div' != $args['style'] ) :
?>
</div>
<?php endif; ?>
<?php
}

add_action('wp_head','hook_css');

function hook_css()
{

get_template_part('inc/custom-styles'); 

}

?>