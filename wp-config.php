<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gso');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_|($)|r5kQzO{8ruA-@yaDubP%`yAE~`a+mqa!W9kiv,xE^I5DW[8cfe(l(/GbG<');
define('SECURE_AUTH_KEY',  'RCO3<Ke%o:k}1Dp0Pw0CN_U_g{x-HOMK($kJS#-uxk:,+%^+%k)h!Q~x,CwM|V?$');
define('LOGGED_IN_KEY',    'eWE@45;EP[Ogg1*2!iqbwgPw_4NN_%j}h:m){F|ywXhZKi/qd7E,T+C[,At|wW1L');
define('NONCE_KEY',        '#1p#kc=s$7 Q-k&XB|h<9hB[?b<`uRSZ6f-}-;sCK^mYy-Kra-epwk=qy2ho9!Qh');
define('AUTH_SALT',        ' DB:x@PDhJ:%uA)k%@z.N5[f;cGW-2yaTXC-q{zi/yeqGX3u3qh%j3eCai6Ar^<)');
define('SECURE_AUTH_SALT', 'bq4?5Ul*(d|hJ$,`(?XfnRl4 nh`HHSiLF/#=e-f:1eBpJb~79}<~r<Ggq(1Cu)y');
define('LOGGED_IN_SALT',   '{$Qln1):_*AGIuKEa>CLo6J]!`bTW9u?Q<>6!|qOk?z_`ips@=}@0^nl]XK8jQSA');
define('NONCE_SALT',       'y6r=FOiJ&$|ua_!!<KO|EPKo.LA`,g7Thf>+WGY3Vs8=bwj7hrJ-Y|Eg1cy:&w:A');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
